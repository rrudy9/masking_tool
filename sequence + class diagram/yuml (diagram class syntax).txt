// Cool Class Diagram
// ------------------

// Chain elements like this
//[Customer]>[Order]>[LineItem]
[ApplicationSetting{bg:wheat}]<[DataManager]
[MaskingDataContent{bg:wheat}] <[DataManager]
[MaskingEventsData{bg:purple}]<[DataManager]

[EventItemData{bg:wheat}] <[MaskingEventsData]
[EventItem{bg:wheat}] <[MaskingEventsData]

[ImagesData{bg:purple}]<[DataManager]
[ImageTexture{bg:wheat}] <[ImagesData]

[MaskingTagData{bg:purple}]<[DataManager]

[MaskingPropertiesData{bg:purple}]<[DataManager]
[TagSettingProperties{bg:wheat}] <[MaskingPropertiesData]
[GrayFilterProperties{bg:wheat}] <[MaskingPropertiesData]
[OverlayFilterProperties{bg:wheat}] <[MaskingPropertiesData]
[ExitFilterProperties{bg:wheat}] <[MaskingPropertiesData]

[MaskingInputsData{bg:purple}]<[DataManager]
[GamepadAnalogInput{bg:wheat}] <[MaskingInputsData]

[InputsMouseTagData{bg:purple}]<[MaskingInputsData]
[MouseTagData{bg:wheat}] <[InputsMouseTagData]

[InputsKeyboardData{bg:purple}]<[MaskingInputsData]
[KeyboardInput{bg:wheat}] <[InputsKeyboardData]
[KeyboardInputData{bg:wheat}] <[InputsKeyboardData]

[GamepadButtonData{bg:purple}]<[MaskingInputsData]
[GamepadClickableData{bg:wheat}] <[GamepadButtonData]

// Add more detail
[≪struct≫;ApplicationSetting|{bg:wheat}+_picToolAction: bool;+_mouseTagActivation: bool;+ _tagMaskingActivation: bool; +_imageSelected: bool; +_firstSelection: bool; +_lastIndex:int; +_currentSelection:int  ]

[<<struct>>;MaskingDataContent|{bg:wheat} + _maskingInputData:MaskingInputData; +_maskingPropertiesData: MaskingPropertiesData; +_maskingEventsData: MaskingEventsData]

[DataManager| -_maskingTagData: MaskingTagData; -_applicationSetting: ApplicationSetting;-_selectedImageData: std::vector(MaskingDataContent); -_imageData:ImagesData; -_maskingEventData:MaskingEventData; -_maskingPropertiesData:MaskingPropertiesData; _maskingInputData:MaskingInputsData; | +getters() ]


[<<struct>>;EventItemData|{bg:wheat} +_eventsItemLabel: std::vector(std::string); +_eventsMaskingData : std::vector(std::vector(EventItem))]

[<<struct>>;EventItem|{bg:wheat} +_value: std::string]

[MaskingEventsData|{bg:purple} -_eventData: EventItemData; _numberOfEvent:int | + getters()]

[<<struct>>;ImageTexture|{bg:wheat} +_imageTexture: unsigned int; +_width: int; +_height: int; +_imageData:unsigned char*;+_compression: int; +_imageName: std::string]

[ImagesData|{bg:purple} -_imagesContainer: std::vector(ImageTexture); | +getters()]

[MaskingTagData|{bg:purple} -_maskingData:std::vector(std::vector(ImVec2)); | +getters()]


[<<struct>>;TagSettingProperties|{bg:wheat} +_width: int; +_height: int; +_percentage: float;+_blockPercentage: float;]

[<<struct>>;GrayFilterProperties|{bg:wheat} +_activateGrayFilter: int; +_moveAfterPress: float; +_pressAfterMove: float;+_keyboardDelay: float;]

[<<struct>>;OverlayFilterProperties|{bg:wheat} +_activateOverlay: bool; +_showOnEvent: bool; +_showTimeOverlay: bool]

[<<struct>>;ExitFilterProperties|{bg:wheat} +_activateExitFilter: bool; +_exitCode: uint8_t ; +_exitTime: float]


[MaskingPropertiesData|{bg:purple} -_tagSetting: TagSettingProperties; -_grayFilter: GrayFilterProperties; -_overlayFilter: OverlayFilterProperties; _exitFilter: ExitFilterProperties; | +getters()]

[<<struct>>;GamepadAnalogInput|{bg:wheat} +_minInputValue: float; +_maxInputValue: float]

[MaskingInputsData|{bg:purple} - _keyboardData: InputsKeyboardData; _gamepadButtonData: GamepadButtonData; - _mouseTagData: InputsMouseData; - _gamepadAnalogList: std::vector(GamepadAnalogInput) | +getters()]

[<<struct>>;MouseTagData|{bg:wheat} +_blackList: bool; +_whiteList: bool; +_mousePoints:std::vector(ImVec2)]

[InputsMouseTagData|{bg:purple} -_mouseTagData: MouseTagData | +getters(); +addMousePoints(ImVec2, ImVec2); +removeMousePoints(index)]

[<<struct>>;KeyboardInput|{bg:wheat} + _scanCode: UINT; +_vKey: std::string; +_vKeyName: std::string]

[<<struct>>;KeyboardInputData|{bg:wheat} +_blackList: bool; +_whiteList: bool; +_keyboardInputs: std::vector(KeyboardInput)]

[InputsKeyboardData|{bg:purple} - _keyboardInputData:KeyboardInputData; +getters(); +insertKeyboardInput(string&, string&, &UINT); _removeKeyboardInput(index)]

[<<struct>>;GamepadClickableData|{bg:purple} +_blackList: bool; 
+_whiteList: bool; +_gamepadButtonList: std::vector(std::string)
]

[GamepadButtonData|{bg:purple} -_gamepadButtonData: GamepadClickableData | +getters(); +addGamepadButton(int&); +removeGamepadButton(int)]

