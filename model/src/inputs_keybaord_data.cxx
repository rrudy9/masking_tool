#include <inputs_keyboard_data.h>

namespace data
{

	InputsKeyboardData::InputsKeyboardData()
	{};



	void InputsKeyboardData::initializeValue()
	{
		_keyboardInputData = { true, false };
		_keyboardInputData._keyboardInputs.clear(); 

	}

	void InputsKeyboardData::insertKeyboardInput(const std::string& vKey, const std::string& vKeyName, const UINT& scanCode)
	{

		struct KeyboardInput inputKeys = {
			scanCode,
			vKey,
			vKeyName
		};

		_keyboardInputData._keyboardInputs.push_back(inputKeys);

	}

	void InputsKeyboardData::removeKeyboardInput(int indexOfItem)
	{
		_keyboardInputData._keyboardInputs.erase(_keyboardInputData._keyboardInputs.begin() + indexOfItem);
	}

	void InputsKeyboardData::openKeyboadInput()
	{
		try
		{
			RAWINPUTDEVICE Rid[1] = { 0 };

			Rid[0].usUsagePage = 0x01;          // HID_USAGE_PAGE_GENERIC
			Rid[0].usUsage = 0x06;              // HID_USAGE_GENERIC_KEYBOARD
			Rid[0].dwFlags = RIDEV_NOLEGACY;    // adds keyboard and also ignores legacy keyboard messages
			Rid[0].hwndTarget = 0;

			if (RegisterRawInputDevices(Rid, 1, sizeof(Rid[0])) == FALSE)
				throw::std::exception("Keyboard Keys Registration Failed");

		}
		catch (std::exception& exception)
		{
			std::cerr << exception.what() << '\n';

		}
	}

	void InputsKeyboardData::startKeyboardInput(std::string& vKeyValue, std::string& vKeyCharName, UINT& getScanCode)
	{

		try {
			LPARAM* lParam = nullptr;
			
			openKeyboadInput(); 

			MSG msg = {};
			
#ifdef DIRECTX_FOUND



			while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg); // this calls window procs!
			}


			vKeyCharName = WindowsManager::getInstance()->_vKeyCharName;
			vKeyValue = WindowsManager::getInstance()->_vKeyValue;
			getScanCode = WindowsManager::getInstance()->_scanCode;
			
#else

			while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg); // this calls window procs!
				
				if (WM_INPUT == msg.message)
				{
					UINT size = 0;
					GetRawInputData((HRAWINPUT)msg.lParam,
						RID_INPUT, NULL, &size, sizeof(RAWINPUTHEADER));

					LPBYTE lpb = NULL;

					try {
						lpb = new BYTE[size];

						if (lpb == NULL)
							throw std::runtime_error("Can't allocate your byte array for raw Input\n");

					}
					catch (const std::runtime_error& exception) {
						std::cerr << "Allocation Failed: " << exception.what() << '\n';
						break;
					}


					GetRawInputData((HRAWINPUT)msg.lParam,
						RID_INPUT, lpb, &size, sizeof(RAWINPUTHEADER));

					RAWINPUT* raw = (RAWINPUT*)lpb;

					if (RIM_TYPEKEYBOARD == raw->header.dwType)
					{


						// need to add hot keys
						RAWKEYBOARD* data = &raw->data.keyboard;

						UINT scanCode = MapVirtualKey(data->VKey, MAPVK_VK_TO_VSC);

						switch (data->VKey)
						{
						case VK_LEFT: case VK_UP: case VK_RIGHT: case VK_DOWN:
						case VK_RCONTROL: case VK_RMENU:
						case VK_LWIN: case VK_RWIN: case VK_APPS:
						case VK_PRIOR: case VK_NEXT:
						case VK_END: case VK_HOME:
						case VK_INSERT: case VK_DELETE:
						case VK_DIVIDE:
						case VK_NUMLOCK:

							scanCode |= KF_EXTENDED;

						}


						TCHAR keyStrName[128];
						int result = 0;

						try {

							result = GetKeyNameText(scanCode << 16, keyStrName, 128);
							if (result == 0)
							{
								throw std::runtime_error("GetKeyNameText result error\n");
							}
						}
						catch (const std::runtime_error& exception) {
							std::cerr << "WinAPI Error occured " << exception.what() << '\n';
							break;
						}

						vKeyCharName = keyStrName;
						vKeyValue = std::to_string(data->VKey);
						getScanCode = scanCode;
					}

					delete[] lpb;
				}
			}
			
#endif // DIRECTX_FOUND

		}
		catch (std::exception& exception)
		{
			std::cerr << exception.what() << '\n';
		}

	}

	void InputsKeyboardData::closeKeyboardInput()
	{
	
		try {
			RAWINPUTDEVICE Rid[1] = { 0 };

			Rid[0].usUsagePage = 0x01;          // HID_USAGE_PAGE_GENERIC
			Rid[0].usUsage = 0x06;              // HID_USAGE_GENERIC_KEYBOARD
			Rid[0].dwFlags = RIDEV_REMOVE;    // adds keyboard and also ignores legacy keyboard messages
			Rid[0].hwndTarget = 0;

			if (RegisterRawInputDevices(Rid, 1, sizeof(Rid[0])) == FALSE)
				throw::std::exception("cant close keybaord input ");
	
		}
		catch (std::exception& exception)
		{
			std::cerr << exception.what() << '\n';
		}

	}


	struct KeyboardInputData& InputsKeyboardData::getKeyboardInputData()
	{
		return _keyboardInputData; 
	}

}