
#include <project_saver.h>

namespace data
{
	ProjectSaver::ProjectSaver(
		DataManager& dataManager
	)
		:_dataManager(dataManager)
	{
	};

	void ProjectSaver::saveProject(const std::string& filePath, const std::string& filePathName)
	{
	
		try {
			
			std::ofstream writeFile(filePathName, std::ios::out | std::ios::binary);
			
			if (!writeFile) {
				throw::std::exception("Can't open file for saving\n"); 
			}
			
			/// write our data
			
			writeFile.write((const char*)&_dataManager.getApplicationSetting(), sizeof(_dataManager.getApplicationSetting()));

			/// we save our masking tools data
			size_t allDataSize = _dataManager.getAllData().size();

			writeFile.write((char*)&allDataSize, sizeof(allDataSize));

			for (size_t i = 0; i < allDataSize; ++i)
			{
				/// we start by saving the masking properties data 
				writeFile.write((char*)&_dataManager.getAllData()[i]._maskingPropertiesData,  sizeof(MaskingPropertiesData));

				/// event data
				{
					for (size_t j = 0; j < _dataManager.getAllData().at(i)._maskingEventsData.getNumberOfEventList(); ++j)
					{
						size_t eventItemDataSize = _dataManager.getAllData().at(i)._maskingEventsData.getEventsItemData()._eventsMaskingData.at(j).size();

						writeFile.write((char*)&eventItemDataSize, sizeof(eventItemDataSize));

						for (size_t t = 0; t < eventItemDataSize; ++t)
						{
							size_t eventItemSize = _dataManager.getAllData().at(i)._maskingEventsData.getEventsItemData()._eventsMaskingData.at(j).at(t)._value.size();
							writeFile.write((char*)&eventItemSize, sizeof(eventItemSize));

							writeFile.write(&_dataManager.getAllData().at(i)._maskingEventsData.getEventsItemData()._eventsMaskingData.at(j).at(t)._value[0], eventItemSize);
						}
					
						size_t eventLabel = _dataManager.getAllData().at(i)._maskingEventsData.getEventsItemData()._eventItemsLabel.at(j).size(); 
						writeFile.write((char*)&eventLabel, sizeof(eventLabel));
						writeFile.write(&_dataManager.getAllData().at(i)._maskingEventsData.getEventsItemData()._eventItemsLabel.at(j)[0], eventLabel);

					
					}
				}

				/// input data
				{
					/// input mouse data 
					{
						writeFile.write((char*)&_dataManager.getAllData().at(i)._maskingInputsData.getMouseInputData().getMouseTagData()._blackList, sizeof(bool));
						writeFile.write((char*)&_dataManager.getAllData().at(i)._maskingInputsData.getMouseInputData().getMouseTagData()._whiteList, sizeof(bool));
					
						size_t mousePointsSize = _dataManager.getAllData().at(i)._maskingInputsData.getMouseInputData().getMouseTagData()._mousePoints.size(); 
						writeFile.write((const char*)&mousePointsSize, sizeof(mousePointsSize));

						for (int k = 0; k < mousePointsSize; ++k)
						{
							writeFile.write((char*)&_dataManager.getAllData().at(i)._maskingInputsData.getMouseInputData().getMouseTagData()._mousePoints.at(k), sizeof(ImVec2));
						}

					}
			
					/// gamepad button
					{
						writeFile.write((char*)&_dataManager.getAllData().at(i)._maskingInputsData.getGamepadButtonData().getGamepadButtonData()._blackList, sizeof(bool));
						writeFile.write((char*)&_dataManager.getAllData().at(i)._maskingInputsData.getGamepadButtonData().getGamepadButtonData()._whiteList, sizeof(bool));

						size_t gamepadButtonListSize = _dataManager.getAllData().at(i)._maskingInputsData.getGamepadButtonData().getGamepadButtonData()._gamepadButtonList.size(); 
						writeFile.write((const char*)&gamepadButtonListSize, sizeof(gamepadButtonListSize));

						for (size_t k = 0; k < gamepadButtonListSize; ++k)
						{
							size_t strSize = _dataManager.getAllData().at(i)._maskingInputsData.getGamepadButtonData().getGamepadButtonData()._gamepadButtonList.at(k).size();
							writeFile.write((char*)&strSize, sizeof(strSize));
							writeFile.write(&_dataManager.getAllData().at(i)._maskingInputsData.getGamepadButtonData().getGamepadButtonData()._gamepadButtonList.at(k)[0], strSize);

						}
					}


					/// gamepad analog data 
					{
						size_t gamepadAnalaogDataSize = _dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().size();
						writeFile.write((char*)&gamepadAnalaogDataSize, sizeof(gamepadAnalaogDataSize));

						for (size_t k = 0; k < gamepadAnalaogDataSize; ++k)
						{
							writeFile.write((char*)&_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(k)._maxInputValue, sizeof(float));
							writeFile.write((char*)&_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(k)._minInputValue, sizeof(float));
						}

					}

					/// keyboard input
					{
						writeFile.write((char*)&_dataManager.getAllData().at(i)._maskingInputsData.getKeyboardData().getKeyboardInputData()._blackList, sizeof(bool));
						writeFile.write((char*)&_dataManager.getAllData().at(i)._maskingInputsData.getKeyboardData().getKeyboardInputData()._whiteList, sizeof(bool));

						size_t keyboardInputDataSize = _dataManager.getAllData().at(i)._maskingInputsData.getKeyboardData().getKeyboardInputData()._keyboardInputs.size(); 
						writeFile.write((const char*)&keyboardInputDataSize, sizeof(keyboardInputDataSize));

						for (size_t k = 0; k < keyboardInputDataSize; ++k)
						{
							writeFile.write((char*)&_dataManager.getAllData().at(i)._maskingInputsData.getKeyboardData().getKeyboardInputData()._keyboardInputs.at(k)._scanCode, sizeof(UINT));
						
							size_t vKeySize = _dataManager.getAllData().at(i)._maskingInputsData.getKeyboardData().getKeyboardInputData()._keyboardInputs.at(k)._vKey.size();
							writeFile.write((char*)&vKeySize, sizeof(vKeySize));
							writeFile.write(&_dataManager.getAllData().at(i)._maskingInputsData.getKeyboardData().getKeyboardInputData()._keyboardInputs.at(k)._vKey[0], vKeySize);

						
							size_t vKeyNameSize = _dataManager.getAllData().at(i)._maskingInputsData.getKeyboardData().getKeyboardInputData()._keyboardInputs.at(k)._vKeyName.size();
							writeFile.write((char*)&vKeyNameSize, sizeof(vKeyNameSize));
							writeFile.write(&_dataManager.getAllData().at(i)._maskingInputsData.getKeyboardData().getKeyboardInputData()._keyboardInputs.at(k)._vKeyName[0], vKeyNameSize);

						}

					}

				}
			}
			
			/// we get our images name to load from the project loader
			/// Write the size of the vector
			size_t imageSizeVector = _dataManager.getImageData().getImageContainer().size();
			writeFile.write((char*)&imageSizeVector, sizeof(imageSizeVector));

			for (size_t i = 0; i < imageSizeVector; ++i)
			{
				size_t strSize = _dataManager.getImageData().getImageContainer()[i]._imageName.size();

				writeFile.write((char*)&strSize, sizeof(strSize));
				writeFile.write(&_dataManager.getImageData().getImageContainer()[i]._imageName[0], strSize);
			}

			
			/// mask tag data
			size_t maskTagDataSize = _dataManager.getMaskingTagData().getMaskingTagData().size(); 
			writeFile.write((char*)&maskTagDataSize, sizeof(maskTagDataSize));
			for (size_t i = 0; i < _dataManager.getMaskingTagData().getMaskingTagData().size(); ++i)
			{
				size_t maskTagDataSize = _dataManager.getMaskingTagData().getMaskingTagData().at(i).size();
				writeFile.write((const char*)&maskTagDataSize, sizeof(maskTagDataSize)); 
				
				for (size_t k = 0; k < maskTagDataSize; ++k)
				{
					writeFile.write((char*)&_dataManager.getMaskingTagData().getMaskingTagData().at(i).at(k), sizeof(ImVec2));

				}
			}


			writeFile.close();
		
			if (!writeFile.good()) {
				throw::std::exception("Error occurred at writing time!\n");
			}

			for (int i = 0; i < _dataManager.getImageData().getImageContainer().size(); ++i)
			{
				std::string fileName = _dataManager.getImageData().getImageContainer().at(i)._imageName.substr(0, _dataManager.getImageData().getImageContainer().at(i)._imageName.find("."));

				std::string imagePath = filePath + "\\"+ fileName+ ".bmp";

				writeTextureToFile(imagePath, _dataManager.getImageData().getImageContainer().at(i)._width
					, _dataManager.getImageData().getImageContainer().at(i)._height,
					_dataManager.getImageData().getImageContainer().at(i)._compression,
					_dataManager.getImageData().getImageContainer().at(i)._imageData
				);

			}

			writeFile.close(); 
		}
		catch (const std::exception& exception)
		{
			std::cerr << exception.what() << '\n';
		};
	}
}