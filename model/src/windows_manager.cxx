#include <windows_manager.h>


WindowsManager* WindowsManager::_windowsManagerInstance{ nullptr };
std::mutex WindowsManager::_mutex;


WindowsManager::WindowsManager()
{
}


WindowsManager* WindowsManager::getInstance()
{
    std::lock_guard<std::mutex> lock(_mutex);
    if (_windowsManagerInstance == nullptr)
    {
        _windowsManagerInstance = new WindowsManager();
    }
    return _windowsManagerInstance;
}

bool& WindowsManager::getWindowMovingValue()
{
    return _startMove; 
}

void WindowsManager::resetInstance()
{
    delete WindowsManager::_windowsManagerInstance;
    WindowsManager::_windowsManagerInstance = NULL;
}

