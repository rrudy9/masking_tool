#include <masking_properties_data.h>

namespace data
{
	MaskingPropertiesData::MaskingPropertiesData()
	{
		initializeMaskingPropertiesDataModel(); 
	};

	void MaskingPropertiesData::initializeMaskingPropertiesDataModel()
	{
		
		_tagSetting = { 16, 16, 0.75, 0.75 };
		_grayFilter = { false, 0.0, 0.0, 0.0 };
		_overlayFilter = { false, false, 0.0 };
		_exitFilter = { false, 0, 0.0 };


	}

	struct TagSettingProperties& MaskingPropertiesData::getTagSetting()
	{
		return _tagSetting;
	}

	struct GrayFilterProperties& MaskingPropertiesData::getGrayFilterData()
	{
		return _grayFilter;
	}

	struct OverlayFilterProperties& MaskingPropertiesData::getOverlayFilterData()
	{
		return _overlayFilter;
	}

	struct ExitFilterProperties& MaskingPropertiesData::getExitFilterData()
	{
		return _exitFilter;
	}


}