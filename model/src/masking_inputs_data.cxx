#include <masking_inputs_data.h>

namespace data
{

	MaskingInputsData::MaskingInputsData()
		:_keyboardData(InputsKeyboardData())
		,_gamepadButtonData(GamepadButtonData())
		,_mouseTagData(InputsMouseTagData())
	{
		initializeMaskingInputsData(); 
	};


	void MaskingInputsData::initializeMaskingInputsData()
	{

		_keyboardData.initializeValue(); 
		_gamepadButtonData.initializeValue(); 
		_mouseTagData.initMouseTagValues(); 

		_gamepadAnalogList.clear(); 

		struct GamepadAnalogInput Rx = {-1.0, 1.0};
		struct GamepadAnalogInput Ry = {-1.0, 1.0};
		struct GamepadAnalogInput Lx = {-1.0, 1.0};
		struct GamepadAnalogInput Ly = {-1.0, 1.0};

		_gamepadAnalogList.push_back(Rx);
		_gamepadAnalogList.push_back(Ry);
		_gamepadAnalogList.push_back(Lx);
		_gamepadAnalogList.push_back(Ly);
		
	}

	InputsKeyboardData& MaskingInputsData::getKeyboardData()
	{
		return _keyboardData;
	}

	GamepadButtonData& MaskingInputsData::getGamepadButtonData()
	{
		return _gamepadButtonData;
	}

	std::vector<GamepadAnalogInput>& MaskingInputsData::getGamepadAnalogData()
	{
		return _gamepadAnalogList;
	}

	InputsMouseTagData& MaskingInputsData::getMouseInputData()
	{
		return _mouseTagData; 
	}

}
