#include <masking_events_data.h>


namespace data
{
    MaskingEventsData::MaskingEventsData()
        :_numberOfEvent(4)
    {
    
        initializeEventItemsLabel(_eventData._eventItemsLabel, _numberOfEvent);

        initializeEventParamsData();

    };

    void MaskingEventsData::initializeEventParamsData()
    {
     
        _eventData._eventsMaskingData.clear();
        _numberOfEvent = 4;

        // init to zero la struct 
        std::vector<EventItem> initValues = 
            std::vector<EventItem>();

        for (int i = 0 ; i < _numberOfEvent ; ++ i)
        {
            _eventData._eventsMaskingData.push_back(initValues);
        }

    }

    int MaskingEventsData::getNumberOfEventList()
    {
        return _numberOfEvent; 
    }

    void MaskingEventsData::fillEventParameter(int currentIndexSelected, const std::string& params)
    { 
    
        if (params != "")
        {
        
            struct EventItem eventItem = { params };

            _eventData._eventsMaskingData.at(currentIndexSelected)
                .push_back(eventItem);
            
        }
    }

    void MaskingEventsData::removeEventParameter(int rowValue, int columnValue)
    {
        _eventData._eventsMaskingData
        [rowValue].erase(
            _eventData._eventsMaskingData[rowValue].begin() + columnValue);

    }

    struct EventItemData& MaskingEventsData::getEventsItemData()
    {
        return _eventData; 
    }

    void MaskingEventsData::initializeEventItemsLabel(std::vector<std::string>& items, uint8_t sizeOFArray)
    {

        // add other 
        char* tempItems[] =
        {
            "Show That",
            "Show This",
            "Do This",
            "Do That"
        };

        for (int i = 0; i < sizeOFArray; ++i)
        {
            items.push_back(tempItems[i]);
        }

    }



   
}