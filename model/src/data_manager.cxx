
#include <data_manager.h>

namespace data
{

	DataManager::DataManager()
		:_maskingEventData(MaskingEventsData())
		, _maskingPropertiesData(MaskingPropertiesData())
		, _maskingInputsData(MaskingInputsData())
		, _imageData(ImagesData())
		,_maskingTagData(MaskingTagData())
	{
		initializeDataManager(); 
	};

	MaskingInputsData& DataManager::getInputsData()
	{
		return _maskingInputsData;
	}

	MaskingPropertiesData& DataManager::getPropertiesData()
	{
		return _maskingPropertiesData;
	}

	MaskingEventsData& DataManager::getEventsData()
	{
		return _maskingEventData;
	}

	ImagesData& DataManager::getImageData()
	{
		return _imageData;
	}

	struct ApplicationSetting& DataManager::getApplicationSetting()
	{
		return _applicationSetting; 
	}

	MaskingTagData& DataManager::getMaskingTagData()
	{	
		return _maskingTagData;
	}

	std::vector<MaskingDataContent>& DataManager::getAllData()
	{
		return _selectedImageData; 
	}

	void DataManager::setDataForEachPicture()
	{

		if (_imageData.getImageContainer().size() > 1)
		{
		
			struct MaskingDataContent maskingData = {
					_maskingInputsData,
					_maskingPropertiesData,
					_maskingEventData
			};


			maskingData._maskingInputsData.initializeMaskingInputsData();
			maskingData._maskingPropertiesData.initializeMaskingPropertiesDataModel(); 
			maskingData._maskingEventsData.initializeEventParamsData();

			_selectedImageData.push_back(maskingData);

			_maskingTagData.getMaskingTagData().push_back(std::vector<ImVec2>());

		}
	}

	void DataManager::setLastDataBeforeSave()
	{
		_selectedImageData.at(_applicationSetting._currentSelection)._maskingPropertiesData = _maskingPropertiesData;
		_selectedImageData.at(_applicationSetting._currentSelection)._maskingEventsData = _maskingEventData;
		_selectedImageData.at(_applicationSetting._currentSelection)._maskingInputsData = _maskingInputsData;

	}


	void DataManager::removeDataForSelectedImage(int index)
	{
		_imageData.removeImage(index); 
		_applicationSetting._currentSelection = index;

		// last one
		if (_imageData.getImageContainer().size() == 0)
		{
			_selectedImageData.at(0)._maskingPropertiesData.initializeMaskingPropertiesDataModel();
			_selectedImageData.at(0)._maskingEventsData.initializeEventParamsData();
			_selectedImageData.at(0)._maskingInputsData.initializeMaskingInputsData();
			_maskingTagData.getMaskingTagData().at(0).clear();

			_maskingEventData = _selectedImageData.at(0)._maskingEventsData;
			_maskingPropertiesData = _selectedImageData.at(0)._maskingPropertiesData;
			_maskingInputsData = _selectedImageData.at(0)._maskingInputsData;


			_applicationSetting = { false, false, false , false, false, 0, 0 };
			
			return; 
		}

		// we keep one 
		if (_selectedImageData.size() > 1)
		{
			_selectedImageData.erase(
				_selectedImageData.begin() + index
			);

			_maskingTagData.getMaskingTagData().erase(
				_maskingTagData.getMaskingTagData().begin() + index
			);

			if (index == _selectedImageData.size())
			{
				_maskingEventData = _selectedImageData.at(index - 1)._maskingEventsData;
				_maskingPropertiesData = _selectedImageData.at(index - 1)._maskingPropertiesData;
				_maskingInputsData = _selectedImageData.at(index - 1)._maskingInputsData;

				_applicationSetting._currentSelection = _applicationSetting._lastIndex = index - 1;
			}
			else
			{
				_maskingEventData = _selectedImageData.at(index)._maskingEventsData;
				_maskingPropertiesData = _selectedImageData.at(index)._maskingPropertiesData;
				_maskingInputsData = _selectedImageData.at(index)._maskingInputsData;
			}
		}
	}


	void DataManager::setMaskingImageData(int currentIndex)
	{

		//before save , we should assure that we have 
		// the lastest modification of the currentSelectedIndex 
		// to copy the current index to the vector 


		if (_applicationSetting._lastIndex != currentIndex)
		{
			_selectedImageData.at(_applicationSetting._lastIndex)._maskingPropertiesData = _maskingPropertiesData;
			_selectedImageData.at(_applicationSetting._lastIndex)._maskingEventsData = _maskingEventData;
			_selectedImageData.at(_applicationSetting._lastIndex)._maskingInputsData = _maskingInputsData;

			_maskingPropertiesData = _selectedImageData.at(currentIndex)._maskingPropertiesData; 
			_maskingEventData = _selectedImageData.at(currentIndex)._maskingEventsData;
			_maskingInputsData = _selectedImageData.at(currentIndex)._maskingInputsData;

			_applicationSetting._lastIndex = currentIndex;
			
			_applicationSetting._tagMaskingActivation = false;
			_applicationSetting._mouseTagActivation = false; 

		}
		else
		{
			_maskingPropertiesData = _selectedImageData.at(currentIndex)._maskingPropertiesData;
			_maskingEventData = _selectedImageData.at(currentIndex)._maskingEventsData;
			_maskingInputsData = _selectedImageData.at(currentIndex)._maskingInputsData;

		}
	}

	void DataManager::initializeDataManager()
	{
	
		struct MaskingDataContent maskingData = {
			_maskingInputsData,
			_maskingPropertiesData,
			_maskingEventData
		};

		_selectedImageData.push_back(maskingData);

		_maskingTagData.getMaskingTagData().push_back(std::vector<ImVec2>());

		_applicationSetting = { false, false, false, false , false, 0, 0};
	}

	void DataManager::resetData()
	{
		_selectedImageData.clear(); 
		_imageData.getImageContainer().clear();
		_maskingTagData.getMaskingTagData().clear(); 

		_maskingEventData.initializeEventParamsData(); 
		_maskingInputsData.initializeMaskingInputsData(); 
		_maskingPropertiesData.initializeMaskingPropertiesDataModel(); 

		initializeDataManager();

	}

	void DataManager::swapData(bool sameSelection, int newIndex, int oldIndex)
	{
		
		// save data of the current index 

		if (newIndex != oldIndex)
		{

			_imageData.swapImage(newIndex, oldIndex); 

			struct MaskingDataContent tempValue = {};

			if (sameSelection)
			{
				_selectedImageData.at(oldIndex)._maskingPropertiesData = _maskingPropertiesData;
				_selectedImageData.at(oldIndex)._maskingEventsData = _maskingEventData;
				_selectedImageData.at(oldIndex)._maskingInputsData = _maskingInputsData;
			}
			
			if (!sameSelection && _applicationSetting._currentSelection == newIndex)
			{
				_selectedImageData.at(newIndex)._maskingPropertiesData = _maskingPropertiesData;
				_selectedImageData.at(newIndex)._maskingEventsData = _maskingEventData;
				_selectedImageData.at(newIndex)._maskingInputsData = _maskingInputsData;

			}

			tempValue._maskingPropertiesData = _selectedImageData.at(newIndex)._maskingPropertiesData;
			tempValue._maskingEventsData = _selectedImageData.at(newIndex)._maskingEventsData;
			tempValue._maskingInputsData = _selectedImageData.at(newIndex)._maskingInputsData;


			_selectedImageData.at(newIndex)._maskingPropertiesData = _selectedImageData.at(oldIndex)._maskingPropertiesData;
			_selectedImageData.at(newIndex)._maskingEventsData = _selectedImageData.at(oldIndex)._maskingEventsData;
			_selectedImageData.at(newIndex)._maskingInputsData = _selectedImageData.at(oldIndex)._maskingInputsData;


			_selectedImageData.at(oldIndex)._maskingPropertiesData = tempValue._maskingPropertiesData;
			_selectedImageData.at(oldIndex)._maskingEventsData = tempValue._maskingEventsData;
			_selectedImageData.at(oldIndex)._maskingInputsData = tempValue._maskingInputsData;

			if (sameSelection)
			{
				_maskingPropertiesData = _selectedImageData.at(newIndex)._maskingPropertiesData;
				_maskingEventData = _selectedImageData.at(newIndex)._maskingEventsData;
				_maskingInputsData = _selectedImageData.at(newIndex)._maskingInputsData;
			
				_applicationSetting._lastIndex = newIndex;
				_applicationSetting._currentSelection = newIndex;
				
			}

			if (!sameSelection && _applicationSetting._currentSelection == newIndex)
			{
			
				_maskingPropertiesData = _selectedImageData.at(newIndex)._maskingPropertiesData;
				_maskingEventData = _selectedImageData.at(newIndex)._maskingEventsData;
				_maskingInputsData = _selectedImageData.at(newIndex)._maskingInputsData;

			}

			// swap masking tag 
			_maskingTagData.swapData(newIndex, oldIndex); 

		}

	}

}

