#include <masking_tag_data.h>

namespace data
{

	MaskingTagData::MaskingTagData()
	{};


	void MaskingTagData::addMaskingTag(int currentSelectedIndex, ImVec2 topLeft, ImVec2 bottomRight)
	{
		if (_maskingData.at(currentSelectedIndex).size() == 1)
		{
			_maskingData.at(currentSelectedIndex).at(0) = topLeft; 
			_maskingData.at(currentSelectedIndex).at(1) = bottomRight; 
		}
		else
		{
			
			for (int i = 0; i < _maskingData.size(); ++i)
			{
				for (int j = 0; j < _maskingData.at(i).size(); j+=2)
				{
					if ( (_maskingData.at(i).at(j).x == topLeft.x 
						&& _maskingData.at(i).at(j).y == topLeft.y ) )
						return;
				}
			}

			_maskingData.at(currentSelectedIndex).push_back(topLeft);
			_maskingData.at(currentSelectedIndex).push_back(bottomRight);
		
		}
	}

	void MaskingTagData::removeMaskingTag(int currentSelectionIndex, int currentIndex)
	{
		_maskingData.at(currentSelectionIndex).erase(
			_maskingData.at(currentSelectionIndex).begin() + currentIndex
		);
	
		_maskingData.at(currentSelectionIndex).erase(
			_maskingData.at(currentSelectionIndex).begin() + currentIndex
		);
	}

	std::vector<std::vector<ImVec2>>& MaskingTagData::getMaskingTagData()
	{
		return _maskingData; 
	}


	void MaskingTagData::swapData(int newIndex, int oldIndex)
	{
		std::vector<ImVec2> tmp = _maskingData.at(newIndex); 

		_maskingData.at(newIndex) = _maskingData.at(oldIndex);
		_maskingData.at(oldIndex) = tmp; 

	}

}