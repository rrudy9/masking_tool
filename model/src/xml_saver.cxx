#include <xml_saver.h>

namespace data
{
	XmlSaver::XmlSaver(data::DataManager& dataManager)
		:_dataManager(dataManager)
	{}

	void XmlSaver::exportXmlDoc(std::string filePathName)
	{

		std::size_t found = filePathName.find_last_of("/\\");
		std::string extractFileName = filePathName.substr(found + 1);
		std::string fileName = extractFileName.substr(0, extractFileName.find("."));

		tinyxml2::XMLDocument document;

		tinyxml2::XMLDeclaration* declaration = document.NewDeclaration();
		declaration->SetValue("xml version='1.0' encoding='UTF-8'");
		document.LinkEndChild(declaration);

		tinyxml2::XMLElement* root = document.NewElement("labels");
		document.LinkEndChild(root);


		tinyxml2::XMLElement* globalParameter = document.NewElement("global");
		globalParameter->SetAttribute("w", _dataManager.getImageData().getImageContainer().at(0)._width);
		globalParameter->SetAttribute("h", _dataManager.getImageData().getImageContainer().at(0)._height);
		globalParameter->SetAttribute("progname", fileName.c_str());

		root->LinkEndChild(globalParameter);


		for (size_t i = 0; i < _dataManager.getAllData().size(); ++i)
		{
			tinyxml2::XMLElement* itemData = document.NewElement("label");
			root->LinkEndChild(itemData);

			/// gray filter setting 
			std::string imageName = _dataManager.getImageData().getImageContainer().at(i)._imageName.substr(0, _dataManager.getImageData().getImageContainer().at(i)._imageName.find("."));
			itemData->SetAttribute("name", imageName.c_str());
			itemData->SetAttribute("percent", round(_dataManager.getAllData().at(i)._maskingPropertiesData.getTagSetting()._percentage).c_str());
			itemData->SetAttribute("percent_per_block", round(_dataManager.getAllData().at(i)._maskingPropertiesData.getTagSetting()._blockPercentage).c_str());
			itemData->SetAttribute("grey_filter", (int)_dataManager.getAllData().at(i)._maskingPropertiesData.getGrayFilterData()._activateGrayFilter);
			itemData->SetAttribute("move_block_time_after_btn_press", round(_dataManager.getAllData().at(i)._maskingPropertiesData.getGrayFilterData()._moveAfterPress).c_str());
			itemData->SetAttribute("btn_press_block_time_after_move", round(_dataManager.getAllData().at(i)._maskingPropertiesData.getGrayFilterData()._pressAfterMove).c_str());
			itemData->SetAttribute("key_press_block_time_after_key_press", round(_dataManager.getAllData().at(i)._maskingPropertiesData.getGrayFilterData()._keyboardDelay).c_str());

			/// tags data 

			{
				tinyxml2::XMLElement* maskTag = nullptr;
				for (size_t k = 0; k < _dataManager.getMaskingTagData().getMaskingTagData().at(i).size(); k += 2)
				{

					if (k == 0)
					{
						maskTag = document.NewElement("tags");
						itemData->LinkEndChild(maskTag);
					}

					tinyxml2::XMLElement* maskTagData = document.NewElement("tag");
					maskTagData->SetAttribute("x", (int)_dataManager.getMaskingTagData().getMaskingTagData().at(i).at(k).x);
					maskTagData->SetAttribute("y", (int)_dataManager.getMaskingTagData().getMaskingTagData().at(i).at(k).y);
					maskTagData->SetAttribute("width", (int)_dataManager.getAllData().at(i)._maskingPropertiesData.getTagSetting()._width);
					maskTagData->SetAttribute("height", (int)_dataManager.getAllData().at(i)._maskingPropertiesData.getTagSetting()._height);
					maskTag->LinkEndChild(maskTagData);

					/*double getWidthRect = sqrt(pow((_dataManager.getMaskingTagData().getMaskingTagData().at(i).at(k + 1).x
						- _dataManager.getMaskingTagData().getMaskingTagData().at(i).at(k).x), 2)

						+ pow((_dataManager.getMaskingTagData().getMaskingTagData().at(i).at(k + 1).y
							- _dataManager.getMaskingTagData().getMaskingTagData().at(i).at(k).y), 2));*/


					float rectWidth = _dataManager.getMaskingTagData().getMaskingTagData().at(i).at(k + 1).x - _dataManager.getMaskingTagData().getMaskingTagData().at(i).at(k).x;
					float getHeightRect = _dataManager.getMaskingTagData().getMaskingTagData().at(i).at(k + 1).y - _dataManager.getMaskingTagData().getMaskingTagData().at(i).at(k).y;

					tinyxml2::XMLElement* tagItem = nullptr;
					for (float y = _dataManager.getMaskingTagData().getMaskingTagData().at(i).at(k).y; y < _dataManager.getMaskingTagData().getMaskingTagData().at(i).at(k).y + getHeightRect; ++y)
					{

						if (y == _dataManager.getMaskingTagData().getMaskingTagData().at(i).at(k).y)
						{
							tagItem = document.NewElement("colors");
							maskTagData->LinkEndChild(tagItem);
						}

						for (float x = _dataManager.getMaskingTagData().getMaskingTagData().at(i).at(k).x; x < _dataManager.getMaskingTagData().getMaskingTagData().at(i).at(k).x + rectWidth; ++x)
						{

							tinyxml2::XMLElement* color = document.NewElement("color");
							tagItem->LinkEndChild(color);

							int pixelPosition = (y * _dataManager.getImageData().getImageContainer().at(0)._width + x);

							color->SetAttribute("r", _dataManager.getImageData().getImageContainer().at(i)._imageData[3 * pixelPosition]);
							color->SetAttribute("g", _dataManager.getImageData().getImageContainer().at(i)._imageData[3 * pixelPosition + 1]);
							color->SetAttribute("b", _dataManager.getImageData().getImageContainer().at(i)._imageData[3 * pixelPosition + 2]);

						}
					}
				}
			}



			// all white list 
			{
				tinyxml2::XMLElement* whiteListData = nullptr;

				whiteListData = document.NewElement("whitelist");
				itemData->LinkEndChild(whiteListData);

				saveMaskingInputData(&whiteListData, document, i, checkList::white_list);

			}



			// all black list 
			{
				tinyxml2::XMLElement* blackListData = nullptr;

				blackListData = document.NewElement("blacklist");
				itemData->LinkEndChild(blackListData);

				saveMaskingInputData(&blackListData, document, i, checkList::black_list);

			}

			/// pads value 
			{
				tinyxml2::XMLElement* gamepadAnalogData = document.NewElement("pad");

				tinyxml2::XMLElement* rX = document.NewElement("rx");
				rX->SetAttribute("min", round(_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(0)._minInputValue).c_str());
				rX->SetAttribute("max", round(_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(0)._maxInputValue).c_str());

				tinyxml2::XMLElement* rY = document.NewElement("ry");
				rY->SetAttribute("min", round(_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(1)._minInputValue).c_str());
				rY->SetAttribute("max", round(_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(1)._maxInputValue).c_str());


				tinyxml2::XMLElement* lX = document.NewElement("lx");
				lX->SetAttribute("min", round(_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(2)._minInputValue).c_str());
				lX->SetAttribute("max", round(_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(2)._maxInputValue).c_str());

				tinyxml2::XMLElement* lY = document.NewElement("ly");
				lY->SetAttribute("min", round(_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(3)._minInputValue).c_str());
				lY->SetAttribute("max", round(_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(3)._maxInputValue).c_str());

				gamepadAnalogData->LinkEndChild(rX);
				gamepadAnalogData->LinkEndChild(rY);
				gamepadAnalogData->LinkEndChild(lX);
				gamepadAnalogData->LinkEndChild(lY);

				itemData->LinkEndChild(gamepadAnalogData);
			}

			/// if overlay filter exit 
			{
				if (_dataManager.getAllData().at(i)._maskingPropertiesData.getOverlayFilterData()._activateOverlay)
				{
					tinyxml2::XMLElement* overlayFilterData = document.NewElement("overlay");
					itemData->LinkEndChild(overlayFilterData);

					std::string imageName = _dataManager.getImageData().getImageContainer().at(i)._imageName.substr(0, _dataManager.getImageData().getImageContainer().at(i)._imageName.find("."));
					imageName = imageName + ".bmp";

					overlayFilterData->SetAttribute("filename", imageName.c_str());
					overlayFilterData->SetAttribute("show_on_event", (int)_dataManager.getAllData().at(i)._maskingPropertiesData.getOverlayFilterData()._showOnEvent);
					overlayFilterData->SetAttribute("show_time", round(_dataManager.getAllData().at(i)._maskingPropertiesData.getOverlayFilterData()._showTimeOverlay).c_str());

				}

			}

			/// if exit filter exists
			{
				if (_dataManager.getAllData().at(i)._maskingPropertiesData.getExitFilterData()._activateExitFilter)
				{
					tinyxml2::XMLElement* exitFilterElement = document.NewElement("exitfilter");
					itemData->LinkEndChild(exitFilterElement);

					exitFilterElement->SetAttribute("exit_code", _dataManager.getAllData().at(i)._maskingPropertiesData.getExitFilterData()._exitCode);
					exitFilterElement->SetAttribute("exit_time", round(_dataManager.getAllData().at(i)._maskingPropertiesData.getExitFilterData()._exitTime).c_str());
				}
			}

			/// events data 
			{
				tinyxml2::XMLElement* eventData = nullptr;
				for (size_t j = 0; j < _dataManager.getAllData().at(i)._maskingEventsData.getNumberOfEventList(); ++j)
				{
					if (j == 0)
					{
						eventData = document.NewElement("events");
						itemData->LinkEndChild(eventData);
					}

					for (size_t k = 0; k < _dataManager.getAllData().at(i)._maskingEventsData.getEventsItemData()._eventsMaskingData.at(j).size(); ++k)
					{


						tinyxml2::XMLElement* eventItemData = document.NewElement("event");
						eventItemData->SetAttribute("name", _dataManager.getAllData().at(i)._maskingEventsData.getEventsItemData()._eventItemsLabel.at(j).c_str());
						eventItemData->SetAttribute("param", _dataManager.getAllData().at(i)._maskingEventsData.getEventsItemData()._eventsMaskingData.at(j).at(k)._value.c_str());

						eventData->LinkEndChild(eventItemData);
					}
				}
			}
		}

		document.SaveFile(filePathName.c_str());

	}

	std::string XmlSaver::round(float var)
	{
		float value = ((int)(var * 100 + .5) / 100.0);

		std::string removeTrailingZero = std::to_string(value);

		for (size_t i = removeTrailingZero.length() - 1; i >= 0; --i)
		{
			if (removeTrailingZero.at(i) == '.')
				break;

			if (removeTrailingZero.at(i) == '0' && removeTrailingZero.at(i - 1) != '.')
				removeTrailingZero.erase(removeTrailingZero.begin() + i);

		}

		return  removeTrailingZero;
	}


	void  XmlSaver::saveMouseData(tinyxml2::XMLDocument& document, tinyxml2::XMLElement** mouseData, int index)
	{
		for (size_t k = 0; k < _dataManager.getAllData().at(index)._maskingInputsData.getMouseInputData().getMouseTagData()._mousePoints.size(); k += 2)
		{

			float rectWidth = _dataManager.getAllData().at(index)._maskingInputsData.getMouseInputData().getMouseTagData()._mousePoints.at(k + 1).x - _dataManager.getAllData().at(index)._maskingInputsData.getMouseInputData().getMouseTagData()._mousePoints.at(k).x;

			float getHeightRect = _dataManager.getAllData().at(index)._maskingInputsData.getMouseInputData().getMouseTagData()._mousePoints.at(k + 1).y - _dataManager.getAllData().at(index)._maskingInputsData.getMouseInputData().getMouseTagData()._mousePoints.at(k).y;

			tinyxml2::XMLElement* mouseRect = document.NewElement("rect");
			(*mouseData)->LinkEndChild(mouseRect);

			mouseRect->SetAttribute("x", (int)_dataManager.getAllData().at(index)._maskingInputsData.getMouseInputData().getMouseTagData()._mousePoints.at(k).x);
			mouseRect->SetAttribute("y", (int)_dataManager.getAllData().at(index)._maskingInputsData.getMouseInputData().getMouseTagData()._mousePoints.at(k).y);
			mouseRect->SetAttribute("width", (int)rectWidth);
			mouseRect->SetAttribute("height", (int)getHeightRect);

		}

	
	}


	void XmlSaver::saveKeyboardData(tinyxml2::XMLDocument& document, tinyxml2::XMLElement** keyboardData, int index)
	{
	
		for (size_t k = 0; k < _dataManager.getAllData().at(index)._maskingInputsData.getKeyboardData().getKeyboardInputData()._keyboardInputs.size(); ++k)
		{

			tinyxml2::XMLElement* scanCode = document.NewElement("scancode");
			(*keyboardData)->LinkEndChild(scanCode);

			scanCode->SetAttribute("value", _dataManager.getAllData().at(index)._maskingInputsData.getKeyboardData().getKeyboardInputData()._keyboardInputs.at(k)._vKey.c_str());

		}

	}

	void XmlSaver::saveGamepadButtonData(tinyxml2::XMLDocument& document, tinyxml2::XMLElement** gamepadButtonData, int index)
	{
		for (size_t k = 0; k < _dataManager.getAllData().at(index)._maskingInputsData.getGamepadButtonData().getGamepadButtonData()._gamepadButtonList.size(); ++k)
		{

			tinyxml2::XMLElement* buttonValue = document.NewElement("button");
			(*gamepadButtonData)->LinkEndChild(buttonValue);

			buttonValue->SetAttribute("value", _dataManager.getAllData().at(index)._maskingInputsData.getGamepadButtonData().getGamepadButtonData()._gamepadButtonList.at(k).c_str());
		}

	}


	void XmlSaver::saveMaskingInputData(tinyxml2::XMLElement** inputListData, tinyxml2::XMLDocument& document, int index, int whatList)
	{
	
		///mouse data 
		{
			tinyxml2::XMLElement* mouseData = nullptr;
			mouseData = document.NewElement("mouse");
			(*inputListData)->LinkEndChild(mouseData);

			bool settingMouseList = false;
			if (whatList == checkList::white_list)
				settingMouseList = _dataManager.getAllData().at(index)._maskingInputsData.getMouseInputData().getMouseTagData()._whiteList;
			else
				settingMouseList = _dataManager.getAllData().at(index)._maskingInputsData.getMouseInputData().getMouseTagData()._blackList;

			if (settingMouseList)
				saveMouseData(document, &mouseData, index);

		}


		/// keys value
		{
			tinyxml2::XMLElement* keyboardData = nullptr;
			keyboardData = document.NewElement("key");
			(*inputListData)->LinkEndChild(keyboardData);


			bool settingKeyboardList = false;
			if (whatList == checkList::white_list)
				settingKeyboardList = _dataManager.getAllData().at(index)._maskingInputsData.getKeyboardData().getKeyboardInputData()._whiteList;
			else
				settingKeyboardList = _dataManager.getAllData().at(index)._maskingInputsData.getKeyboardData().getKeyboardInputData()._blackList;


			if (settingKeyboardList)
				saveKeyboardData(document, &keyboardData, index);
		}


		/// gamepad  value
		{
			tinyxml2::XMLElement* gamepadButtonData = nullptr;
			gamepadButtonData = document.NewElement("gamepad");
			(*inputListData)->LinkEndChild(gamepadButtonData);


			bool settingGamepadButtonList = false;
			if (whatList == checkList::white_list)
				settingGamepadButtonList = _dataManager.getAllData().at(index)._maskingInputsData.getGamepadButtonData().getGamepadButtonData()._whiteList;
			else
				settingGamepadButtonList = _dataManager.getAllData().at(index)._maskingInputsData.getGamepadButtonData().getGamepadButtonData()._blackList;

			if (settingGamepadButtonList)
				saveGamepadButtonData(document, &gamepadButtonData, index);

		}

	}

}