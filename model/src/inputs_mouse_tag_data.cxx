#include <inputs_mouse_tag_data.h>

namespace data
{

	InputsMouseTagData::InputsMouseTagData()
	{}; 


	void InputsMouseTagData::initMouseTagValues()
	{
		_mouseTagData = { true, false }; 
		_mouseTagData._mousePoints.clear(); 

	}

	struct MouseTagData& InputsMouseTagData::getMouseTagData()
	{
		return _mouseTagData; 
	}

	void InputsMouseTagData::addMousePoints(ImVec2 topLeft, ImVec2 bottomRight)
	{
		_mouseTagData._mousePoints.push_back(topLeft);
		_mouseTagData._mousePoints.push_back(bottomRight);
	}

	void InputsMouseTagData::removeMousePoints(int indexOfMousePoints)
	{
		_mouseTagData._mousePoints.erase(_mouseTagData._mousePoints.begin() + indexOfMousePoints);
		_mouseTagData._mousePoints.erase(_mouseTagData._mousePoints.begin() + indexOfMousePoints);
	}

}