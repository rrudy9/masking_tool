#include <images_data.h>

namespace data
{

	ImagesData::ImagesData()
		:_imagesContainer(std::vector<ImageTexture>())
	{
		
	};

	std::vector<ImageTexture>& ImagesData::getImageContainer()
	{
		return _imagesContainer; 
	}

	bool ImagesData::loadImageFromFile(const std::string& filePath)
	{
		struct ImageTexture imageLoaded = {0};
		
		std::size_t found = filePath.find_last_of("/\\");
		imageLoaded._imageName  = filePath.substr(found + 1);

		std::size_t extention= filePath.find_last_of(".");

#ifdef DIRECTX_FOUND
		bool res = loadTextureFromFile(filePath, &imageLoaded._imageTexture, imageLoaded._width, imageLoaded._height, imageLoaded._compression, &imageLoaded._imageData, 4);

#else
		int requestComponent = 0;


		if (filePath.substr(extention + 1) == "PNG"
			|| filePath.substr(extention + 1) == "png"
			|| filePath.substr(extention + 1) == "bmp"
			)
		{
			requestComponent = 4;
	}
		else if (filePath.substr(extention + 1) == "jpg"
			|| filePath.substr(extention + 1) == "jpeg"
			)
		{

			requestComponent = 3;
		}

		bool res = loadTextureFromFile(filePath, imageLoaded._imageTexture, imageLoaded._width, imageLoaded._height, imageLoaded._compression, &imageLoaded._imageData, requestComponent);
#endif // DIRECTX_FOUND
		IM_ASSERT(res);

		if (_imagesContainer.size() >= 1)
		{
			if (imageLoaded._width != _imagesContainer.at(0)._width
				|| imageLoaded._height != _imagesContainer.at(0)._height)
			{
				freeImage(&imageLoaded._imageData);
				return false;
			}
		}

		_imagesContainer.push_back(imageLoaded);

		return true; 
	}

	void ImagesData::removeImage(int indexImage)
	{
		freeImage(&_imagesContainer.at(indexImage)._imageData);
		_imagesContainer.erase(_imagesContainer.begin() + indexImage); 
	}

	void ImagesData::swapImage(int newIndex, int oldIndex)
	{
		ImageTexture tmp = _imagesContainer[newIndex];
		
		_imagesContainer[newIndex] = _imagesContainer[oldIndex];
		_imagesContainer[oldIndex] = tmp;

	}

	ImagesData::~ImagesData()
	{
		for (int i = 0; i < _imagesContainer.size(); ++i)
		{
			freeImage(&_imagesContainer.at(i)._imageData);
		}
	}

	

}