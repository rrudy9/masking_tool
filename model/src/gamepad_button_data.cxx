#include <gamepad_button_data.h>

namespace data
{

	GamepadButtonData::GamepadButtonData()
	{};

	void GamepadButtonData::initializeValue()
	{
		_gamepadButtonData = {true, false };
		_gamepadButtonData._gamepadButtonList.clear();
	}

	struct GamepadClickableData& GamepadButtonData::getGamepadButtonData()
	{
		return _gamepadButtonData; 
	}

	void GamepadButtonData::addGamepadButton(const int& gamepadValue)
	{
		_gamepadButtonData._gamepadButtonList.push_back(std::to_string(gamepadValue));
	}

	void GamepadButtonData::removeGamepadButton(int indexValue)
	{
		_gamepadButtonData._gamepadButtonList.erase(_gamepadButtonData._gamepadButtonList.begin() + indexValue);
	}


}