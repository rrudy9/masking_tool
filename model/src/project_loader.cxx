
#include <project_loader.h>

namespace data
{
	ProjectLoader::ProjectLoader(
		DataManager& dataManager
	)
		:_dataManager(dataManager)
	{

	};

	void ProjectLoader::loadProject(const std::string& filePath, const std::string& filePathName)
	{
		try {

			std::fstream readFile(filePathName, std::ios::in | std::ios::binary);

			if (!readFile.is_open()) {
				throw::std::exception("Can't open file for loading\n");
			}


			_dataManager.getAllData().clear();
			_dataManager.getImageData().getImageContainer().clear();
			_dataManager.getMaskingTagData().getMaskingTagData().clear();

			std::string fileExtension = filePathName.substr(filePathName.find_last_of(".") + 1);
			
			if (fileExtension != "ptp")
			{
				readFile.read((char*)&_dataManager.getApplicationSetting(), sizeof(_dataManager.getApplicationSetting()));

				_dataManager.getApplicationSetting()._firstSelection = false;

				size_t allDataSize = 0;

				readFile.read((char*)&allDataSize, sizeof(allDataSize));
				_dataManager.getAllData().resize(allDataSize);

				for (size_t i = 0; i < allDataSize; ++i)
				{
					/// we start by reading the data structures in masking properties
					readFile.read((char*)&_dataManager.getAllData()[i]._maskingPropertiesData, sizeof(MaskingPropertiesData));

					/// read event data
					{
						for (size_t j = 0; j < _dataManager.getAllData().at(i)._maskingEventsData.getNumberOfEventList(); ++j)
						{
							size_t eventSizeItem = 0;
							readFile.read((char*)&eventSizeItem, sizeof(eventSizeItem));

							_dataManager.getAllData().at(i)._maskingEventsData.getEventsItemData()._eventsMaskingData.at(j).resize(eventSizeItem);
							for (int k = 0; k < eventSizeItem; ++k)
							{
								size_t strSize = 0;
								readFile.read((char*)&strSize, sizeof(strSize));

								_dataManager.getAllData()[i]._maskingEventsData.getEventsItemData()._eventsMaskingData.at(j).at(k)._value.resize(strSize);
								readFile.read(&_dataManager.getAllData()[i]._maskingEventsData.getEventsItemData()._eventsMaskingData.at(j).at(k)._value[0], strSize);
							}


							size_t eventLabel = 0;
							readFile.read((char*)&eventLabel, sizeof(eventLabel));
							_dataManager.getAllData().at(i)._maskingEventsData.getEventsItemData()._eventItemsLabel.at(j).resize(eventLabel);

							readFile.read(&_dataManager.getAllData().at(i)._maskingEventsData.getEventsItemData()._eventItemsLabel.at(j)[0], eventLabel);



						}
					}

					// input data 
					{
						///read input mouse data
						{
							readFile.read((char*)&_dataManager.getAllData()[i]._maskingInputsData.getMouseInputData().getMouseTagData()._blackList, sizeof(bool));
							readFile.read((char*)&_dataManager.getAllData()[i]._maskingInputsData.getMouseInputData().getMouseTagData()._whiteList, sizeof(bool));

							size_t sizeOfMouseVectorData = 0;

							readFile.read((char*)&sizeOfMouseVectorData, sizeof(sizeOfMouseVectorData));

							ImVec2 mousePoints = { 0,0 };

							for (int k = 0; k < sizeOfMouseVectorData; ++k)
							{
								readFile.read((char*)&mousePoints, sizeof(mousePoints));
								_dataManager.getAllData().at(i)._maskingInputsData.getMouseInputData().getMouseTagData()._mousePoints.push_back(mousePoints);
							}

						}

						///read gamepad button
						{
							readFile.read((char*)&_dataManager.getAllData()[i]._maskingInputsData.getGamepadButtonData().getGamepadButtonData()._blackList, sizeof(bool));
							readFile.read((char*)&_dataManager.getAllData()[i]._maskingInputsData.getGamepadButtonData().getGamepadButtonData()._whiteList, sizeof(bool));

							size_t gamepadButtonListSize = 0;
							readFile.read((char*)&gamepadButtonListSize, sizeof(gamepadButtonListSize));

							_dataManager.getAllData().at(i)._maskingInputsData.getGamepadButtonData().getGamepadButtonData()._gamepadButtonList.resize(gamepadButtonListSize);

							for (size_t k = 0; k < gamepadButtonListSize; ++k)
							{
								size_t strSize = 0;
								readFile.read((char*)&strSize, sizeof(strSize));
								_dataManager.getAllData().at(i)._maskingInputsData.getGamepadButtonData().getGamepadButtonData()._gamepadButtonList[k].resize(strSize);

								readFile.read(&_dataManager.getAllData().at(i)._maskingInputsData.getGamepadButtonData().getGamepadButtonData()._gamepadButtonList[k][0], strSize);

							}
						}

						/// gamepad analog data 
						{

							size_t gamepadAnalogDataSize = 0;
							readFile.read((char*)&gamepadAnalogDataSize, sizeof(gamepadAnalogDataSize));

							for (size_t k = 0; k < gamepadAnalogDataSize; ++k)
							{
								readFile.read((char*)&_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(k)._maxInputValue, sizeof(float));
								readFile.read((char*)&_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(k)._minInputValue, sizeof(float));
							}
						}

						// keyboard inputs data
						{
							readFile.read((char*)&_dataManager.getAllData()[i]._maskingInputsData.getKeyboardData().getKeyboardInputData()._blackList, sizeof(bool));
							readFile.read((char*)&_dataManager.getAllData()[i]._maskingInputsData.getKeyboardData().getKeyboardInputData()._whiteList, sizeof(bool));


							size_t keyboardInputDataSize = 0;
							readFile.read((char*)&keyboardInputDataSize, sizeof(keyboardInputDataSize));

							_dataManager.getAllData().at(i)._maskingInputsData.getKeyboardData().getKeyboardInputData()._keyboardInputs.resize(keyboardInputDataSize);


							for (size_t k = 0; k < keyboardInputDataSize; ++k)
							{
								readFile.read((char*)&_dataManager.getAllData().at(i)._maskingInputsData.getKeyboardData().getKeyboardInputData()._keyboardInputs.at(k)._scanCode, sizeof(UINT));

								/// vkey
								size_t vKeySize = 0;
								readFile.read((char*)&vKeySize, sizeof(vKeySize));
								_dataManager.getAllData().at(i)._maskingInputsData.getKeyboardData().getKeyboardInputData()._keyboardInputs.at(k)._vKey.resize(vKeySize);

								readFile.read(&_dataManager.getAllData().at(i)._maskingInputsData.getKeyboardData().getKeyboardInputData()._keyboardInputs.at(k)._vKey[0], vKeySize);


								/// vkeyName
								size_t vKeyNameSize = 0;
								readFile.read((char*)&vKeyNameSize, sizeof(vKeyNameSize));
								_dataManager.getAllData().at(i)._maskingInputsData.getKeyboardData().getKeyboardInputData()._keyboardInputs.at(k)._vKeyName.resize(vKeyNameSize);

								readFile.read(&_dataManager.getAllData().at(i)._maskingInputsData.getKeyboardData().getKeyboardInputData()._keyboardInputs.at(k)._vKeyName[0], vKeyNameSize);

							}

						}

					}

				}

				std::vector<std::string> imageName = {};

				size_t imageVectorSize = 0;
				readFile.read((char*)&imageVectorSize, sizeof(imageVectorSize));

				imageName.resize(imageVectorSize);

				for (size_t i = 0; i < imageVectorSize; ++i)
				{
					size_t strSize = 0;
					readFile.read((char*)&strSize, sizeof(strSize));
					imageName[i].resize(strSize);
					readFile.read(&imageName[i][0], strSize);

				}

				size_t maskTagSize = 0;
				readFile.read((char*)&maskTagSize, sizeof(maskTagSize));

				_dataManager.getMaskingTagData().getMaskingTagData().resize(maskTagSize);

				for (size_t i = 0; i < maskTagSize; ++i)
				{
					size_t pointsSize = 0;
					readFile.read((char*)&pointsSize, sizeof(pointsSize));

					ImVec2 points = { 0,0 };

					for (int j = 0; j < pointsSize; ++j)
					{
						readFile.read((char*)&points, sizeof(points));
						_dataManager.getMaskingTagData().getMaskingTagData().at(i).push_back(points);
					}
				}


				for (int i = 0; i < imageName.size(); ++i)
				{
					std::string fileName = imageName.at(i).substr(0, imageName.at(i).find("."));

					std::string pathImage = filePath + "\\" + fileName + ".bmp";
					_dataManager.getImageData().loadImageFromFile(pathImage);
				}

				readFile.close();
				if (!readFile.good()) {
					throw::std::exception("Error occurred at reading time!\n");
				}

			}
			else
			{
				// read old application project data 
				pictoolproto::Project project;
				
				std::vector<std::string> imageName = {};


				if (!project.ParseFromIstream(&readFile))
				{
					throw::std::exception("Error occurred at reading time!\n");
				}

				int version = project.version();

				_dataManager.getAllData().resize(project.items_size());

				for (int i = 0; i < project.items_size(); i++)
				{
					pictoolproto::Item* item = project.mutable_items(i);

					if (item->path().length() != 0)
						imageName.push_back(item->path());


					{
						struct TagSettingProperties tagSettingProperties = {};

						tagSettingProperties._percentage = item->tag_percent();
						tagSettingProperties._blockPercentage = item->block_percent();
						tagSettingProperties._width = 16;
						tagSettingProperties._height = 16;

						_dataManager.getAllData().at(i)._maskingPropertiesData.getTagSetting() = tagSettingProperties;

					}


					{
						struct GrayFilterProperties grayFilterProperties = {};

						// gray filter
						if (version <= DEPRECATED_VERSION)
						{
							float gray_filter_deprecated_val = item->gray_filter();
							grayFilterProperties._activateGrayFilter = gray_filter_deprecated_val == 0 ? false : true;
							grayFilterProperties._moveAfterPress = gray_filter_deprecated_val;
							grayFilterProperties._pressAfterMove = 0;
							grayFilterProperties._keyboardDelay = 0;
						}
						else
						{
							grayFilterProperties._activateGrayFilter = item->gray_filter_enabled();
							grayFilterProperties._moveAfterPress = item->move_after_press();
							grayFilterProperties._pressAfterMove = item->press_after_move();
							grayFilterProperties._keyboardDelay = item->keyboard_delay();
						}

						_dataManager.getAllData().at(i)._maskingPropertiesData.getGrayFilterData() = grayFilterProperties;
					}

					{
						struct OverlayFilterProperties overlayFilterProperties = {};

						//Overlay
						overlayFilterProperties._activateOverlay = item->overlay();
						overlayFilterProperties._showOnEvent = item->show_on_event();
						overlayFilterProperties._showTimeOverlay = item->show_time();

						_dataManager.getAllData().at(i)._maskingPropertiesData.getOverlayFilterData() = overlayFilterProperties;
					}

					{

						
						// Add Pad
						_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(0)._minInputValue = version <= DEPRECATED_VERSION ? -1 : item->rx_min();
						_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(0)._maxInputValue = version <= DEPRECATED_VERSION ? 1 : item->rx_max();

						_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(1)._minInputValue = version <= DEPRECATED_VERSION ? -1 : item->ry_min();
						_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(1)._maxInputValue = version <= DEPRECATED_VERSION ? 1 : item->ry_max();

						_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(2)._minInputValue = version <= DEPRECATED_VERSION ? -1 : item->lx_min();
						_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(2)._maxInputValue = version <= DEPRECATED_VERSION ? 1 : item->lx_max();

						_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(3)._minInputValue = version <= DEPRECATED_VERSION ? -1 : item->ly_min();
						_dataManager.getAllData().at(i)._maskingInputsData.getGamepadAnalogData().at(3)._maxInputValue = version <= DEPRECATED_VERSION ? 1 : item->ly_max();

					}


					// Exit Filter
					{
						struct ExitFilterProperties exitFilterProperties = {};

						if (version <= DEPRECATED_VERSION_2)
						{
							exitFilterProperties._activateExitFilter = false;
							exitFilterProperties._exitCode = 0;
							exitFilterProperties._exitTime = 1.0;

						}
						else
						{
							exitFilterProperties._activateExitFilter = item->exit_filter();
							exitFilterProperties._exitCode = item->exit_code();
							exitFilterProperties._exitTime = item->exit_time();
						}

						_dataManager.getAllData().at(i)._maskingPropertiesData.getExitFilterData() = exitFilterProperties;
					}

					{
						for (int j = 0; j < item->tags_size(); ++j)
						{

							if (j == 0)
								_dataManager.getMaskingTagData().getMaskingTagData().resize(i + 1);

							const pictoolproto::Point& p = item->tags(j);

							ImVec2 point = ImVec2(p.x(), p.y());

							_dataManager.getMaskingTagData().getMaskingTagData().at(i).push_back(point);

							// add the bottom right points
							ImVec2 botRightPoint = ImVec2(p.x() + TAG_DEFAULT_WIDTH, p.y() + TAG_DEFAULT_HEIGHT);
							_dataManager.getMaskingTagData().getMaskingTagData().at(i).push_back(botRightPoint);

						}
					}


					{
						struct MouseTagData mouseTagData = {};

						for (int j = 0; j < item->mouse_white_size(); ++j)
						{

							if (j == 0)
							{
								mouseTagData._whiteList = true;
								mouseTagData._blackList = false;
							}

							pictoolproto::Rect* r = (item->pictoolproto::Item::mutable_mouse_white)(j);

							ImVec2 mousePoints = ImVec2(r->x(), r->y());
							mouseTagData._mousePoints.push_back(mousePoints);

							// bottom right

							ImVec2 bottomRight = ImVec2(r->x() + r->width(), r->y() + r->height());
							mouseTagData._mousePoints.push_back(bottomRight);
						}

						if (item->mouse_white_size() != 0)
							_dataManager.getAllData().at(i)._maskingInputsData.getMouseInputData().getMouseTagData() = mouseTagData;

						// we can have just one , the black list will take over .. 

						for (int j = 0; j < item->mouse_black_size(); ++j)
						{

							if (j == 0)
							{
								mouseTagData._whiteList = false;
								mouseTagData._blackList = true;
							}

							pictoolproto::Rect* r = (item->pictoolproto::Item::mutable_mouse_black)(j);

							ImVec2 mousePoints = ImVec2(r->x(), r->y());
							mouseTagData._mousePoints.push_back(mousePoints);

							// bottom right

							ImVec2 bottomRight = ImVec2(r->x() + r->width(), r->y() + r->height());
							mouseTagData._mousePoints.push_back(bottomRight);
						}

						if (item->mouse_black_size() != 0)
							_dataManager.getAllData().at(i)._maskingInputsData.getMouseInputData().getMouseTagData() = mouseTagData;

					}

					{

						struct KeyboardInputData keyboardInputData = {};


						for (int j = 0; j < item->keys_white_size(); ++j)
						{

							struct KeyboardInput keyboardInput = {};
							if (j == 0)
							{
								keyboardInputData._blackList = false;
								keyboardInputData._whiteList = true;
							}

							pictoolproto::Keys* keys = (item->pictoolproto::Item::mutable_keys_white)(j);

							keyboardInput._scanCode = keys->value(0);
							keyboardInput._vKey = std::to_string(keys->value(0));
							keyboardInput._vKeyName = keys->name();

							keyboardInputData._keyboardInputs.push_back(keyboardInput);
						}

						if (item->keys_white_size() != 0)
							_dataManager.getAllData().at(i)._maskingInputsData.getKeyboardData().getKeyboardInputData() = keyboardInputData;



						for (int j = 0; j < item->keys_black_size(); ++j)
						{

							struct KeyboardInput keyboardInput = {};
							if (j == 0)
							{
								keyboardInputData._blackList = true;
								keyboardInputData._whiteList = false;
							}

							pictoolproto::Keys* keys = (item->pictoolproto::Item::mutable_keys_black)(j);

							keyboardInput._scanCode = keys->value(0);
							keyboardInput._vKey = std::to_string(keys->value(0));
							keyboardInput._vKeyName = keys->name();

							keyboardInputData._keyboardInputs.push_back(keyboardInput);
						}

						if (item->keys_black_size() != 0)
							_dataManager.getAllData().at(i)._maskingInputsData.getKeyboardData().getKeyboardInputData() = keyboardInputData;

					}
					
					
					// Add GameButtons WHITE / BLACK List
					{
					
						struct GamepadClickableData gamepadButton = {};

						for (int j = 0; j < item->gamebuttons_white_size(); ++j)
						{
							if (j == 0)
							{
								gamepadButton._whiteList = true;
								gamepadButton._blackList = false;
							}

							gamepadButton._gamepadButtonList.push_back(std::to_string(item->gamebuttons_white(j)));

						}

						if (item->gamebuttons_white_size() != 0)
							_dataManager.getAllData().at(i)._maskingInputsData.getGamepadButtonData().getGamepadButtonData() = gamepadButton;



						for (int j = 0; j < item->gamebuttons_black_size(); ++j)
						{
							if (j == 0)
							{
								gamepadButton._whiteList = false;
								gamepadButton._blackList = true;
							}

							gamepadButton._gamepadButtonList.push_back(std::to_string(item->gamebuttons_black(j)));
						}

						if (item->gamebuttons_black_size() != 0)
							_dataManager.getAllData().at(i)._maskingInputsData.getGamepadButtonData().getGamepadButtonData() = gamepadButton;
				
					}

					{
					

						// i suppose event before the event was show this , show that.. 
						// we gonna proceed this way .. 

						_dataManager.getAllData().at(i)._maskingEventsData.initializeEventParamsData(); 

						// Add Events
						for (int j = 0; j < item->events_size(); ++j)
						{

							const pictoolproto::Event& p = item->events(j);

							for (int k = 0; k < _dataManager.getAllData().at(i)._maskingEventsData.getEventsItemData()._eventItemsLabel.size(); ++k)
							{
								std::string str = _dataManager.getAllData().at(i)._maskingEventsData.getEventsItemData()._eventItemsLabel.at(k);
								str.erase(std::remove(str.begin(), str.end(), ' '), str.end());

								if (str == p.name())
								{
									_dataManager.getAllData().at(i)._maskingEventsData.fillEventParameter(k, p.param()); 
									break; 
								}
							}
						}
					}
				}

				readFile.close();

				_dataManager.getApplicationSetting()._firstSelection = false;
				_dataManager.getApplicationSetting()._currentSelection = 0;


				for (int i = 0; i < imageName.size(); ++i)
				{
					std::string pathImage = filePath + "\\" + imageName.at(i);
					_dataManager.getImageData().loadImageFromFile(pathImage);
				}

			}

			_dataManager.setMaskingImageData(_dataManager.getApplicationSetting()._currentSelection);

		}
		catch (const std::exception& exception)
		{
			std::cerr << exception.what() << '\n';
		};
	}

}