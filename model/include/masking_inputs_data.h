#ifndef MASKING_INPUTS_DATA_H
#define MASKING_INPUTS_DATA_H

#include <iostream>
#include <Windows.h>

#include <vector>
#include <string>

#include <exception>         

#ifdef CPPUTEST_AVAILABLE
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

#include <inputs_keyboard_data.h>
#include <gamepad_button_data.h>
#include <inputs_mouse_tag_data.h>

namespace data
{

	struct GamepadAnalogInput
	{
		float _minInputValue;
		float _maxInputValue;
	};

	class MaskingInputsData 
	{
		public:
		
			MaskingInputsData(); 

			void initializeMaskingInputsData();

			InputsKeyboardData& getKeyboardData();
			
			GamepadButtonData& getGamepadButtonData();

			InputsMouseTagData& getMouseInputData();

			std::vector<GamepadAnalogInput>& getGamepadAnalogData(); 

		private:
			
			InputsKeyboardData _keyboardData;
	
			GamepadButtonData _gamepadButtonData;

			InputsMouseTagData _mouseTagData; 

			std::vector<GamepadAnalogInput> _gamepadAnalogList;


	};
	
}


#endif