#pragma once

#include <iostream>
#include <mutex>

#include <graphical_lib.h>

class WindowsManager
{
public:
	WindowsManager(); 

	WindowsManager(WindowsManager& other) = delete;
	void operator=(const WindowsManager&) = delete;

	static WindowsManager* getInstance();
	static void resetInstance();

	bool& getWindowMovingValue(); 


#ifdef DIRECTX_FOUND

	ID3D11Device* g_pd3dDevice = NULL;
	ID3D11DeviceContext* g_pd3dDeviceContext = NULL;
	IDXGISwapChain* g_pSwapChain = NULL;
	ID3D11RenderTargetView* g_mainRenderTargetView = NULL;

	// for winProc
	std::string _vKeyCharName = "";
	std::string _vKeyValue = "";
	UINT _scanCode = 0; 


#else
	const GLFWvidmode* _mode = nullptr;
	GLFWmonitor* _monitor = nullptr;
#endif


private:

	static WindowsManager* _windowsManagerInstance; 
	static std::mutex _mutex; 

	bool _startMove = true; 





};