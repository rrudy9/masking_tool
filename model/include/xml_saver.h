#pragma once

#include <data_manager.h>
#include <tinyxml2.h>

#include <math.h>

namespace data
{

	class XmlSaver
	{
	public :

		enum checkList { white_list = 0, black_list };

		XmlSaver(data::DataManager& dataManager);

		void exportXmlDoc(std::string filePathName); 

	private:

		data::DataManager& _dataManager; 

		std::string round(float var); 

		void saveMouseData(tinyxml2::XMLDocument& document, tinyxml2::XMLElement** mouseData, int index);

		void saveKeyboardData(tinyxml2::XMLDocument& document, tinyxml2::XMLElement** keyboardData, int index);

		void saveGamepadButtonData(tinyxml2::XMLDocument& document, tinyxml2::XMLElement** gamepadButtonData, int index);

		void saveMaskingInputData(tinyxml2::XMLElement** inputListData, tinyxml2::XMLDocument& document, int index, int whatList);

	};

}