#pragma once


#include<iostream>
#include<fstream>
#include <string>

#include <data_manager.h>

#include <image_writer.h>

namespace data
{
	class ProjectSaver
	{
	public:
		ProjectSaver(
			DataManager& dataManager
		); 
		
		void saveProject(const std::string& filePath, const std::string& filePathName); 

	private:
		DataManager& _dataManager; 
		
	};

}