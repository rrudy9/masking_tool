#ifndef IMAGES_DATA_H
#define IMAGES_DATA_H

#include <iostream>
#include <graphical_lib.h>

#include <image_loader.h>

#include <vector>


namespace data
{

	struct ImageTexture
	{
#ifdef DIRECTX_FOUND
		ID3D11ShaderResourceView* _imageTexture;
#else
		unsigned int _imageTexture;
#endif
		int _width; 
		int _height; 
		unsigned char* _imageData; 
		int _compression; 
		std::string _imageName; 


	};

	class ImagesData
	{
	public :
		ImagesData(); 
		~ImagesData();

		bool loadImageFromFile(const std::string& filePath);

		void removeImage(int indexImage);

		std::vector<ImageTexture>& getImageContainer(); 

		void swapImage(int newIndex, int oldIndex); 

	private:
		std::vector<ImageTexture> _imagesContainer;

	};

}

#endif