#pragma once

#include <vector>
#include <graphical_lib.h>

namespace data
{

	struct MouseTagData
	{
		bool _blackList; 
		bool _whiteList; 
		
		std::vector<ImVec2> _mousePoints;

	};

	class InputsMouseTagData
	{

	public:
		InputsMouseTagData(); 

		void initMouseTagValues(); 

		void addMousePoints(ImVec2 topLeft, ImVec2 bottomRight);

		void removeMousePoints(int indexOfMousePoints);

		struct MouseTagData& getMouseTagData();

	private:
		
		struct MouseTagData _mouseTagData; 

	};
}