#ifndef CONTENT_MANAGER_H
#define CONTENT_MANAGER_H

#include <masking_events_data.h>
#include <masking_inputs_data.h>
#include <masking_properties_data.h>
#include <images_data.h>

#include <iostream>

#ifdef CPPUTEST_AVAILABLE
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif


#include <vector>

#include <masking_tag_data.h>

namespace data
{

	struct MaskingDataContent
	{
		MaskingInputsData _maskingInputsData;

		MaskingPropertiesData _maskingPropertiesData;

		MaskingEventsData _maskingEventsData;

	};

	struct ApplicationSetting
	{
		bool _picToolsActivation;
		bool _mouseTagActivation; 
		bool _tagMaskingActivation;

		bool _imageSelected; 

		bool _firstSelection; 

		int _lastIndex;
		int _currentSelection; 

	};


	class DataManager
	{
	public:
		DataManager(); 

		void setDataForEachPicture();

		void setMaskingImageData(int currentIndex);
		
		MaskingInputsData& getInputsData(); 
		
		MaskingPropertiesData& getPropertiesData();
		
		MaskingEventsData& getEventsData();
		
		ImagesData& getImageData();

		struct ApplicationSetting& getApplicationSetting(); 

		MaskingTagData& getMaskingTagData();

		void setLastDataBeforeSave();

		std::vector<MaskingDataContent>& getAllData();

		void removeDataForSelectedImage(int index); 

		void resetData(); 

		void swapData(bool sameSelection, int newIndex, int oldIndex); 

	private:

		void initializeDataManager(); 

		MaskingInputsData _maskingInputsData;

		MaskingPropertiesData _maskingPropertiesData;

		MaskingEventsData _maskingEventData;

		ImagesData _imageData;

		std::vector<MaskingDataContent> _selectedImageData;

		struct ApplicationSetting _applicationSetting; 

		// since the masking tag should be accessed from every item (picture) 
		//  it will be declared in the data manager
		MaskingTagData _maskingTagData;

	};

}

#endif