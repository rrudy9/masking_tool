#pragma once

#include <iostream>
#include <vector>
#include <string>

namespace data
{ 
	struct GamepadClickableData
	{
		bool _blackList = true;
		bool _whiteList = false;

		std::vector<std::string> _gamepadButtonList;

	};

	class GamepadButtonData
	{
	public:

		GamepadButtonData();

		void initializeValue(); 

		void addGamepadButton(const int& gamepadValue);

		void removeGamepadButton(int indexValue);

		struct GamepadClickableData& getGamepadButtonData();

	private:
		struct GamepadClickableData _gamepadButtonData;

	};
}