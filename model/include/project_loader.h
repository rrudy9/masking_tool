#pragma once 

#include<iostream>
#include<fstream>
#include <string>

#include <image_loader.h>
#include <data_manager.h>

#include <proto/pictool.pb.h>

#define DEPRECATED_VERSION              2
#define DEPRECATED_VERSION_2            3
#define CURRENT_PROJECT_VERSION         7


#define TAG_DEFAULT_WIDTH				16
#define TAG_DEFAULT_HEIGHT				16


namespace data
{
	class ProjectLoader
	{
	public:
		ProjectLoader(
			DataManager& dataManager			
		);

		void loadProject(const std::string& filePath, const std::string& filePathName);


	private:

		DataManager& _dataManager; 
	};
}