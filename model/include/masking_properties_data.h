#ifndef MASKING_PROPERTIES_DATA_H
#define MASKING_PROPERTIES_DATA_H

#include <iostream>
#include <vector>

#ifdef CPPUTEST_AVAILABLE
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

namespace data {
	


	struct TagSettingProperties
	{
		// tag setting
		int _width;
		int _height;
		float _percentage;
		float _blockPercentage;
	};

	struct GrayFilterProperties
	{
		// gray filter properties 
		bool _activateGrayFilter;
		float _moveAfterPress;
		float _pressAfterMove;
		float _keyboardDelay;

	};

	struct OverlayFilterProperties
	{
		//overlay filter 
		bool _activateOverlay;
		bool _showOnEvent;
		float _showTimeOverlay;

	};

	struct ExitFilterProperties
	{
		// exit filter 
		bool _activateExitFilter;
		uint8_t _exitCode;
		float _exitTime;

	};

	
	class MaskingPropertiesData  
	{

	public:
		MaskingPropertiesData(); 
	
		void initializeMaskingPropertiesDataModel();

		struct TagSettingProperties& getTagSetting();
		struct GrayFilterProperties& getGrayFilterData();
		struct OverlayFilterProperties& getOverlayFilterData();
		struct ExitFilterProperties& getExitFilterData();


	private:

		struct TagSettingProperties _tagSetting;

		struct GrayFilterProperties _grayFilter; 
		struct OverlayFilterProperties _overlayFilter;
		struct ExitFilterProperties _exitFilter;

	};

}
	
#endif