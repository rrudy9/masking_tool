#ifndef MASKING_EVENTS_H
#define MASKING_EVENTS_H

#include <iostream>
#include <vector>
#include <string>


#ifdef CPPUTEST_AVAILABLE
#include "CppUTest/MemoryLeakDetectorMallocMacros.h"
#endif

#include <vector>

namespace data
{

	struct EventItem
	{
		std::string _value;
	};

	struct EventItemData
	{
		std::vector<std::string> _eventItemsLabel;
		std::vector<std::vector<EventItem>> _eventsMaskingData;

	};

	class MaskingEventsData
	{
	public:
		MaskingEventsData(); 

		void initializeEventParamsData();
		void fillEventParameter(int currentIndexSelected, const std::string& params);
		void removeEventParameter(int rowValue, int columnValue);

		int getNumberOfEventList(); 

		struct EventItemData& getEventsItemData();
	
	private:

		int _numberOfEvent;

		struct EventItemData _eventData; 

		void initializeEventItemsLabel(std::vector<std::string>& items, uint8_t sizeOFArray);

	};


}

#endif