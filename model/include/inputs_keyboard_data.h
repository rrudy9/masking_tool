#pragma once 

#include <iostream>
#include <Windows.h>
#include <vector>
#include <string>

#include <windows_manager.h>

namespace data
{
	struct KeyboardInput
	{
		UINT _scanCode;
		std::string _vKey;
		std::string _vKeyName;

	};

	
	struct KeyboardInputData
	{
		bool _blackList = true;
		bool _whiteList = false;

		std::vector<KeyboardInput> _keyboardInputs;

	};


	class InputsKeyboardData
	{
	public:

		enum keysList { deleteKey = 0 };

		InputsKeyboardData();

		void initializeValue(); 

		void openKeyboadInput(); 

		void startKeyboardInput(std::string& vKeyValue, std::string& vKeyCharName, UINT& getScanCode);

		void closeKeyboardInput();

		void insertKeyboardInput(const std::string& vKey, const std::string& vKeyName, const UINT& scanCode);

		void removeKeyboardInput(int indexOfItem);

		struct KeyboardInputData& getKeyboardInputData(); 

	private:
		
		struct KeyboardInputData  _keyboardInputData; 
		
	};

}