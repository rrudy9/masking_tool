#pragma once

#include <graphical_lib.h>
#include <vector>

namespace data
{
	
	class MaskingTagData
	{
	
	public: 
		MaskingTagData(); 

		void addMaskingTag(int currentSelectedIndex, ImVec2 topLeft, ImVec2 bottomRight);

		void removeMaskingTag(int currentSelectionIndex, int currentIndex); 

		std::vector<std::vector<ImVec2>>& getMaskingTagData();
			
		void swapData(int newIndex, int oldIndex); 

	private:
		
		// we'll use double vector 
		// this way we can draw each mask of each picture (index) everytime 
		// while changing the color of every tag

		std::vector<std::vector<ImVec2>> _maskingData;
	};
}