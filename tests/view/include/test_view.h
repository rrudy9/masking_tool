#ifndef TEST_VIEW_H
#define TEST_VIEW_H

#include <base_content.h>
#include <data_manager.h>


namespace ImGuiBaseContent
{
	
	class TestView : public BaseContentItem
	{

	public:

		TestView(struct data::DataManager& dataManager);
		//~TestView(); 

		void show(bool& open) override;

	private:
		void createItem(); 

		void consoleView(); 

		void runAll(); 

		void runTestsForMasking(); 

		void eventMaskingTest(); 

		void inputMaskingTest(); 

		struct data::DataManager& _dataManager;

		std::vector<std::string> _testLog; 

	};

}

#endif