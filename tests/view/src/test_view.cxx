#include <test_view.h>

namespace ImGuiBaseContent
{

	TestView::TestView(struct data::DataManager& dataManager)
		:BaseContentItem()
		,_dataManager(dataManager)
	{
		_testLog = std::vector<std::string>(); 
	}


	void TestView::show(bool& open)
	{
		createImGuiWindow<TestView>(&TestView::createItem,
			this, open, ICON_FA_TV   " Test Panel");
	}

	void TestView::createItem()
	{
	
		createImGuiTreeNode(
			&TestView::runAll, this, "Run Application"); 

		createImGuiTreeNode(
			&TestView::runTestsForMasking, this, "Masking");

		ImGui::NewLine(); 

		createChildBarWindow(
			&TestView::consoleView, this, "console_test", "Log Test", true,
			ImGuiWindowFlags_HorizontalScrollbar |ImGuiWindowFlags_MenuBar,
			ImVec2(0, 0), IM_COL32(46, 46, 49, 1000), ImVec4(1.0f, 1.0f, 1.0f, 1.0f)
		
		);
		

	}

	void TestView::runAll()
	{
		createHelpMarker(
			"This will load a project, do some stuff and then save the project\n");

		ImGui::SameLine(); 

		ImGui::Button("Run All");

	}
	
	void TestView::runTestsForMasking()
	{
		createImGuiTreeNode(
			&TestView::eventMaskingTest, this, "Events");

		createImGuiTreeNode(
			&TestView::inputMaskingTest, this, "Inputs");

	}

	void TestView::eventMaskingTest()
	{
		
		if (ImGui::Button("Run Event Test"))
		{ 


			// will see how to do it 
			std::map<int, std::vector<int>> eventsValueTest = std::map<int, std::vector<int>>(); 

			_testLog.push_back("Running Event Tests.. ");
	/*		for (uint8_t eventCount = 0; eventCount < _dataManager->_maskingEventContent->_sizeOfEvenItemsArray; ++eventCount)
			{
				std::vector<int> vecCount = std::vector<int>(); 

				for (int i = 0; i < 100; ++i)
				{

					int testValues = rand() % 10011 + 1;

					std::string s = std::to_string(testValues);
					const char* intChasr = s.c_str();

					vecCount.push_back(testValues); 

					data::fillEventParameter(
						&_dataManager->_dataContent[0]->_maskingEventHandle
						, eventCount, intChasr);
				
				
				
				}

				eventsValueTest[eventCount] = vecCount; 
			}*/




		}
	
	}

	void TestView::consoleView()
	{
		for (int i = 0; i < _testLog.size(); ++i)
		{
			ImGui::Text(_testLog.at(i).c_str());
			//	ImGui::Spacing(); 
		}

	}

	void TestView::inputMaskingTest()
	{
		
		
	

	}



}
