
#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include <main_window.h>

int main()
{
    HWND hWnd = GetConsoleWindow();
    ShowWindow(hWnd, SW_HIDE); 

    DWORD last_error;
    DWORD result;
    DWORD path_size = 1024;
    char* path = (char*)malloc(1024);

    for (;;)
    {
        memset(path, 0, path_size);
        result = GetModuleFileName(0, path, path_size - 1);
        last_error = GetLastError();

        if (0 == result)
        {
            free(path);
            path = 0;
            break;
        }
        else if (result == path_size - 1)
        {
            free(path);
            /* May need to also check for ERROR_SUCCESS here if XP/2K */
            if (ERROR_INSUFFICIENT_BUFFER != last_error)
            {
                path = 0;
                break;
            }
            path_size = path_size * 2;
            path = (char*)malloc(path_size);
        }
        else
        {
            break;
        }
    }

    if (!path)
    {
        fprintf(stderr, "Failure: %d\n", last_error);
    }
    else
    {
        printf("path=%s\n", path);
    }

    
	MainWindow *mainWindow = MainWindow::getInstance();

	mainWindow->init(1200, 960, "Pic Tools"); 

	mainWindow->render(); 

    mainWindow->resetInstance(); 
	
    return 0;
}

