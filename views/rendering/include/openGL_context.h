
#pragma once

#include <base_context.h>
#include <Windows.h>

#include <image_loader.h>

#include <windows_manager.h>

class OpenGLContext : public BaseContext
{

public:
	OpenGLContext();

	void init(VirtualWindow* window) override;
	void preRendering() override;
	void postRendering() override;
	void endRendering() override;


};


