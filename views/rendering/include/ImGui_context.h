#pragma once

#include <base_context.h>
#include <IconsFontAwesome5.h>
#include <windows_manager.h>

class ImGUIContext : public BaseContext
{

public:
	ImGUIContext();

	void init(VirtualWindow* window) override;
	void preRendering() override;
	void postRendering() override;
	void endRendering() override;

};

