#pragma once

#include <base_context.h>
#include <virtual_window.h>
#include <Windows.h>
#include <tchar.h>

#include <windows_manager.h>

class DirectXContext : public BaseContext
{
public:
	DirectXContext();

	void init(VirtualWindow* window) override;
	void preRendering() override;
	void postRendering() override;
	void endRendering() override;

	bool CreateDeviceD3D(HWND hWnd);
	void CleanupDeviceD3D();
	void CreateRenderTarget();
	void CleanupRenderTarget();

};

