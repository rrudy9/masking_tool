#pragma once


#include <virtual_window.h> 
//#include <vertex_holder.h>
#include <vector>

class BaseContext
{
public:

	BaseContext()
		:_virtualWindow(nullptr)
	{};

	virtual void init(VirtualWindow* window) = 0; 
	virtual void preRendering() = 0;
	virtual void postRendering() = 0;
	virtual void endRendering() = 0;

protected:
	VirtualWindow* _virtualWindow = nullptr;
};


class OpenGLVertexBuffersManager
{

public:
	OpenGLVertexBuffersManager()
	{};

	//virtual void createBuffers(std::vector<Vertex>& vertices, std::vector<unsigned int>& indices, std::vector<Texture>& textures) = 0;
	virtual void bind() = 0;
	virtual void unbind() = 0;
	virtual void deleteBuffers() = 0;

	virtual void draw(int numberOfVertices) = 0;


protected:
	GLuint _VAO = 0;
	GLuint _VBO = 0;
	GLuint _EBO = 0;
};


class OpenGLFrameBuffersManager
{

public:
	OpenGLFrameBuffersManager()
	{};

	virtual void createBuffers(int width, int height) = 0;

	virtual void bind() = 0;
	virtual void unbind() = 0;
	virtual void deleteBuffers() = 0;
	virtual int getTexture() = 0;


protected:

	unsigned int _FBO = 0;
	unsigned _texId = 0;
	unsigned _depthMap = 0;//_rbo = 0; // depth and stencil buffer
	int _width = 0;
	int _height = 0;
};
