#include <DirectX_context.h>


DirectXContext::DirectXContext()
    :BaseContext()
{};


void DirectXContext::init(VirtualWindow* window)
{
    _virtualWindow = window;

    try
    {
        if (!CreateDeviceD3D(window->getHWND()))
        {
            CleanupDeviceD3D();
            ::UnregisterClass(window->getWNDClass().lpszClassName, window->getWNDClass().hInstance);
            throw std::exception("Can't create your directx window \n");
        }
    }
    catch (std::exception& exception)
    {
        std::cerr << exception.what() << std::endl;
    }

    // Show the window
    ::ShowWindow(window->getHWND(), SW_SHOWDEFAULT);
    ::UpdateWindow(window->getHWND());

}

void DirectXContext::preRendering()
{

}


void DirectXContext::postRendering()
{


    WindowsManager::getInstance()->g_pSwapChain->Present(1, 0); // Present with vsync
}

void DirectXContext::endRendering()
{
    CleanupDeviceD3D();
    ::DestroyWindow(_virtualWindow->getHWND());
    ::UnregisterClass(_virtualWindow->getWNDClass().lpszClassName, _virtualWindow->getWNDClass().hInstance);
}


bool DirectXContext::CreateDeviceD3D(HWND hWnd)
{
    // Setup swap chain
    DXGI_SWAP_CHAIN_DESC sd;
    ZeroMemory(&sd, sizeof(sd));
    sd.BufferCount = 2;
    sd.BufferDesc.Width = 0;
    sd.BufferDesc.Height = 0;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow = hWnd;
    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;
    
    sd.Windowed = TRUE;
    sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

    UINT createDeviceFlags = 0;
    //createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
    D3D_FEATURE_LEVEL featureLevel;
    const D3D_FEATURE_LEVEL featureLevelArray[2] = { D3D_FEATURE_LEVEL_11_0, D3D_FEATURE_LEVEL_10_0, };
    if (D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, createDeviceFlags, featureLevelArray, 2, D3D11_SDK_VERSION, &sd, &WindowsManager::getInstance()->g_pSwapChain, &WindowsManager::getInstance()->g_pd3dDevice, &featureLevel, &WindowsManager::getInstance()->g_pd3dDeviceContext) != S_OK)
        return false;

    CreateRenderTarget();

    return true;
}

void DirectXContext::CleanupDeviceD3D()
{
    CleanupRenderTarget();
    if (WindowsManager::getInstance()->g_pSwapChain) { WindowsManager::getInstance()->g_pSwapChain->Release();  WindowsManager::getInstance()->g_pSwapChain = NULL; }
    if (WindowsManager::getInstance()->g_pd3dDeviceContext) { WindowsManager::getInstance()->g_pd3dDeviceContext->Release();  WindowsManager::getInstance()->g_pd3dDeviceContext = NULL; }
    if (WindowsManager::getInstance()->g_pd3dDevice) { WindowsManager::getInstance()->g_pd3dDevice->Release();  WindowsManager::getInstance()->g_pd3dDevice = NULL; }
}

void DirectXContext::CreateRenderTarget()
{
    ID3D11Texture2D* pBackBuffer;
    WindowsManager::getInstance()->g_pSwapChain->GetBuffer(0, IID_PPV_ARGS(&pBackBuffer));
    WindowsManager::getInstance()->g_pd3dDevice->CreateRenderTargetView(pBackBuffer, NULL, &WindowsManager::getInstance()->g_mainRenderTargetView);
    pBackBuffer->Release();
}

void DirectXContext::CleanupRenderTarget()
{
    if (WindowsManager::getInstance()->g_mainRenderTargetView)
    { 
        WindowsManager::getInstance()->g_mainRenderTargetView->Release(); 
        WindowsManager::getInstance()->g_mainRenderTargetView = NULL;
    }
}
