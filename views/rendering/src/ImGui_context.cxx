
#include <ImGui_context.h>

// GL 3.0 + GLSL 130

const char* glsl_version = "#version 130";

ImGUIContext::ImGUIContext()
	:BaseContext()
{};


void ImGUIContext::init(VirtualWindow* window)
{
	_virtualWindow = window;

	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();

	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;       // Enable Keyboard Controls
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking

#ifdef DIRECTX_FOUND

	ImGui_ImplWin32_Init(window->getHWND());
	ImGui_ImplDX11_Init(WindowsManager::getInstance()->g_pd3dDevice, WindowsManager::getInstance()->g_pd3dDeviceContext);

#else
	ImGui_ImplGlfw_InitForOpenGL(_virtualWindow->getWindow(), true);
	ImGui_ImplOpenGL3_Init(glsl_version);
#endif
	
	
	ImGui::StyleColorsDark();


	io.Fonts->AddFontDefault();

	io.Fonts->AddFontDefault();

	// merge in icons from Font Awesome
	ImFontConfig icons_config;
	icons_config.MergeMode = true;
	icons_config.GlyphMinAdvanceX = 14.0f;
	icons_config.GlyphOffset = { 0.f, 2.f };

	icons_config.PixelSnapH = true;
	static const ImWchar icons_ranges[] = { ICON_MIN_FA, ICON_MAX_FA, 0 };

	io.Fonts->AddFontFromFileTTF("Ressources/fonts/fa-solid-900.ttf", 14.0f, &icons_config, icons_ranges);

	
	ImFont* font = io.Fonts->Fonts[1];
	io.FontDefault = font;


}

void ImGUIContext::preRendering()
{
	// Start the Dear ImGui framex

#ifdef DIRECTX_FOUND
	ImGui_ImplDX11_NewFrame();
	ImGui_ImplWin32_NewFrame();
#else
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
#endif

	ImGui::NewFrame();
}

void ImGUIContext::postRendering()
{
	// Render dear imgui into screen
	ImGui::Render();
#ifdef DIRECTX_FOUND

	WindowsManager::getInstance()->g_pd3dDeviceContext->OMSetRenderTargets(1, &WindowsManager::getInstance()->g_mainRenderTargetView, NULL);

	const float clear_color_with_alpha[4] = { 0.0f, 0.0f, 0.0f, 1.0f };

	WindowsManager::getInstance()->g_pd3dDeviceContext->ClearRenderTargetView(WindowsManager::getInstance()->g_mainRenderTargetView, clear_color_with_alpha);
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());

#else
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
#endif

}

void ImGUIContext::endRendering()
{

#ifdef DIRECTX_FOUND
	ImGui_ImplDX11_Shutdown();
	ImGui_ImplWin32_Shutdown();
#else
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
#endif

	ImGui::DestroyContext();

}
