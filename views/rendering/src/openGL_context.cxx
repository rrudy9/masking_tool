
#include <openGL_context.h>


static void onResizeWindow(GLFWwindow* window, int width, int height)
{
	VirtualWindow* mainWindow = (VirtualWindow*)glfwGetWindowUserPointer(window);
	mainWindow->resizeWindow(width, height);
}

static void onWindowClose(GLFWwindow* window)
{
	VirtualWindow* mainWindow = (VirtualWindow*)glfwGetWindowUserPointer(window);
	mainWindow->onClose();

}

static void onKeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	VirtualWindow* mainWindow = (VirtualWindow*)glfwGetWindowUserPointer(window);
	mainWindow->onKey(key, scancode, action, mods);
}

static void cursor_position_callback(GLFWwindow* window, double x, double y) 
{
	VirtualWindow* mainWindow = (VirtualWindow*)glfwGetWindowUserPointer(window);

	if (mainWindow->_isMainWindowMoved == 1) 
	{
		mainWindow->offsetCursorX = x - mainWindow->originCursorPosX;
		mainWindow->offsetCursorY = y - mainWindow->originCursorPosY;
	}

}

static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	VirtualWindow* mainWindow = (VirtualWindow*)glfwGetWindowUserPointer(window);

	if (WindowsManager::getInstance()->getWindowMovingValue())
	{

		if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
		{
		
			SetCursor(LoadCursor(NULL, IDC_SIZEALL));

			mainWindow->_isMainWindowMoved = 1;
			double x, y;
			glfwGetCursorPos(window, &x, &y);
			mainWindow->originCursorPosX = floor(x);
			mainWindow->originCursorPosY = floor(y);

		}

		if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE)
		{

			mainWindow->_isMainWindowMoved = 0;
			mainWindow->originCursorPosX = 0;
			mainWindow->originCursorPosY = 0;
			mainWindow->offsetCursorX = 0;
			mainWindow->offsetCursorY = 0;

		}

	}

}



OpenGLContext::OpenGLContext()
	:BaseContext()
{};

void OpenGLContext::init(VirtualWindow* window)
{
	_virtualWindow = window;


	if (!glfwInit()) {
		fprintf(stderr, "failed to initialize GLFW.\n");
		exit(0);
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	

//	glfwWindowHint(GLFW_DECORATED, false);
	
	GLFWwindow* oGLwindow = glfwCreateWindow(_virtualWindow->_widthScreen, _virtualWindow->_heightScreen, _virtualWindow->_windowTitle, nullptr, nullptr);


	_virtualWindow->setWindow(&oGLwindow);

	if (!oGLwindow) {
		fprintf(stderr, "Failed to create GLFW Window");
		glfwTerminate();
		exit(0);
	}

	glfwSetWindowUserPointer(oGLwindow, window);
	glfwSetWindowSizeCallback(oGLwindow, onResizeWindow);
	glfwSetWindowCloseCallback(oGLwindow, onWindowClose);
	glfwSetKeyCallback(oGLwindow, onKeyCallback);

	/// to move frameless window
	glfwSetCursorPosCallback(oGLwindow, cursor_position_callback);
	glfwSetMouseButtonCallback(oGLwindow, mouse_button_callback);

	GLFWimage images[1];
	loadImage("Ressources/assets/icon.png", images[0].width, images[0].height, &images[0].pixels);
	glfwSetWindowIcon(oGLwindow, 1, images);
	freeImage(&images[0].pixels);

	

	glfwMakeContextCurrent(_virtualWindow->getWindow());

	glfwSwapInterval(1); // Enable vsync

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		fprintf(stderr, "failed to initialize GLAD");
		exit(0);
	}

	glEnable(GL_DEPTH_TEST);


}

void OpenGLContext::preRendering()
{
	glViewport(0, 0, _virtualWindow->_widthScreen, _virtualWindow->_heightScreen);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);
	

	glfwGetWindowSize(_virtualWindow->getWindow(), &(_virtualWindow->_widthScreen), &(_virtualWindow->_heightScreen));

	
}

void OpenGLContext::postRendering()
{

	if (_virtualWindow->_isMainWindowMoved && !_virtualWindow->_fullScreen) 
	{

		glfwGetWindowPos(_virtualWindow->getWindow(), &_virtualWindow->currentCursorPosX, &_virtualWindow->currentCursorPosY);
		glfwSetWindowPos(_virtualWindow->getWindow(), _virtualWindow->currentCursorPosX + _virtualWindow->offsetCursorX, _virtualWindow->currentCursorPosY + _virtualWindow->offsetCursorY);
		_virtualWindow->offsetCursorX = 0;
		_virtualWindow->offsetCursorY = 0;
		_virtualWindow->originCursorPosX += _virtualWindow->offsetCursorX;
		_virtualWindow->originCursorPosY += _virtualWindow->offsetCursorY;

	}


	glfwGetWindowPos(_virtualWindow->getWindow(), &_virtualWindow->topLeftX, &_virtualWindow->topLeftY);

	glfwPollEvents();
	glfwSwapBuffers(_virtualWindow->getWindow());

}

void OpenGLContext::endRendering()
{
	glfwDestroyWindow(_virtualWindow->getWindow());
	glfwTerminate();
}

