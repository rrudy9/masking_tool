#include <file_manager.h>


namespace ImGuiBaseContent
{
    FileManager::FileManager(
        const std::string& title
       ,data::DataManager& dataManager
        ,bool& getQuitReponse
    )
        :BaseContent(title)
        ,_fileBrowser(FileBrowser(_filePathName, _filePath))
        ,_dataManager(dataManager)
        , _dialog(Dialog(getQuitReponse))
        , _projectSaver(dataManager)
        , _projectLoader(dataManager)
        , _xmlSaver(dataManager)

    {

    }


    void FileManager::rendering()
    {
        createMenuBarItem<FileManager>(&FileManager::createItem, this);

        checkState();
        getBrowserResponse();

    }


    void FileManager::createItem()
    {

        if (ImGui::BeginMenu("New"))
        {
            if (ImGui::MenuItem(ICON_FA_FILE " New Project"))
            {
                _dataManager.resetData(); 
            }

            if (ImGui::MenuItem(ICON_FA_FILE_IMAGE " New Image"))
            {
                _fileBrowser.setComponentState(FileBrowser::load_pictures);
                _showBrowser = true;
            }
           
            ImGui::EndMenu();
        }


        if (ImGui::MenuItem(ICON_FA_FOLDER_OPEN " Open Project", NULL))
        {

            _fileBrowser.setComponentState(FileBrowser::load_project);
            _showBrowser = true;

        }

        if (ImGui::MenuItem(ICON_FA_SAVE " Save Project", NULL))
        {
            
            if (_dataManager.getImageData().getImageContainer().size() == 0)
            {
                _dialog.setComponentSetting(Dialog::dialogState::warning, "Warning", "Please load a picture first", "Ok", "Cancel");
                _showDialog = true;
            }
            else
            {
                _fileBrowser.setComponentState(FileBrowser::save_project);
                _showBrowser = true;
            }

        }

        if (ImGui::MenuItem(ICON_FA_FILE_EXPORT " Export As XML", NULL))
        {

            if (_dataManager.getImageData().getImageContainer().size() == 0)
            {
                _dialog.setComponentSetting(Dialog::dialogState::warning, "Warning", "Please load a picture first", "Ok", "Cancel");
                _showDialog = true;
            }
            else
            {
                _fileBrowser.setComponentState(FileBrowser::export_as_xml);
                _showBrowser = true;
            }

        }

        if (ImGui::MenuItem("Quit", "Alt+F4")) {
            _dialog.setComponentSetting(Dialog::quit_state, "Quit", "Close? ", "Yes", "No");
            _showDialog = true;
        }


    }

    void FileManager::checkState()
    {

        _fileBrowser.show(_showBrowser);
        _dialog.show(_showDialog);

    }



    void FileManager::getBrowserResponse()
    {
        
        if (_filePathName.length() != 0)
        {
            if (_fileBrowser.getComponentState() == FileBrowser::browserState::load_pictures)
            {

                // we load the image first 
                if (!_dataManager.getImageData().loadImageFromFile(_filePathName))
                {
                    // launch dialog exception
                    _dialog.setComponentSetting(Dialog::dialogState::warning, "Warning",
                        "Can't add the following picture:" +
                        _filePathName + ". The size must be " +
                        std::to_string(_dataManager.getImageData().getImageContainer().at(0)._width) + "x" +
                        std::to_string(_dataManager.getImageData().getImageContainer().at(0)._height),
                        "Ok", "Cancel");

                    _showDialog = true;
                }

                // and then we initialize the data for each loaded picture
                _dataManager.setDataForEachPicture();
            }
            else if (_fileBrowser.getComponentState() == FileBrowser::browserState::export_as_xml)
            {
                _dataManager.setLastDataBeforeSave();
                _xmlSaver.exportXmlDoc(_filePathName);
            }
            else if (_fileBrowser.getComponentState() == FileBrowser::browserState::save_project)
            {
                _dataManager.setLastDataBeforeSave();
                _projectSaver.saveProject(_filePath, _filePathName);
            }
            else if (_fileBrowser.getComponentState() == FileBrowser::browserState::load_project)
            {
                _projectLoader.loadProject(_filePath, _filePathName);
            }
            _filePathName = "";
            _filePath = "";
        }

    }

    FileManager::~FileManager()
    {
    }

}
