#include <views.h>

namespace ImGuiBaseContent
{

	Views::Views(
		const std::string& title,
		data::DataManager& dataManager
		)
		:BaseContent(title)
		,_dataManager(dataManager)
		,_pictureJunker(PictureJunker(dataManager))
		,_pictureView(PictureView(dataManager))
		,_projectPanel(ProjectPanel(dataManager))
	{
	}
	
	void Views::rendering() 
	{
		createMenuBarItem<Views>(&Views::createItem, this);

		checkState();
	}

	void Views::createItem()
	{

		ImGui::MenuItem(ICON_FA_STREET_VIEW " Scene View", NULL, &_openPictureView);
		ImGui::MenuItem(ICON_FA_IMAGES " Picture Junker", NULL, &_openPictureJunker);
		ImGui::MenuItem(ICON_FA_SOLAR_PANEL " Project Panel", NULL, &_openProjectPanel);

	} 


	void Views::checkState()
	{
	
		_pictureView.show(_openPictureView);
		_pictureJunker.show(_openPictureJunker);
		_projectPanel.show(_openProjectPanel); 
	
	}

	Views::~Views()
	{
	}

}