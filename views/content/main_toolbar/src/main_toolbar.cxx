#include <main_toolbar.h>


namespace ImGuiBaseContent
{

    MainToolbar::MainToolbar() 
        :_dataManager(data::DataManager())
        , _fileManager(ICON_FA_FILE_ALT " File", _dataManager, _getQuitDialogReponse)
        ,_tools((ICON_FA_TOOLS " Tools"), _dataManager)
        ,_views(ICON_FA_MAP " Views", _dataManager)
        ,_quitDiaog(Dialog(_getQuitDialogReponse))
    {

    }

    void MainToolbar::init(VirtualWindow* window)
    {
        _virtualWindow = window;
        _quitDiaog.setComponentSetting(Dialog::quit_state, "Quit", "Close? ", "Yes", "No");

    }

    void MainToolbar::rendering()
    {
        
        if (ImGui::BeginMainMenuBar())
        {
           
            ImGui::DockSpaceOverViewport(ImGui::GetMainViewport());
            
            //file import section
            _fileManager.rendering();

            //views section
            _views.rendering();
            //tools section 
            _tools.rendering();

            //add others section
            ImGui::PushStyleColor(ImGuiCol_Header, IM_COL32(255,140,0,1000));
            ImGui::MenuItem(ICON_FA_BORDER_NONE  " Go Frameless", NULL, &_virtualWindow->_goFrameless);
            ImGui::PopStyleColor(); 

            createHelpMarker<MainToolbar>(
                "- Use your mouse Wheel for image zooming\n\n"
                "- You can change the order of your images in the list by dragging them with your mouse\n\n"
                "- Hold your 'Right click' button of your mouse over the image to drag it around\n\n"
                "- Hold your 'Right click' button of your mouse anywhere to move the window around\n\n"
                "- You can remove selected / added items by right clicking on them (event params, mouse rect..)\n\n"
                "- Click the 'Delete' Key button on your keyboard to remove a selected image  \n\n"
                "- Click on 'Go Frameless' to remove borders from your window (you can't resize it when it is frameless)\n\n"
                , IM_COL32(255, 255, 255, 1000));

            ImGui::NextColumn();
            std::string minimize = ICON_FA_WINDOW_MINIMIZE;
            std::string maximize = _virtualWindow->_fullScreen ? ICON_FA_WINDOW_RESTORE:  ICON_FA_WINDOW_MAXIMIZE;
            std::string close = ICON_FA_WINDOW_CLOSE;
            
            ImGui::SetCursorPosX(ImGui::GetCursorPosX() + ImGui::GetColumnWidth() -
                ImGui::CalcTextSize(minimize.c_str()).x - ImGui::CalcTextSize(maximize.c_str()).x - ImGui::CalcTextSize(close.c_str()).x - 17
                - ImGui::GetScrollX() - 2 * ImGui::GetStyle().ItemSpacing.x);
            
            if (_virtualWindow->_goFrameless)
            {
                ImGui::PushStyleColor(ImGuiCol_Button, IM_COL32(0,0,0,1));

                if (ImGui::Button(minimize.c_str()))
                    _virtualWindow->minimizeWindow(); 

                if (ImGui::Button(maximize.c_str()))
                    _virtualWindow->maximizeWindow(); 

                if (ImGui::Button(close.c_str()))
                    _showQuitDialog = true; 


                ImGui::PopStyleColor(); 
            }

            ImGui::EndMainMenuBar();
        
        }

        if (_getQuitDialogReponse)
            _virtualWindow->onClose();

        _quitDiaog.show(_showQuitDialog);
       
        _virtualWindow->goBorderless();

        if (!_virtualWindow->_goFrameless && _virtualWindow->_fullScreen )
            _virtualWindow->_fullScreen = false;

    }

    

    MainToolbar::~MainToolbar()
    {
    
    }

}