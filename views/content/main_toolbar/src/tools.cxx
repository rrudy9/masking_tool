
#include <tools.h>


namespace ImGuiBaseContent
{

    Tools::Tools(const std::string& title, 
        data::DataManager& dataManager)
        :BaseContent(title)
        , _dataManager(dataManager)
        , _styleEditor(StyleEditor())
        , _maskingPanel(MaskingPanel(dataManager))
        #ifdef TEST_FOUND
       // ,_testView (TestView(dataManager))
        #endif
    {

    }
    void Tools::rendering()
    {
        createMenuBarItem<Tools>(&Tools::createItem, this);

        checkState();
    }

    void Tools::createItem()
    {

        ImGui::MenuItem(ICON_FA_BORDER_STYLE " Style Editor", NULL, &_showStyleEditor);
        ImGui::MenuItem(ICON_FA_MARKER    " Masking Tool", NULL, &_showMaskingPanel);

        #ifdef TEST_FOUND
            ImGui::MenuItem(ICON_FA_TV  " Tests Panel ", NULL, &_showTestPanel);
        #endif

    }

    void Tools::checkState()
    {
        _styleEditor.show(_showStyleEditor);
        _maskingPanel.show(_showMaskingPanel);

        #ifdef TEST_FOUND
    //    _testView.show(_showTestPanel);
        #endif

    }

    Tools::~Tools()
    {

    }


}