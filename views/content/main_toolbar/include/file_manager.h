#pragma once

#include <base_content.h>
#include <file_browser.h>
#include <data_manager.h>
#include <dialog.h>

#include <project_panel.h>

#include <project_saver.h>
#include <project_loader.h>
#include <xml_saver.h>


namespace ImGuiBaseContent
{

	class FileManager : public BaseContent
	{

	public:
	
		FileManager(const std::string& title
			,data::DataManager& dataManager
			,bool &getQuiResponse
		);

		~FileManager();

		void rendering() override;


	private:

		FileBrowser _fileBrowser;

		data::DataManager& _dataManager;
	
		data::ProjectLoader _projectLoader;
		data::ProjectSaver _projectSaver;
		data::XmlSaver _xmlSaver;

		Dialog _dialog;

		bool _showDialog = false;

		void checkState();

		bool _showBrowser = false;

		void createItem();

		std::string _filePathName = "";
		std::string _filePath = ""; 
	
		void getBrowserResponse();

	};

}
