#pragma once 

#include <base_content.h>
#include <graphical_lib.h>

#include <picture_view.h>
#include <picture_junker.h>
#include <project_panel.h>
#include <data_manager.h>

#include <virtual_window.h>

namespace ImGuiBaseContent
{

	class Views : public BaseContent
	{
	public:
	
		Views(
			const std::string& title
			,data::DataManager& dataManager
		);
		~Views();
	
		void rendering() override;

	private:

		bool _openPictureView = true;
		bool _openPictureJunker = true;
		bool _openProjectPanel = true;

		PictureView _pictureView;
		PictureJunker _pictureJunker;
		ProjectPanel _projectPanel;

		void createItem();
		void checkState();

		data::DataManager& _dataManager; 

	};
}