#pragma once

#include <graphical_lib.h>
//#include <component_creator.h>


#include "IconsFontAwesome5.h"

namespace ImGuiBaseContent
{

	class BaseContent
	{
	public:

		BaseContent(const std::string& title)
			: _title(title)
		{};

		virtual void rendering() = 0;

		std::string getTitle()
		{
			return _title;
		};
		
	protected:
		std::string _title;

	};


	class BaseContentItem
	{

	public:
		BaseContentItem()
		{};

		virtual void show(bool& open) = 0;

	//protected:
	//	void createChildBarWindow(void(*function)(),
	//		const char* id, const char* windowTitle,
	//		bool border, ImGuiWindowFlags childWindowFlags, ImVec2 size, ImU32 barColor, ImVec4 barTextColor)
	//		;


	};



	template<class C>
	void createMenuBarItem(void (C::* function)(), C* c)
	{
		if (ImGui::BeginMenu(c->getTitle().c_str()))
		{

			(c->*function)();

			ImGui::EndMenu();

		}

	}



	template<class C>
	void createImGuiWindow(void (C::* function)(), C* c, bool& open, const char* title, ImGuiWindowFlags windowFlags)
	{
		if (open)
		{

			ImGui::Begin(title, &open
				, windowFlags
				);


			(c->*function)();


			ImGui::End();

		}

	}

	template<class C>
	void createCollapsingHeader(void (C::* function)(), C* c, const char* collapsingHeaderTitle)
	{
		if (!ImGui::CollapsingHeader(collapsingHeaderTitle, ImGuiTreeNodeFlags_Leaf|  ImGuiTreeNodeFlags_DefaultOpen))
			return;

		(c->*function)();

	}


	// tooo much parameter 
	template<class C>
	void createChildBarWindow(void (C::* function)(), C* c,
		const char* id, const char* windowTitle,
		bool border, ImGuiWindowFlags childWindowFlags, ImVec2 size, ImU32 barColor, ImVec4 barTextColor)
	{

		ImGui::PushStyleColor(ImGuiCol_MenuBarBg, barColor);

		ImGui::BeginChild(id, size, border, childWindowFlags);
		if (ImGui::BeginMenuBar())
		{
			ImGui::TextColored(barTextColor, windowTitle);
			ImGui::EndMenuBar();
		}

		(c->*function)();

		ImGui::EndChild();
		ImGui::PopStyleColor();

	}

	// tooo much parameter 
	template<class C>
	void createChildBarWindow(void (C::* function)(int row), int row, C* c,
		const char* id, const char* windowTitle,
		bool border, ImGuiWindowFlags childWindowFlags, ImVec2 size, ImU32 barColor, ImVec4 barTextColor)
	{

		ImGui::PushStyleColor(ImGuiCol_MenuBarBg, barColor);

		ImGui::BeginChild(id, size, border, childWindowFlags);
		if (ImGui::BeginMenuBar())
		{
			ImGui::TextColored(barTextColor, windowTitle);
			ImGui::EndMenuBar();
		}

		(c->*function)(row);

		ImGui::EndChild();
		ImGui::PopStyleColor();

	}

	template<class C>
	void createImGuiTreeNode(void (C::* function)(), C* c, const char* title)
	{
		if (ImGui::TreeNode(title))
		{
			(c->*function)();
			
			ImGui::TreePop(); 
		}

	}

	template<class C>
	void createHelpMarker(const char* description, ImU32 helpMarkerColor)
	{
		ImGui::PushStyleColor(ImGuiCol_TextDisabled, helpMarkerColor);
		ImGui::TextDisabled("(?)");
		if (ImGui::IsItemHovered())
		{
			ImGui::BeginTooltip();
			ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
			ImGui::TextUnformatted(description);
			ImGui::PopTextWrapPos();
			ImGui::EndTooltip();
		}
		ImGui::PopStyleColor();
	}

	template<class C>
	void createCheckLists(bool& blackListValue, bool& whiteListValue)
	{


		ImGui::PushStyleColor(ImGuiCol_CheckMark, IM_COL32(255, 60, 60, 2000));
		ImGui::Checkbox("Black List", &blackListValue);
		if (blackListValue)
			whiteListValue = false;

		if (!blackListValue)
			blackListValue = true;


		ImGui::SameLine();

		ImGui::PushStyleColor(ImGuiCol_CheckMark, IM_COL32(60, 255, 60, 200));

		ImGui::Checkbox("White List", &whiteListValue);

		if (whiteListValue)
			blackListValue = false;

		if (!whiteListValue)
			whiteListValue = true;

		ImGui::PopStyleColor(2);

	}

}