#pragma once 

#include <base_content.h>
#include <style_editor.h>
#include <masking_panel.h>
#include <data_manager.h>

#ifdef TEST_FOUND
#include <test_view.h>
#endif

namespace ImGuiBaseContent
{

    class Tools : public BaseContent
    {
    public:
        
        Tools(
            const std::string& title, 
            data::DataManager& dataManager);
        ~Tools();

        void rendering() override;

    private:

        void createItem();
        void checkState();



        bool _showStyleEditor = false;
        bool _showMaskingPanel = true;

        bool _showTestPanel = true; 

        StyleEditor _styleEditor;
        MaskingPanel _maskingPanel;

        #ifdef TEST_FOUND
     //   TestView _testView;
        #endif


        data::DataManager& _dataManager;

    };
}