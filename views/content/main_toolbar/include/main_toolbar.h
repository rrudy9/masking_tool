
#pragma once

#include <file_manager.h>

#include <tools.h>
#include <views.h>

#include <virtual_window.h>
#include <data_manager.h>

#include <dialog.h>


namespace ImGuiBaseContent
{
	class MainToolbar
	{
	public:

		MainToolbar();
		
		~MainToolbar();

		void rendering();
		void init(VirtualWindow* window);

	private:

		FileManager _fileManager;
		Tools _tools;
		Views _views;


		data::DataManager _dataManager;
		VirtualWindow* _virtualWindow = nullptr;

		bool _getQuitDialogReponse = false; 
		bool _showQuitDialog = false; 

		Dialog _quitDiaog; 

	
	};
}