#include <masking_properties.h>


namespace ImGuiBaseContent
{


	MaskingProperties::MaskingProperties(
		data::MaskingPropertiesData& maskingPropertiesData
	)
		:BaseContentItem()
		, _maskingPropertiesData(maskingPropertiesData)
		,_grayFilterProperties(GrayFilterProperties(maskingPropertiesData.getGrayFilterData()))
		,_tagSettingProperties(TagSettingProperties(maskingPropertiesData.getTagSetting()))
		,_overlayFilterProperties(OverlayFilterProperties(maskingPropertiesData.getOverlayFilterData()))
		,_exitFilterProperties(ExitFilterProperties(maskingPropertiesData.getExitFilterData()))
	{
	}


	void MaskingProperties::show(bool& open)
	{
		//ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(ImGui::GetWindowWidth() * 3 / 100, ImGui::GetWindowHeight() * 60 / 100));

		createImGuiWindow<MaskingProperties>(&MaskingProperties::createItem,
			this, open, "Masking Properties", ImGuiWindowFlags_HorizontalScrollbar);
		//	ImGui::PopStyleVar();

	}

	void MaskingProperties::createItem()
	{

		_tagSettingProperties.show(_showPropertiesContent);

		ImGui::Spacing();

		ImGui::Separator();
		ImGui::Spacing();
		ImGui::Spacing();

		_grayFilterProperties.show(_showPropertiesContent);

		ImGui::Spacing();
		ImGui::Spacing();


		_exitFilterProperties.show(_showPropertiesContent);

		ImGui::Spacing();
		ImGui::Spacing();

		_overlayFilterProperties.show(_showPropertiesContent);

	}

	MaskingProperties::~MaskingProperties()
	{}

}