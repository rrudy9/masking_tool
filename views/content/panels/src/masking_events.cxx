#include <masking_events.h>



namespace ImGuiBaseContent
{
;

MaskingEvents::MaskingEvents(
    data::MaskingEventsData& maskingEventData
    , struct data::ApplicationSetting& applicationSetting
)
    : BaseContentItem()
    , _maskingEventsData(maskingEventData)
    , _applicationSetting(applicationSetting)
    {

    }

    void MaskingEvents::show(bool& open)
    {
        // ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(ImGui::GetWindowWidth() * 3 / 100, ImGui::GetWindowHeight() * 60 / 100));

        createImGuiWindow<MaskingEvents>(&MaskingEvents::createItem,
            this, open, "Masking Events", ImGuiWindowFlags_HorizontalScrollbar);

        //ImGui::PopStyleVar();

    }


    int MaskingEvents::MyResizeCallback(ImGuiInputTextCallbackData* data)
    {

        if (data->EventFlag == ImGuiInputTextFlags_CallbackResize)
        {
            // Resize string callback
            std::string* str = (std::string*)data->UserData;
            IM_ASSERT(data->Buf == str->c_str());
            str->resize(data->BufTextLen);
            data->Buf = (char*)str->c_str();
        }

        return 0;
    }


    void MaskingEvents::createItem()
    {

       
        if (checkIfSelectionChanged != _applicationSetting._currentSelection)
            _eventValue = "";
        
        
        ImGui::PushItemWidth(ImGui::GetContentRegionAvail().x * 1 / 2);

        std::string firstValue = _maskingEventsData.getEventsItemData()._eventItemsLabel[_currentEventSelectedComboBox];

        if (ImGui::BeginCombo("Event", firstValue.c_str()))
        {

            for (int i = 0; i < _maskingEventsData.getEventsItemData()._eventItemsLabel.size(); ++i) {
                bool is_selected = (_currentEventSelectedComboBox == i);

                if (ImGui::Selectable(_maskingEventsData.getEventsItemData()._eventItemsLabel[i].c_str()))
                {
                    _currentEventSelectedComboBox = i;

                    if (is_selected)
                        ImGui::SetItemDefaultFocus(); 
                }
            }
            ImGui::EndCombo();
        }

        ImGui::Spacing();

        ImGui::InputTextWithHint("##params_event",
            "Event Param", 
            (char*)_eventValue.c_str(),
            _eventValue.capacity() + 1,
            ImGuiInputTextFlags_CallbackResize,
            MyResizeCallback, (void*)&_eventValue
        );

        ImGui::Spacing();
        if (ImGui::Button("Add"))
        {
            if(_applicationSetting._picToolsActivation)
                _maskingEventsData.fillEventParameter(
                    _currentEventSelectedComboBox, 
                    _eventValue);
        }

        ImGui::NewLine();

        createTableEventItem();
       
    }


    void MaskingEvents::createTableEventItem()
    {

        static ImGuiTableFlags flags =
            ImGuiTableFlags_ScrollX |
            ImGuiTableFlags_ScrollY |
            ImGuiTableFlags_RowBg |
            ImGuiTableFlags_BordersOuter |
            ImGuiTableFlags_BordersV |
            ImGuiTableFlags_Reorderable |
            ImGuiTableFlags_Hideable;

        // chueck this to make it global
        const float TEXT_BASE_HEIGHT = ImGui::GetTextLineHeightWithSpacing();


        // check to align it to begin


        ImVec2 outer_size = ImVec2(0.0f, TEXT_BASE_HEIGHT * 8);
        if (ImGui::BeginTable("table_scrollx", 2, flags, outer_size))
        {
            ImGui::TableSetupScrollFreeze(1, 1);
            ImGui::TableSetupColumn("Events #", ImGuiTableColumnFlags_NoHide);
            ImGui::TableSetupColumn("Params");
            ImGui::TableHeadersRow();

            for (int row = 0; row < _maskingEventsData.getNumberOfEventList(); ++row)
            {
                ImGui::TableNextRow();
                for (int column = 0; column < 2; ++column)
                {
                    if (!ImGui::TableSetColumnIndex(column) && column > 0)
                        continue;
                    if (column == 0)
                        ImGui::Text("%s", _maskingEventsData.getEventsItemData()._eventItemsLabel[row].c_str());
                    else
                    {
                        for (int eventParamsIndex = 0; eventParamsIndex < _maskingEventsData.getEventsItemData()._eventsMaskingData[row].size(); ++eventParamsIndex)
                        {
                            std::string closeButtonTitle = "";
                            ImGui::PushID(row + eventParamsIndex * 1000);

                            float hue = row * 0.08f;
                            ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(hue, 0.6f, 0.6f));
                            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(hue, 0.7f, 0.7f));
                            ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(hue, 0.8f, 0.8f));

                            ImGui::Button(_maskingEventsData.
                                getEventsItemData()._eventsMaskingData[row][eventParamsIndex]._value.c_str());


                            ImGui::PushStyleColor(ImGuiCol_FrameBgHovered, (ImVec4)ImColor::HSV(hue, 0.7f, 0.7f));

                            closeButtonTitle = "Remove '" + _maskingEventsData.
                                getEventsItemData()._eventsMaskingData[row][eventParamsIndex]._value
                                + "'";

                            if (ImGui::BeginPopupContextItem())
                            {
                                if (ImGui::Button(closeButtonTitle.c_str()))
                                {
                                    _maskingEventsData.removeEventParameter(row, eventParamsIndex);
                                    ImGui::CloseCurrentPopup();
                                }

                                ImGui::EndPopup();
                            }
                            ImGui::PopStyleColor();

                            ImGui::SameLine();
                            ImGui::PopStyleColor(3);
                            ImGui::PopID();

                        }
                    }
                }
            }

            ImGui::EndTable();
        }

          checkIfSelectionChanged = _applicationSetting._currentSelection;

    }
    


    MaskingEvents::~MaskingEvents()
    {
    }

}