#include <masking_panel.h>

namespace ImGuiBaseContent
{

    MaskingPanel::MaskingPanel(data::DataManager& dataManager)
        :BaseContentItem()
        
        , _dataManager(dataManager)
        
        , _properties(MaskingProperties(
            dataManager.getPropertiesData())
        )
        , _inputs(MaskingInputs(
            dataManager.getInputsData(), dataManager.getApplicationSetting()
        ))
        , _events(MaskingEvents(
             dataManager.getEventsData(), dataManager.getApplicationSetting())
        )
    {

    }


    void MaskingPanel::show(bool& open)
    {

        createImGuiWindow<MaskingPanel>(&MaskingPanel::createItem,
            this, open, ICON_FA_MARKER   " Masking Tool", ImGuiWindowFlags_HorizontalScrollbar);

        checkState();
    }
                             
    void MaskingPanel::createItem()
    {
        if( ! _dataManager.getApplicationSetting()._tagMaskingActivation)
            ImGui::PushStyleColor(ImGuiCol_Button, IM_COL32(175, 130, 186, 400));

        else
            ImGui::PushStyleColor(ImGuiCol_Button, IM_COL32(175, 130, 186, 1000));


        if (ImGui::Button("Activate Masking"))
            _dataManager.getApplicationSetting()._tagMaskingActivation = !_dataManager.getApplicationSetting()._tagMaskingActivation;
        
        if (_dataManager.getApplicationSetting()._tagMaskingActivation)
        {

            _dataManager.getApplicationSetting()._mouseTagActivation = false; // we set it directly to false
            ImGui::SameLine();
            ImGui::Text("Masking Tag Activated !");
        }

        ImGui::NewLine();

        ImGui::Checkbox("Properties", &_activePropertiesMasking);
        ImGui::SameLine();
        ImGui::Checkbox("Inputs", &_activeInputMasking);
        ImGui::SameLine();
        ImGui::Checkbox("Events", &_activeEventMasking);

        ImGui::PopStyleColor();

    }

    void MaskingPanel::checkState()
    {
        _properties.show(_activePropertiesMasking);
        _inputs.show(_activeInputMasking);
        _events.show(_activeEventMasking);

    }

    MaskingPanel::~MaskingPanel()
    {

    }

}