#include <mouse_tag_input.h>

namespace ImGuiBaseContent
{
	MouseTagInput::MouseTagInput(
		data::InputsMouseTagData& inputMouseTagData
	)
		:_inputMouseTagData(inputMouseTagData)
	{
	
	};



	void MouseTagInput::getOriginalPictureSize(int width, int height)
	{
		_imageOriginalSize = ImVec2((float)width, (float)height); 
	}

	void MouseTagInput::startDrawMouseTag(double imageWidth, double imageHeight, ImVec2 initialCursorPosition, ImVec2 transformCursorPosition)
	{

		ImGuiIO& io = ImGui::GetIO();

		float deltaCursorX = transformCursorPosition.x - initialCursorPosition.x;
		float deltaCursorY = transformCursorPosition.y - initialCursorPosition.y;

		_currentImageRectSize = ImVec2((float)imageWidth, (float)imageHeight);

		_mousePositionInWindow.x =   io.MousePos.x - ImGui::GetWindowPos().x - ImGui::GetWindowContentRegionMin().x - deltaCursorX;
		_mousePositionInWindow.y =  io.MousePos.y - ImGui::GetWindowPos().y - ImGui::GetWindowContentRegionMin().y - deltaCursorY;

		ImGui::BeginTooltip();
			
		ImGui::Text("(%.2f,%.2f) (x,y)", _mousePositionInWindow.x, _mousePositionInWindow.y);
		ImGui::Text("(%.2f,%.2f) (w,h)", _currentImageRectSize.x, _currentImageRectSize.y);

		ImGui::EndTooltip();


		if (_mousePositionInWindow.x < 0.0f) { _mousePositionInWindow.x = 0.0f; }
		if (_mousePositionInWindow.y < 0.0f) { _mousePositionInWindow.y = 0.0f; }

		_mousePositionInImage ={
			_mousePositionInWindow.x * _imageOriginalSize.x / _currentImageRectSize.x ,
			_mousePositionInWindow.y * _imageOriginalSize.y / _currentImageRectSize.y
		};

		// Add first and second point
		if (!_startDrawing && ImGui::IsMouseClicked(ImGuiMouseButton_Left))
		{
			_inputMouseTagData.addMousePoints(_mousePositionInImage, _mousePositionInImage);
			_startDrawing = true;
		}
		if (_startDrawing)
		{
			_inputMouseTagData.getMouseTagData()._mousePoints.back() = _mousePositionInImage;
			if (!ImGui::IsMouseDown(ImGuiMouseButton_Left))
			{
				if (_inputMouseTagData.getMouseTagData()._mousePoints.at(_inputMouseTagData.getMouseTagData()._mousePoints.size() - 2).x >
					_inputMouseTagData.getMouseTagData()._mousePoints.back().x
					&&
					_inputMouseTagData.getMouseTagData()._mousePoints.at(_inputMouseTagData.getMouseTagData()._mousePoints.size() - 2).y >
					_inputMouseTagData.getMouseTagData()._mousePoints.back().y
					)
				{
					float swapX = _inputMouseTagData.getMouseTagData()._mousePoints.at(_inputMouseTagData.getMouseTagData()._mousePoints.size() - 2).x;
					float swapY = _inputMouseTagData.getMouseTagData()._mousePoints.at(_inputMouseTagData.getMouseTagData()._mousePoints.size() - 2).y;

					_inputMouseTagData.getMouseTagData()._mousePoints.at(_inputMouseTagData.getMouseTagData()._mousePoints.size() - 2).x = _inputMouseTagData.getMouseTagData()._mousePoints.back().x;
					_inputMouseTagData.getMouseTagData()._mousePoints.at(_inputMouseTagData.getMouseTagData()._mousePoints.size() - 2).y = _inputMouseTagData.getMouseTagData()._mousePoints.back().y;

					_inputMouseTagData.getMouseTagData()._mousePoints.back().x = swapX;
					_inputMouseTagData.getMouseTagData()._mousePoints.back().y = swapY;
										
				}

				else if (_inputMouseTagData.getMouseTagData()._mousePoints.at(_inputMouseTagData.getMouseTagData()._mousePoints.size() - 2).y >
					_inputMouseTagData.getMouseTagData()._mousePoints.back().y)
				{
					float swapY = _inputMouseTagData.getMouseTagData()._mousePoints.at(_inputMouseTagData.getMouseTagData()._mousePoints.size() - 2).y;
					_inputMouseTagData.getMouseTagData()._mousePoints.at(_inputMouseTagData.getMouseTagData()._mousePoints.size() - 2).y = _inputMouseTagData.getMouseTagData()._mousePoints.back().y;
					_inputMouseTagData.getMouseTagData()._mousePoints.back().y = swapY;

				}
				
				else if (_inputMouseTagData.getMouseTagData()._mousePoints.at(_inputMouseTagData.getMouseTagData()._mousePoints.size() - 2).x >
					_inputMouseTagData.getMouseTagData()._mousePoints.back().x)
				{
					float swapX = _inputMouseTagData.getMouseTagData()._mousePoints.at(_inputMouseTagData.getMouseTagData()._mousePoints.size() - 2).x;
					
					_inputMouseTagData.getMouseTagData()._mousePoints.at(_inputMouseTagData.getMouseTagData()._mousePoints.size() - 2).x = _inputMouseTagData.getMouseTagData()._mousePoints.back().x;
					_inputMouseTagData.getMouseTagData()._mousePoints.back().x = swapX;

				}

				_startDrawing = false;

			}
		}

	}

}