#include <overlay_filter_properties.h>

namespace ImGuiBaseContent
{
	OverlayFilterProperties::OverlayFilterProperties(
		struct data::OverlayFilterProperties& overlayFilterProperties
	)
		:BaseContentItem()
		, _overlayFilterProperties(overlayFilterProperties)
	{

	};

	void OverlayFilterProperties::show(bool& open)
	{
		ImGui::PushStyleColor(ImGuiCol_Header, IM_COL32(255, 160, 60, 200));
		createCollapsingHeader<OverlayFilterProperties>(&OverlayFilterProperties::createOverlayFilterContent, this, "Overlay Filter");
		ImGui::PopStyleColor();

	}

	void OverlayFilterProperties::createOverlayFilterContent()
	{
		ImGui::Spacing();
		ImGui::Checkbox("Activate Overlay Filter", &_overlayFilterProperties._activateOverlay);
		ImGui::NewLine(); 
		ImGui::Checkbox("Show On Event", &_overlayFilterProperties._showOnEvent);


		ImGui::NewLine();
		ImGui::InputFloat("Show Time", &_overlayFilterProperties._showTimeOverlay, 0.01f, _maxOverlay, "%.3f");
		if (_overlayFilterProperties._showTimeOverlay < 0)
			_overlayFilterProperties._showTimeOverlay = 0;

	}


}