#include <picture_junker.h>


namespace ImGuiBaseContent
{
	PictureJunker::PictureJunker(
		data::DataManager& dataManager
	)
		:BaseContentItem()
		,_dataManager(dataManager)
	{
	}


	void PictureJunker::show(bool& open)
	{
		//	ImGui::SetNextWindowPos(ImVec2(600, 80));

		createImGuiWindow<PictureJunker>(&PictureJunker::createItem,
			this, open, ICON_FA_IMAGES " Picture Junker", ImGuiWindowFlags_HorizontalScrollbar);
	
	}

	void PictureJunker::createItem()
	{
		
		ImGuiStyle style = ImGui::GetStyle(); 
		float windowVisibleContent = ImGui::GetWindowPos().x + ImGui::GetWindowContentRegionMax().x;

		for (int indexImageContainer = 0; indexImageContainer < _dataManager.getImageData().getImageContainer().size(); ++indexImageContainer)
		{

			ImGui::PushID(indexImageContainer + 9 * 99);


			if (_dataManager.getApplicationSetting()._imageSelected && _dataManager.getApplicationSetting()._currentSelection == indexImageContainer)
			{
				ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.2f, 0.8f, 0.8f, 1.0f });
				ImGui::PushStyleColor(ImGuiCol_Text, ImVec4{ 0.2f, 0.8f, 0.8f, 1.0f });

			}
			else
			{
			
				ImGui::PushStyleColor(ImGuiCol_Button, ImVec4{ 0.2f, 0.3f, 0.4f, 1.0f });
				ImGui::PushStyleColor(ImGuiCol_Text, ImVec4{ 0.2f, 0.3f, 0.4f, 1.0f });
			
			}

			ImGui::BeginGroup();

			if (_dataManager.getApplicationSetting()._lastIndex != indexImageContainer)
				_countSelectionOnSameIndex = 0; 
			

			if (ImGui::ImageButton(
				(void*)(intptr_t)_dataManager.getImageData().getImageContainer().at(indexImageContainer)._imageTexture,
				_imageButtonSize, ImVec2(0.0f, 0.0f), ImVec2(1.0f, 1.0f), -1,
				style.Colors[2]))
			{

				_dataManager.getApplicationSetting()._imageSelected = true;
				_dataManager.getApplicationSetting()._currentSelection = indexImageContainer;

				if (_dataManager.getApplicationSetting()._currentSelection != _dataManager.getApplicationSetting()._lastIndex)
					_dataManager.setMaskingImageData(indexImageContainer);

				if (!_dataManager.getApplicationSetting()._picToolsActivation)
					_dataManager.getApplicationSetting()._picToolsActivation = true;

				_deleteSelection = true;

				
			}
			


			ImGui::Text(_dataManager.getImageData().getImageContainer().at(indexImageContainer)._imageName.c_str());

			ImGui::EndGroup(); 
			
			ImGui::PopStyleColor(2);




			float lastButton = ImGui::GetItemRectMax().x;
			float nextButton = lastButton + style.ItemSpacing.x + _imageButtonSize.x + _dataManager.getImageData().getImageContainer().at(indexImageContainer)._imageName.length() ;

			if (indexImageContainer + 1 < _dataManager.getImageData().getImageContainer().size() && nextButton < windowVisibleContent)
				ImGui::SameLine();

		/*	std::string removeButtonTitle = "Remove '" +
				_imageData.getImageContainer().at(indexImageContainer)._imageName + "'";

			if (ImGui::BeginPopupContextItem())
			{
				if (ImGui::Button(removeButtonTitle.c_str()))
				{
					
					_imageData.removeImage(indexImageContainer);

					_applicationSetting._currentSelection = indexImageContainer;
					_deleteSelection = false;

					_removedImage = true; 

					ImGui::CloseCurrentPopup();
				}

				ImGui::EndPopup();
			}*/

		/*	data::ImageTexture item = _imageData.getImageContainer()[indexImageContainer];
			
			if (ImGui::IsItemActive() && !ImGui::IsItemHovered())
			{
				int nextItemIndex = indexImageContainer + (ImGui::GetMouseDragDelta(0).y < 0.f ? 1 : -1);
				if (nextItemIndex >= 0 && nextItemIndex < _imageData.getImageContainer().size())
				{
					_imageData.getImageContainer()[indexImageContainer] = _imageData.getImageContainer()[nextItemIndex];
					_imageData.getImageContainer()[nextItemIndex] = item;
					ImGui::ResetMouseDragDelta();
					
					_applicationSetting._currentSelection = nextItemIndex; 
				}
			}*/

			if (ImGui::BeginDragDropSource(ImGuiDragDropFlags_None))
			{
				ImGui::SetDragDropPayload("SWAP_PICTURE_POSITION", &indexImageContainer, sizeof(int));

				ImGui::ImageButton(
					(void*)(intptr_t)_dataManager.getImageData().getImageContainer().at(indexImageContainer)._imageTexture,
					_imageButtonSize, ImVec2(0.0f, 0.0f), ImVec2(1.0f, 1.0f), -1,
					style.Colors[2]); 

				if (_dataManager.getApplicationSetting()._currentSelection == indexImageContainer)
					_sameSelection = true;
				else
					_sameSelection = false; 

				ImGui::EndDragDropSource();


			}	

			if (ImGui::BeginDragDropTarget())
			{
				if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("SWAP_PICTURE_POSITION"))
				{
					IM_ASSERT(payload->DataSize == sizeof(int));
					int payloadIndex = *(const int*)payload->Data;

					_dataManager.swapData(_sameSelection, indexImageContainer, payloadIndex); 
					
				}

				ImGui::EndDragDropTarget();
			}



			if (ImGui::IsWindowFocused()
				&& _dataManager.getApplicationSetting()._currentSelection == indexImageContainer
				&& _deleteSelection
				)
			{
				_deleteKeyState = GetAsyncKeyState(VK_DELETE);
				
				//_shiftKeyState = GetAsyncKeyState(VK_SHIFT);

				if ((1 << 15) & _deleteKeyState)
				{
					
					_dataManager.removeDataForSelectedImage(indexImageContainer);
					
					_deleteSelection = false; 
				}

				//// move photo 
				//if (ImGui::GetIO().KeyShift && )
				//{


				//}


			}

			ImGui::PopID();
		}
	
	}



	PictureJunker::~PictureJunker()
	{
		// set the dock 

	}

}