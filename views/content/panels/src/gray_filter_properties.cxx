#include <gray_filter_properties.h>


namespace ImGuiBaseContent
{

	GrayFilterProperties::GrayFilterProperties(
		struct data::GrayFilterProperties& grayFilterProperties
	) 
		:BaseContentItem()
		,_grayFilterProperties(grayFilterProperties)
	{
	
	};


	void GrayFilterProperties::show(bool& open)
	{
		ImGui::PushStyleColor(ImGuiCol_Header, IM_COL32(255, 255, 255, 100));
		createCollapsingHeader<GrayFilterProperties>(&GrayFilterProperties::createGrayFilterContent, this, "Gray Filter");
		ImGui::PopStyleColor();
	}

	void GrayFilterProperties::createGrayFilterContent()
	{
		ImGui::Spacing();
		ImGui::Checkbox("Activate Gray Filter", &_grayFilterProperties._activateGrayFilter);

		ImGui::NewLine();

		ImGui::InputFloat("Move After Press", &_grayFilterProperties._moveAfterPress, 0.01f, _maxGrayFilter, "%.3f");
		if (_grayFilterProperties._moveAfterPress < 0)
			_grayFilterProperties._moveAfterPress = 0;

		ImGui::NewLine();
		ImGui::InputFloat("Press After Move", &_grayFilterProperties._pressAfterMove, 0.01f, _maxGrayFilter, "%.3f");
		if (_grayFilterProperties._pressAfterMove < 0)
			_grayFilterProperties._pressAfterMove = 0;

		ImGui::NewLine();
		ImGui::InputFloat("Keyboard Delay", &_grayFilterProperties._keyboardDelay, 0.01f, _maxGrayFilter, "%.3f");
		if (_grayFilterProperties._keyboardDelay < 0)
			_grayFilterProperties._keyboardDelay = 0;


	}

}