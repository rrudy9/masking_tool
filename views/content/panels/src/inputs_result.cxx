#include <inputs_result.h>

namespace ImGuiBaseContent
{

	InputsResult::InputsResult(
		data::MaskingInputsData& maskingInputData
	)
		:BaseContentItem()
		, _maskingInputData(maskingInputData)
	{
		_inputLabels.push_back("Keyboard Inputs");
		_inputLabels.push_back("Gamepad Button");
	};

	void InputsResult::show(bool& open)
	{
		createImGuiWindow<InputsResult>(&InputsResult::inputResults,
			this, open, "Masking Inputs Results", ImGuiWindowFlags_HorizontalScrollbar);

	}


	void InputsResult::inputResults()
	{
	
		ImGui::NewLine();

		ImGuiStyle& style = ImGui::GetStyle();

		float child_w = (ImGui::GetContentRegionAvail().x - 4 * style.ItemSpacing.x) / 2;
		float child_y = (ImGui::GetContentRegionAvail().y - 4 * style.ItemSpacing.y) / 2;

		if (child_w < 1.0f || child_y < 1.0f)
		{
			child_w = 1.0f;
			child_y = 1.0f;
		}

		bool getRightColor = false;

		// keyboard scan code, gamepad button
		for (int row = 0; row < 2; ++row)
		{
			if (row == 0)
			{
				getRightColor = _maskingInputData.getKeyboardData().getKeyboardInputData()._blackList;
			}
			else
			{
				getRightColor = _maskingInputData.getGamepadButtonData().getGamepadButtonData()._blackList;
			}

			createChildBarWindow<InputsResult>(
				&InputsResult::fillInputContent,
				row, this, _inputLabels.at(row).c_str() + row + 0,
				_inputLabels.at(row).c_str(),
				true, childWindowsFlags, ImVec2(0, child_y),
				getRightColor
				? _blackListColor : _whiteListColor,
				ImVec4(1.0f, 1.0f, 1.0f, 1.0f)
				);

			ImGui::NewLine();

		}
	
	}



	void InputsResult::fillInputContent(int row)
	{


		ImGuiStyle style = ImGui::GetStyle();
		float windowVisibleContent = ImGui::GetWindowPos().x + ImGui::GetWindowContentRegionMax().x;

		if (row == 0) // keyboard input
		{
			for (int i = 0; i < _maskingInputData.getKeyboardData().getKeyboardInputData()._keyboardInputs.size(); ++i)
			{

				ImGui::PushID(row + i * 1000); // maybe will need the id to style it globally // will see later

				ImGui::Button(
					_maskingInputData.getKeyboardData().getKeyboardInputData()._keyboardInputs.at(i)._vKeyName.c_str());

				std::string removeButtonTitle = "Remove '" +
					_maskingInputData.getKeyboardData().getKeyboardInputData()._keyboardInputs.at(i)._vKeyName + "'";

				if (ImGui::BeginPopupContextItem())
				{
					if (ImGui::Button(removeButtonTitle.c_str()))
					{
						_maskingInputData.getKeyboardData().removeKeyboardInput(
							i);

						ImGui::CloseCurrentPopup();
					}

					ImGui::EndPopup();
				}
				ImGui::PopID();

				float lastButton = ImGui::GetItemRectMax().x;
			
				float nextButton = lastButton + style.ItemSpacing.x +
					_maskingInputData.getKeyboardData().
					getKeyboardInputData()._keyboardInputs.size() <= i ? (float)_maskingInputData.getKeyboardData().
					getKeyboardInputData()._keyboardInputs.at(i)._vKeyName.length() : 0;

				
				if (i + 1 < _maskingInputData.getKeyboardData().getKeyboardInputData()._keyboardInputs.size() && nextButton < windowVisibleContent)
					ImGui::SameLine();

			}
		}
		else // gamepad button
		{

			for (int i = 0; i < _maskingInputData.getGamepadButtonData().getGamepadButtonData()._gamepadButtonList.size(); ++i)
			{

				ImGui::PushID(row + i * 1000); // maybe will need the id to style it globally // will see later

				ImGui::Button(_maskingInputData.getGamepadButtonData()
					.getGamepadButtonData()._gamepadButtonList.at(i).c_str());

				std::string removeButtonTitle = "Remove '" +
					_maskingInputData.getGamepadButtonData().getGamepadButtonData()._gamepadButtonList.at(i) + "'";


				if (ImGui::BeginPopupContextItem())
				{
					if (ImGui::Button(removeButtonTitle.c_str()))
					{
						_maskingInputData.getGamepadButtonData(). removeGamepadButton(
							i );

						ImGui::CloseCurrentPopup();
					}
					ImGui::EndPopup();
				}
				ImGui::PopID();


				float lastButton = ImGui::GetItemRectMax().x;
				float nextButton = lastButton + style.ItemSpacing.x +
					_maskingInputData.getGamepadButtonData().
					getGamepadButtonData()._gamepadButtonList.size() <= i ? (float)_maskingInputData.getGamepadButtonData().
					getGamepadButtonData()._gamepadButtonList.at(i).length() : 0;


				if (i + 1 < _maskingInputData.getGamepadButtonData().getGamepadButtonData()._gamepadButtonList.size()
					&& nextButton < windowVisibleContent)
					ImGui::SameLine();

			}

		}

	}
}