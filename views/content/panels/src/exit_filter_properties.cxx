#include <exit_filter_properties.h>

namespace ImGuiBaseContent
{
	ExitFilterProperties::ExitFilterProperties(
		struct data::ExitFilterProperties& exitFilterProperties
	)
		:BaseContentItem()
		, _exitFilterProperties(exitFilterProperties)
	{
	};

	void ExitFilterProperties::show(bool& open)
	{

		ImGui::PushStyleColor(ImGuiCol_Header, IM_COL32(255, 60, 60, 200));
		createCollapsingHeader<ExitFilterProperties>(&ExitFilterProperties::createExitFilterContent, this, "Exit Filter");
		ImGui::PopStyleColor();
	}


	void ExitFilterProperties::createExitFilterContent()
	{
		ImGui::Spacing();
		ImGui::Checkbox("Activate Exit Filter", &_exitFilterProperties._activateExitFilter);

		ImGui::NewLine();

		ImGui::InputScalar("Exit Code", ImGuiDataType_U8, &_exitFilterProperties._exitCode, &_exitCodeSteps, NULL, NULL);
		if (_exitFilterProperties._exitCode < 0)
			_exitFilterProperties._exitCode = 0;

		ImGui::NewLine();
		ImGui::InputFloat("Exit Time", &_exitFilterProperties._exitTime, 0.01f, _maxExitCode, "%.3f");
		if (_exitFilterProperties._exitTime < 0)
			_exitFilterProperties._exitTime = 0;
	}
}