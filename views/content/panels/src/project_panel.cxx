#include <project_panel.h>

namespace ImGuiBaseContent
{
	ProjectPanel::ProjectPanel(
		data::DataManager& dataManager
	)
		:BaseContentItem()
		, _fileBrowser(FileBrowser(_filePathName, _filePath))
		, _dataManager(dataManager)
		,_projectSaver(dataManager)
		,_projectLoader(dataManager)
		, _dialog(Dialog(_getDialogResponse))
		,_xmlSaver(dataManager)
	{

	}

	void ProjectPanel::show(bool& open)
	{
		createImGuiWindow<ProjectPanel>(&ProjectPanel::createItem, this, open,
			ICON_FA_SOLAR_PANEL " Project Panel", ImGuiWindowFlags_HorizontalScrollbar);

		checkState();
	}

	void ProjectPanel::createItem()
	{

		//	ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);


			//ImGui::Separator();
		ImGui::Spacing();
		ImGui::Spacing();


		ImVec2 center = ImGui::GetMainViewport()->GetCenter();
		ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));

		if (ImGui::Button(ICON_FA_FOLDER_OPEN " Load Project"))
		{
			_fileBrowser.setComponentState(FileBrowser::load_project);
			_showBrowser = true;

		}

		ImGui::SameLine();

		if (ImGui::Button(ICON_FA_SAVE " Save Project"))
		{
			if (_dataManager.getImageData().getImageContainer().size() == 0)
			{
				_dialog.setComponentSetting(Dialog::dialogState::warning, "Warning", "Please load a picture first", "Ok", "Cancel");
				_showDialog = true;
			}
			else
			{
				_fileBrowser.setComponentState(FileBrowser::save_project);
				_showBrowser = true;
			}
		}
		ImGui::SameLine();

	
		if (ImGui::Button(ICON_FA_FILE_IMAGE " Load Picture"))
		{

			_fileBrowser.setComponentState(FileBrowser::load_pictures);
			_showBrowser = true;

		}
		ImGui::Spacing();
		ImGui::Spacing();


		if (ImGui::Button(ICON_FA_FILE_EXPORT " Export As Xml"))
		{
			if (_dataManager.getImageData().getImageContainer().size() == 0)
			{
				_dialog.setComponentSetting(Dialog::dialogState::warning, "Warning", "Please load a picture first", "Ok", "Cancel");
				_showDialog = true;
			}
			else
			{
				_fileBrowser.setComponentState(FileBrowser::export_as_xml);
				_showBrowser = true;
			}
		}

		getBrowserResponse(); 
	}

	void ProjectPanel::checkState()
	{
		_fileBrowser.show(_showBrowser);
		_dialog.show(_showDialog);
		
	}


	void ProjectPanel::getBrowserResponse()
	{

		if (_filePathName.length() != 0)
		{
			if (_fileBrowser.getComponentState() == FileBrowser::browserState::load_pictures)
			{

				// we load the image first 
				if (!_dataManager.getImageData().loadImageFromFile(_filePathName))
				{
					// launch dialog exception
					_dialog.setComponentSetting(Dialog::dialogState::warning, "Warning", 
						"Can't add the following picture:" +
						_filePathName + ". The size must be " + 
						std::to_string(_dataManager.getImageData().getImageContainer().at(0)._width) + "x" + 
						std::to_string(_dataManager.getImageData().getImageContainer().at(0)._height ) , 
						"Ok", "Cancel");
					
					_showDialog = true;
				}

				// and then we initialize the data for each loaded picture
				_dataManager.setDataForEachPicture();
			}
			else if (_fileBrowser.getComponentState() == FileBrowser::browserState::export_as_xml)
			{
				_dataManager.setLastDataBeforeSave();
				_xmlSaver.exportXmlDoc(_filePathName); 
			}
			else if (_fileBrowser.getComponentState() == FileBrowser::browserState::save_project)
			{
				_dataManager.setLastDataBeforeSave(); 
				_projectSaver.saveProject(_filePath, _filePathName);
			}
			else if (_fileBrowser.getComponentState() == FileBrowser::browserState::load_project)
			{
				_projectLoader.loadProject(_filePath, _filePathName);
			}
			_filePathName = "";
			_filePath = "";
		}

	}

	ProjectPanel::~ProjectPanel()
	{}

}