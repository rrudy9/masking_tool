#include <gamepad_button_inputs.h>

namespace ImGuiBaseContent
{

	GamepadButtonInputs::GamepadButtonInputs(
		data::GamepadButtonData& gamepadButtonData
		, struct data::ApplicationSetting& applicationSetting

	)
		:BaseContentItem()
		,_gamepadButtonData(gamepadButtonData)
		,_applicationSetting(applicationSetting)
	{

	};

	void GamepadButtonInputs::show(bool& open)
	{
		createChildBarWindow<GamepadButtonInputs>(
			&GamepadButtonInputs::gamepadButtonContent,
			this, "gamepad_input", "Gamepad Button", true, childWindowsFlags,
			ImVec2(0, 110),
			_gamepadButtonData.getGamepadButtonData()._blackList
			? _blackListColor : _whiteListColor
			, ImVec4(1.0f, 1.0f, 1.0f, 1.0f)
			);

	}


	void GamepadButtonInputs::gamepadButtonContent()
	{

		if (checkIfSelectionChanged != _applicationSetting._currentSelection)
			_gamepadButton = 0; 

		ImGui::Spacing();
		ImGui::Spacing();

		createCheckLists<GamepadButtonInputs>(_gamepadButtonData.getGamepadButtonData()._blackList, _gamepadButtonData.getGamepadButtonData()._whiteList);

		ImGui::NewLine();


		ImGui::PushItemWidth(ImGui::GetContentRegionAvail().x * 1 / 2);

		ImGui::InputScalar("##Gamepad Buttons", ImGuiDataType_U8, &_gamepadButton, NULL, NULL, NULL);
		if (_gamepadButton > 99)
			_gamepadButton = 99;

		ImGui::SameLine();
		if (ImGui::Button("Add"))
		{
			if (_applicationSetting._picToolsActivation)
				_gamepadButtonData.addGamepadButton(_gamepadButton);
		}

		checkIfSelectionChanged = _applicationSetting._currentSelection;

	}

}