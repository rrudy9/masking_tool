#include <mouse_inputs.h>

namespace ImGuiBaseContent
{

	MouseInputs::MouseInputs(
		data::InputsMouseTagData& mouseData
		, struct data::ApplicationSetting& applicationSetting

	)
		:BaseContentItem()
		,_mouseData(mouseData)
		,_applicationSetting(applicationSetting)
	{
	
	};


	void MouseInputs::show(bool& open)
	{
		createChildBarWindow<MouseInputs>(
			&MouseInputs::mouseWindowContent,
			this, "mouse_input", "Mouse", true, childWindowsFlags,
			ImVec2(0, 110), _mouseData.getMouseTagData()._blackList
			? _blackListColor : _whiteListColor, ImVec4(1.0f, 1.0f, 1.0f, 1.0f)
			);
	}

	void MouseInputs::mouseWindowContent()
	{
		ImGui::Spacing();
		ImGui::Spacing();

		createCheckLists<MouseInputs>(_mouseData.getMouseTagData()._blackList, _mouseData.getMouseTagData()._whiteList);

		ImGui::NewLine();

		if (_mouseData.getMouseTagData()._blackList)
		{
			if ( ! _applicationSetting._mouseTagActivation)
				ImGui::PushStyleColor(ImGuiCol_Button, _blackListColor);
			else
				ImGui::PushStyleColor(ImGuiCol_Button, IM_COL32(255, 60, 60, 1000));
		}
		else
		{
			if ( ! _applicationSetting._mouseTagActivation)
				ImGui::PushStyleColor(ImGuiCol_Button, _whiteListColor);
			else
				ImGui::PushStyleColor(ImGuiCol_Button, IM_COL32(60, 255, 60, 140));
		}

		if (ImGui::Button("Activate Mouse Rect"))
		{
			_applicationSetting._mouseTagActivation = !_applicationSetting._mouseTagActivation;
		}

		if (_applicationSetting._mouseTagActivation)
		{
			
			_applicationSetting._tagMaskingActivation = false; // we set it directly to false 

			ImGui::SameLine();
			ImGui::Text("Mouse Tag Activated!"); 
		}

		ImGui::Spacing(); 

		ImGui::PopStyleColor();
	}

}