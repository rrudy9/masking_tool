#include <keyboard_inputs.h>

namespace ImGuiBaseContent
{
	KeyboardInputs::KeyboardInputs(
		data::InputsKeyboardData& keyboardData
		, struct data::ApplicationSetting& applicationSetting

	)
		:BaseContentItem()
		, _keyboardData(keyboardData)
		,_applicationSetting(applicationSetting)
	{
	
	};

	void KeyboardInputs::show(bool& open)
	{
	
		createChildBarWindow<KeyboardInputs>(
			&KeyboardInputs::keyBoardWindowContent,
			this, "keyboard_input", "Keyboard Input", true, childWindowsFlags,
			ImVec2(0, 110),
			_keyboardData.getKeyboardInputData()._blackList
			? _blackListColor : _whiteListColor
			, ImVec4(1.0f, 1.0f, 1.0f, 1.0f)
			);
	}

	void KeyboardInputs::keyBoardWindowContent()
	{
		if (checkIfSelectionChanged != _applicationSetting._currentSelection)
		{
			_vKey = "";
			_vKeyCharName = "";
			_scanCode = 0;
		}

		ImGui::Spacing();
		ImGui::Spacing();
		createCheckLists<KeyboardInputs>(_keyboardData.getKeyboardInputData()._blackList, _keyboardData.getKeyboardInputData()._whiteList);

		ImGui::NewLine();

		ImGui::PushItemWidth(ImGui::GetContentRegionAvail().x * 1 / 2);

		ImGui::InputTextWithHint("##keyboard_Keys_black_list",
			"Keyboard ScanCode",
			(char*)_vKeyCharName.c_str(),
			_vKeyCharName.capacity() + 1,
			ImGuiInputTextFlags_ReadOnly);

		if (ImGui::IsItemActive())
		{

			_keyboardData.startKeyboardInput(_vKey, _vKeyCharName, _scanCode);

		}
		else
		{
			_keyboardData.closeKeyboardInput();
		}

		ImGui::SameLine();
		if (ImGui::Button("Add"))
		{
			if (_applicationSetting._picToolsActivation)
			{
				if (_vKey != "")
					_keyboardData.insertKeyboardInput(_vKey, _vKeyCharName, _scanCode);
			}
		}

		checkIfSelectionChanged = _applicationSetting._currentSelection;
	
		
	}

}