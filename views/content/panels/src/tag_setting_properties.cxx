#include <tag_setting_properties.h>


namespace ImGuiBaseContent
{
	TagSettingProperties::TagSettingProperties(
		struct data::TagSettingProperties& tagSettingProperties
	)
		:BaseContentItem()
		, _tagSettingProperties(tagSettingProperties)
	{
	
	};


	void TagSettingProperties::show(bool& open)
	{
		//	ImGui::SetCursorPos(ImVec2(30, 80));

		ImGui::PushItemWidth(ImGui::GetContentRegionAvail().x * 1 / 2);


		ImGui::DragScalar("Width", ImGuiDataType_U8, &_tagSettingProperties._width, .3f, 0, &_maxWidth);
		if (_tagSettingProperties._width < _maxWidth)
			_tagSettingProperties._width = _maxWidth;


		ImGui::DragScalar("Height", ImGuiDataType_U8, &_tagSettingProperties._height, .3f, 0, &_maxHeight);
		if (_tagSettingProperties._height < _maxHeight)
			_tagSettingProperties._height = _maxHeight;


		//ImGui::SetCursorPos(ImVec2(30, 103));


	//	ImGui::SetCursorPos(ImVec2(30, 126));

		ImGui::SliderFloat("Tag", &_tagSettingProperties._percentage, 0.0f, _maxTagsPercentage, "%.3f %%");

		//ImGui::SetCursorPos(ImVec2(30, 149));

		ImGui::SliderFloat("Block", &_tagSettingProperties._blockPercentage, 0.0f, _maxTagsPercentage, "%.3f %%");

	}

}