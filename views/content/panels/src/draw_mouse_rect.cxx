#include <draw_mouse_rect.h>

namespace ImGuiBaseContent
{
	DrawMouseRect::DrawMouseRect(
		data::ImagesData& imagesData
		, data::InputsMouseTagData& inputMouseTagData
		, data::ApplicationSetting& applicationSetting
	)
		:_imagesData(imagesData)
		,_mouseTagData(inputMouseTagData)
		, _applicationSetting(applicationSetting)
	{};

	void DrawMouseRect::drawMouseRect(double currentImageWidth, double currentImageHeight, int imageSelection, ImVec2 initialCursorPosition, ImVec2 transformCursorPosition)
	{

		ImDrawList* drawList = ImGui::GetWindowDrawList();

		float deltaCursorX = transformCursorPosition.x - initialCursorPosition.x; 
		float deltaCursorY = transformCursorPosition.y - initialCursorPosition.y; 

		for (int i = 0; i < _mouseTagData.getMouseTagData()._mousePoints.size(); i += 2)
		{
			ImGui::PushID((int)_mouseTagData.getMouseTagData()._mousePoints.at(i).x + i * 1000); // maybe will need the id to style it globally // will see later

			_topLeft = ImVec2
			(
				deltaCursorX+ ImGui::GetWindowPos().x + ImGui::GetWindowContentRegionMin().x
				+ _mouseTagData.getMouseTagData()._mousePoints.at(i).x * (float)currentImageWidth / _imagesData.getImageContainer().at(imageSelection)._width

				,

				deltaCursorY + ImGui::GetWindowPos().y + ImGui::GetWindowContentRegionMin().y
				+ _mouseTagData.getMouseTagData()._mousePoints.at(i).y * (float)currentImageHeight / _imagesData.getImageContainer().at(imageSelection)._height
			);

			_bottomRight = ImVec2
			(
				deltaCursorX+ ImGui::GetWindowPos().x + ImGui::GetWindowContentRegionMin().x
				+ _mouseTagData.getMouseTagData()._mousePoints.at(i + 1).x * (float)currentImageWidth / _imagesData.getImageContainer().at(imageSelection)._width
				,

				deltaCursorY + ImGui::GetWindowPos().y + ImGui::GetWindowContentRegionMin().y
				+ _mouseTagData.getMouseTagData()._mousePoints.at(i + 1).y * (float)currentImageHeight / _imagesData.getImageContainer().at(imageSelection)._height
			),

			drawList->AddRectFilled(
				_topLeft,
				_bottomRight,
				_mouseTagData.getMouseTagData()._blackList ? 
				_blackListColor : _whiteListColor
				, 1.0f)
				;

			// draw border 
			drawRectangleBorder(&drawList, _topLeft, _bottomRight);

			ImGuiIO& io = ImGui::GetIO();

			if (_applicationSetting._mouseTagActivation
				&& ImGui::IsMouseReleased(ImGuiMouseButton_Right)
				&& io.MousePos.x > _topLeft.x && io.MousePos.x  < _bottomRight.x
				&& io.MousePos.y > _topLeft.y && io.MousePos.y < _bottomRight.y)
			{
				
				//_mouseTagData.removeMousePoints(i);

				ImGui::OpenPopupOnItemClick("remove_rectangle");
			}
	
			if (ImGui::BeginPopup("remove_rectangle"))
			{
				WindowsManager::getInstance()->getWindowMovingValue() = false;

				if (ImGui::Button("Remove This"))
				{
					_mouseTagData.removeMousePoints(i); 
				}
				
				ImGui::EndPopup();
			}


			ImGui::PopID();

		}
	}



	void DrawMouseRect::drawRectangleBorder(ImDrawList** drawList, ImVec2 topLeft, ImVec2 bottomRight)
	{

		// top border
		(*drawList)->AddLine(
			ImVec2(topLeft.x, topLeft.y)
			, ImVec2(topLeft.x + (bottomRight.x - topLeft.x), topLeft.y),
			IM_COL32(0, 0, 0, 1000),
			1.0f
		);

		// right corner
		(*drawList)->AddLine(
			ImVec2(topLeft.x + (bottomRight.x - topLeft.x), topLeft.y)
			, ImVec2(topLeft.x + (bottomRight.x - topLeft.x), bottomRight.y),
			IM_COL32(0, 0, 0, 1000),
			1.0f
		);

		// bottom corner 
		(*drawList)->AddLine(
			ImVec2(topLeft.x, bottomRight.y)
			, ImVec2(topLeft.x + (bottomRight.x - topLeft.x), bottomRight.y),
			IM_COL32(0, 0, 0, 1000),
			1.0f
		);

		// left corner
		(*drawList)->AddLine(
			ImVec2(topLeft.x, topLeft.y)
			, ImVec2(topLeft.x, bottomRight.y),
			IM_COL32(0, 0, 0, 1000),
			1.0f
		);

	}


}