#include <masking_tag.h>

namespace ImGuiBaseContent
{
	MaskingTag::MaskingTag(
		data::TagSettingProperties& maskingTagProperties
		,data::MaskingTagData& maskingTagData
	)
		:_maskingTagData(maskingTagData)
		,_tagSettingProperties(maskingTagProperties)
	{
	
	};

	void MaskingTag::getOriginalPictureSize(int width, int height)
	{
		_imageOriginalSize = ImVec2((float)width, (float)height); 
	}

	void MaskingTag::startDrawMaskTag(int currentSelectedIndex, double imageWidth, double imageHeight, ImVec2 initialCursorPosition, ImVec2 transformCursorPosition)
	{
		ImGuiIO& io = ImGui::GetIO();

		float deltaCursorX = transformCursorPosition.x - initialCursorPosition.x;
		float deltaCursorY = transformCursorPosition.y - initialCursorPosition.y;

		_currentImageRectSize = ImVec2((float)imageWidth, (float) imageHeight); 

		ImGui::SetMouseCursor(ImGuiMouseCursor_None);

		double getRatioWidth =  (_currentImageRectSize.x / _imageOriginalSize.x ); 
		double getRatioHeight = (_currentImageRectSize.y / _imageOriginalSize.y ); 

		_tagPositionInWindow.x = io.MousePos.x - ImGui::GetWindowPos().x - ImGui::GetWindowContentRegionMin().x - deltaCursorX;
		_tagPositionInWindow.y = io.MousePos.y - ImGui::GetWindowPos().y - ImGui::GetWindowContentRegionMin().y - deltaCursorY;


		// get X position in Grid 
		{
			int dividedImagePlainX = imageWidth / (_tagSettingProperties._width * getRatioWidth);
			int extractPositionInGridImagePlainX = std::ceil(_tagPositionInWindow.x * dividedImagePlainX) / imageWidth;

			float getPositionInGridX = (_tagSettingProperties._width * getRatioWidth )  * extractPositionInGridImagePlainX;
			_tagPositionInWindow.x += (getPositionInGridX - _tagPositionInWindow.x);
		}

		// get Y position in Grid 
		{ 
			int dividedImagePlainY = imageHeight / ( _tagSettingProperties._height * getRatioHeight )  ;

			int extractPositionInGridImagePlainY = std::ceil(_tagPositionInWindow.y * dividedImagePlainY) / imageHeight;

			float getPositionInGridY = ( _tagSettingProperties._height * getRatioHeight)  * extractPositionInGridImagePlainY;
			_tagPositionInWindow.y += (getPositionInGridY - _tagPositionInWindow.y);
		}


		_topLeft = ImVec2( 
			(_tagPositionInWindow.x * _imageOriginalSize.x /  _currentImageRectSize.x)	//- _tagSettingProperties._width
			,
			(_tagPositionInWindow.y * _imageOriginalSize.y /  _currentImageRectSize.y) //- _tagSettingProperties._height
		);

		_bottomRight = ImVec2(
			(_tagPositionInWindow.x *_imageOriginalSize.x /  _currentImageRectSize.x)+ _tagSettingProperties._width 
			,
			(_tagPositionInWindow.y * _imageOriginalSize.y /  _currentImageRectSize.y)+ _tagSettingProperties._height 
		);


		
		ImGui::GetForegroundDrawList()->AddRectFilled(
			ImVec2(
				_tagPositionInWindow.x - -ImGui::GetWindowPos().x +  ImGui::GetWindowContentRegionMin().x +  deltaCursorX
				, _tagPositionInWindow.y - -ImGui::GetWindowPos().y +  ImGui::GetWindowContentRegionMin().y +  deltaCursorY
			),
			ImVec2( ( _tagPositionInWindow.x - -ImGui::GetWindowPos().x +  ImGui::GetWindowContentRegionMin().x +  deltaCursorX) +_tagSettingProperties._width * getRatioWidth
			, (_tagPositionInWindow.y - -ImGui::GetWindowPos().y +  ImGui::GetWindowContentRegionMin().y +  deltaCursorY)  + _tagSettingProperties._height * getRatioHeight)
			,
			IM_COL32(219, 217, 112, 1000)
			, 1.0f
		);


		drawRectangleBorder(ImVec2(deltaCursorX, deltaCursorY), ImVec2(getRatioWidth, getRatioHeight)); 


		ImVec2 currentMousePosition = ImGui::GetIO().MousePos;
		ImGui::GetIO().MousePos.x = io.MousePos.x + _tagSettingProperties._width * getRatioWidth ;
		ImGui::GetIO().MousePos.y = io.MousePos.y + _tagSettingProperties._height * getRatioHeight ;

		ImGui::BeginTooltip();

		ImGui::Text("(%.2f,%.2f) (x,y)", _tagPositionInWindow.x, _tagPositionInWindow.y);
		ImGui::Text("(%.2f,%.2f) (w,h)", _currentImageRectSize.x, _currentImageRectSize.y);

		ImGui::EndTooltip();
		ImGui::GetIO().MousePos = currentMousePosition;

		if (_tagPositionInWindow.x < 0.0f) { _tagPositionInWindow.x = 0.0f; }
		if (_tagPositionInWindow.y < 0.0f) { _tagPositionInWindow.y = 0.0f; }

		if (ImGui::IsMouseClicked(ImGuiMouseButton_Left))
			_maskingTagData.addMaskingTag(currentSelectedIndex,_topLeft, _bottomRight);
	}


	void MaskingTag::drawRectangleBorder( ImVec2 cursor, ImVec2 ratio )
	{
		ImGui::GetForegroundDrawList()->AddLine(
				ImVec2( (_tagPositionInWindow.x - -ImGui::GetWindowPos().x + ImGui::GetWindowContentRegionMin().x  + cursor.x )
					, ( _tagPositionInWindow.y - -ImGui::GetWindowPos().y + ImGui::GetWindowContentRegionMin().y + cursor.y )
				)
				, ImVec2( (_tagPositionInWindow.x  - -ImGui::GetWindowPos().x +
					ImGui::GetWindowContentRegionMin().x + cursor.x) + _tagSettingProperties._width * ratio.x 
					, _tagPositionInWindow.y - -ImGui::GetWindowPos().y + ImGui::GetWindowContentRegionMin().y  + cursor.y
				),
				IM_COL32(0,0,0, 1000),
				1.0f
			);

		// right corner
		ImGui::GetForegroundDrawList()->AddLine(
			 ImVec2((_tagPositionInWindow.x - -ImGui::GetWindowPos().x +
				ImGui::GetWindowContentRegionMin().x + cursor.x) + _tagSettingProperties._width * ratio.x
				, _tagPositionInWindow.y - -ImGui::GetWindowPos().y + ImGui::GetWindowContentRegionMin().y + cursor.y
			),
			ImVec2((_tagPositionInWindow.x - -ImGui::GetWindowPos().x +
				ImGui::GetWindowContentRegionMin().x + cursor.x) + _tagSettingProperties._width * ratio.x
				, ( _tagPositionInWindow.y - -ImGui::GetWindowPos().y + ImGui::GetWindowContentRegionMin().y + cursor.y) + _tagSettingProperties._height * ratio.y
			),
			IM_COL32(0, 0, 0, 1000),
			1.0f
		);

		// bottom corner 
		ImGui::GetForegroundDrawList()->AddLine(
			ImVec2((_tagPositionInWindow.x - -ImGui::GetWindowPos().x +
				ImGui::GetWindowContentRegionMin().x + cursor.x) 
				, (_tagPositionInWindow.y - -ImGui::GetWindowPos().y + ImGui::GetWindowContentRegionMin().y + cursor.y) + _tagSettingProperties._height * ratio.y
			),
			ImVec2((_tagPositionInWindow.x - -ImGui::GetWindowPos().x +
				ImGui::GetWindowContentRegionMin().x + cursor.x) + _tagSettingProperties._width * ratio.x
				, (_tagPositionInWindow.y - -ImGui::GetWindowPos().y + ImGui::GetWindowContentRegionMin().y + cursor.y) + _tagSettingProperties._height * ratio.y
			),
			IM_COL32(0, 0, 0, 1000),
			1.0f
		);

		//// left corner
		ImGui::GetForegroundDrawList()->AddLine(
			ImVec2((_tagPositionInWindow.x - -ImGui::GetWindowPos().x +
				ImGui::GetWindowContentRegionMin().x + cursor.x)
				, (_tagPositionInWindow.y - -ImGui::GetWindowPos().y + ImGui::GetWindowContentRegionMin().y + cursor.y)
			),
			ImVec2((_tagPositionInWindow.x - -ImGui::GetWindowPos().x +
				ImGui::GetWindowContentRegionMin().x + cursor.x)
				, (_tagPositionInWindow.y - -ImGui::GetWindowPos().y + ImGui::GetWindowContentRegionMin().y + cursor.y) + _tagSettingProperties._height * ratio.y
			),
			IM_COL32(0, 0, 0, 1000),
			1.0f
		);


	}

}