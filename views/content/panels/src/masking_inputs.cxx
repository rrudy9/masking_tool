#include <masking_inputs.h>

namespace ImGuiBaseContent
{


	MaskingInputs::MaskingInputs(
		data::MaskingInputsData& maskingInpuiData
		, struct data::ApplicationSetting& applicationSetting
		)
		:BaseContentItem()
		, _maskingInputData(maskingInpuiData)
		,_keyboardInputs(KeyboardInputs(maskingInpuiData.getKeyboardData(), applicationSetting))
		,_mouseInputs(MouseInputs(maskingInpuiData.getMouseInputData(), applicationSetting))
		,_gamepadButtonInputs(GamepadButtonInputs(maskingInpuiData.getGamepadButtonData(), applicationSetting))
		,_gamepadAnalogInputs(GamepadAnalogInputs(maskingInpuiData.getGamepadAnalogData()))
		, _maskingInputResult(InputsResult(maskingInpuiData))
	{

	}

	void MaskingInputs::show(bool& open)
	{
		createImGuiWindow<MaskingInputs>(&MaskingInputs::createItem,
			this, open, "Masking Inputs", ImGuiWindowFlags_HorizontalScrollbar);
	}

	void MaskingInputs::createItem()
	{

		ImGuiStyle& style = ImGui::GetStyle();

		float child_w = (ImGui::GetContentRegionAvail().x - 4 * style.ItemSpacing.x) / 2;
		float child_y = (ImGui::GetContentRegionAvail().y - 4 * style.ItemSpacing.y) / 2;


		ImGui::Spacing();
		ImGui::Checkbox("Mouse", &_showMouseViewContent);
		ImGui::SameLine();
	
		ImGui::Checkbox("Keyboard Input", &_showKeyboardViewContent);
		ImGui::SameLine();

		ImGui::Checkbox("Gamepad Button", &_showGamepadButtonViewContent);
		ImGui::SameLine();
		
		ImGui::Checkbox("Pads", &_showPads);



		
		if (_showMouseViewContent)
		{
			ImGui::NewLine();
			_mouseInputs.show(_showMouseViewContent);
		}


		if (_showKeyboardViewContent)
		{
			ImGui::NewLine();
			_keyboardInputs.show(_showKeyboardViewContent);

		};


		if (_showGamepadButtonViewContent)
		{
			ImGui::NewLine();

			_gamepadButtonInputs.show(_showGamepadButtonViewContent);
		
		}


		if (_showPads)
		{
			ImGui::NewLine();
			_gamepadAnalogInputs.show(_showPads);
		}

		ImGui::NewLine();
		ImGui::Checkbox("Show Your Input Results", &_showInputResults);		
		
		
		if (_showInputResults)
		{
			_maskingInputResult.show(_showInputResults);
		}

	}


	MaskingInputs::~MaskingInputs()
	{

	}


}


