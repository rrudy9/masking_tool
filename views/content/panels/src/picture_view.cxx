

#include <picture_view.h>

namespace ImGuiBaseContent
{
	PictureView::PictureView(
		data::DataManager& dataManager
	)
		:BaseContentItem()
		,_imageData(dataManager.getImageData())
		,_applicationSetting(
			dataManager.getApplicationSetting())
		,_mouseTagInput(
			dataManager.getInputsData().getMouseInputData()
		)
		,_drawMouseRect(
			dataManager.getImageData(),
			dataManager.getInputsData().getMouseInputData()
			, dataManager.getApplicationSetting()
		)
		,_maskingTag(
			dataManager.getPropertiesData().getTagSetting(),
			dataManager.getMaskingTagData()
		)
		, _drawMaskTag(
			dataManager.getImageData()
			, dataManager.getMaskingTagData()
			, dataManager.getApplicationSetting()
		)
	{

	}


	void PictureView::setSelectedImage()
	{
		
		if (_applicationSetting._imageSelected)
		{
			if (!_applicationSetting._firstSelection)
			{
				_applicationSetting._firstSelection = true;

				/// first time set image and height for picture
				_imageWidth = _imageData.getImageContainer().at(_applicationSetting._currentSelection)._width;
				_imageHeight = _imageData.getImageContainer().at(_applicationSetting._currentSelection)._height;


				_mouseTagInput.getOriginalPictureSize(
					_imageData.getImageContainer().at(_applicationSetting._currentSelection)._width
					, _imageData.getImageContainer().at(_applicationSetting._currentSelection)._height
				);

				_maskingTag.getOriginalPictureSize(
					_imageData.getImageContainer().at(_applicationSetting._currentSelection)._width
					, _imageData.getImageContainer().at(_applicationSetting._currentSelection)._height
				);

			}
		}

	}

	void PictureView::show(bool& open)
	{
		setSelectedImage();

		//	ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0, 0));
		createImGuiWindow<PictureView>(&PictureView::createItem,
			this, open, ICON_FA_IMAGE " Picture View", ImGuiWindowFlags_HorizontalScrollbar | ImGuiWindowFlags_NoScrollWithMouse);
		//	ImGui::PopStyleVar();
	}


	void PictureView::createItem()
	{
		ImGuiIO& io = ImGui::GetIO();

		_initialCursorPos = ImGui::GetCursorPos();
			
		_initScreenCursor = ImGui::GetCursorScreenPos(); 


		ImVec2 imagePosition = ImVec2( ImGui::GetWindowSize().x - _imageWidth ,
			ImGui::GetWindowSize().y - _imageHeight );

		_imageCursorPos = ImVec2(imagePosition.x * 0.5f, imagePosition.y * 0.5f);


		_imageCursorPos.x = clip(_imageCursorPos.x, _initialCursorPos.x, _imageCursorPos.x);
		_imageCursorPos.y = clip(_imageCursorPos.y, _initialCursorPos.y, _imageCursorPos.y);
			
		ImGui::SetCursorPos(_imageCursorPos);

		if (_applicationSetting._currentSelection <= _imageData.getImageContainer().size() - 1
			&& _imageData.getImageContainer().size() && _applicationSetting._imageSelected
			)
		{
			ImGui::Image(
				(void*)(intptr_t)_imageData.getImageContainer()
				.at(_applicationSetting._currentSelection)._imageTexture,
				ImVec2{ (float)_imageWidth,(float)_imageHeight} 
			);

			if (ImGui::IsItemHovered())
			{
				WindowsManager::getInstance()->getWindowMovingValue() = false; 

				zoomPicture();


				if (_applicationSetting._mouseTagActivation)
				{
					_mouseTagInput.startDrawMouseTag(_imageWidth, _imageHeight, _initialCursorPos, _imageCursorPos);
				}

				if (_applicationSetting._tagMaskingActivation)
				{
					_maskingTag.startDrawMaskTag(_applicationSetting._currentSelection, _imageWidth, _imageHeight, _initialCursorPos, _imageCursorPos);
				}

				dragImage();

			}
			else
			{
				WindowsManager::getInstance()->getWindowMovingValue() = true; 
			}

			// draw mouse rect
			_drawMouseRect.drawMouseRect(_imageWidth, _imageHeight, _applicationSetting._currentSelection, _initialCursorPos, _imageCursorPos);

			_drawMaskTag.drawTagRect(_imageWidth, _imageHeight, _applicationSetting._currentSelection, _initialCursorPos, _imageCursorPos);

		} 
		
	}


	float PictureView::clip(float n, float lower, float upper) 
	{
		return std::max(lower, std::min(n, upper));
	}

	void PictureView::dragImage()
	{

		if (ImGui::IsMouseDragging(ImGuiMouseButton_Right)
			)
		{

			ImGui::SetMouseCursor(ImGuiMouseCursor_Hand);

			ImGui::SetScrollX(ImGui::GetScrollX() - ImGui::GetIO().MouseDelta.x);
			ImGui::SetScrollY(ImGui::GetScrollY() - ImGui::GetIO().MouseDelta.y);

		}

	}

	void PictureView::zoomPicture()
	{
	
		ImGuiIO& io = ImGui::GetIO();
		
		float deltaCursorX = _imageCursorPos.x - _initialCursorPos.x;
		float deltaCursorY = _imageCursorPos.y - _initialCursorPos.y;

		if (io.MouseWheel)
		{
			if (io.MouseWheel > 0 || io.MouseWheelH > 0)
			{
			
				_imageHeight = _imageHeight * 1.02;
				_imageWidth = _imageWidth * 1.02;
		
				double dx = ( io.MousePos.x - ImGui::GetWindowPos().x - ImGui::GetWindowContentRegionMin().x - deltaCursorX ) * (1.02 - 1); 
				double dy = ( io.MousePos.y - ImGui::GetWindowPos().y - ImGui::GetWindowContentRegionMin().y - deltaCursorY ) * (1.02 - 1);
				
				ImGui::SetScrollX( ImGui::GetScrollX() +  dx );
				ImGui::SetScrollY( ImGui::GetScrollY() +  dy );

			}
			else
			{
				_imageHeight = _imageHeight / 1.02;
				_imageWidth = _imageWidth / 1.02;

				double dx = (io.MousePos.x - ImGui::GetWindowPos().x - ImGui::GetWindowContentRegionMin().x - deltaCursorX ) * (1 / 1.02 - 1);
				double dy = (io.MousePos.y - ImGui::GetWindowPos().y - ImGui::GetWindowContentRegionMin().y - deltaCursorY ) * (1 / 1.02 - 1);

				ImGui::SetScrollX( ImGui::GetScrollX() + dx);
				ImGui::SetScrollY( ImGui::GetScrollY() + dy);

			}
		}
	}


	PictureView::~PictureView()
	{

	}

}