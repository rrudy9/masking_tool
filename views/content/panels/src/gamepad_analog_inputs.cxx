#include <gamepad_analog_inputs.h>

namespace ImGuiBaseContent
{

	GamepadAnalogInputs::GamepadAnalogInputs(
		std::vector<data::GamepadAnalogInput>& gamepadAnalogList
	)
		:BaseContentItem()
		,_gamepadAnalogList(gamepadAnalogList)
	{
	
		_padsLabel.push_back("Rx");
		_padsLabel.push_back("Ry");
		_padsLabel.push_back("Lx");
		_padsLabel.push_back("Ly");
	};


	void GamepadAnalogInputs::show(bool& open)
	{
		createChildBarWindow<GamepadAnalogInputs>(
			&GamepadAnalogInputs::padInputContentWindow,
			this, "pad_list", "Pad", true, childWindowsFlags,
			ImVec2(0, 200), _whiteListColor, ImVec4(1.0f, 1.0f, 1.0f, 1.0f)
			);
	}


	void GamepadAnalogInputs::padInputContentWindow()
	{

		
		if (ImGui::BeginTable("split", 3, ImGuiTableFlags_None))
		{
			for (int i = 0; i < 4; ++i)
			{
				ImGui::NewLine();
				for (int col = 0; col < 3; ++col)
				{

					if (col == 0)
					{
						ImGui::TableNextColumn();
						ImGui::Text("%s", _padsLabel[i].c_str());
					}
					else
					{

						std::string id = "##" + _padsLabel[i] + "_" + (char)col;
						ImGui::TableNextColumn();
						ImGui::DragFloat( id.c_str(), col == 1 ? &_gamepadAnalogList[i]._minInputValue : &_gamepadAnalogList[i]._maxInputValue
							, 0.005f,
							_minimumPadValue,
							_maximumPadValue);

					}
				}
			}
			ImGui::EndTable();
		}

	
	}
}

