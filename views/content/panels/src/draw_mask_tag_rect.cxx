#include <draw_mask_tag_rect.h>

namespace ImGuiBaseContent
{
	DrawMaskTagRect::DrawMaskTagRect(
		data::ImagesData& imagesData
		, data::MaskingTagData& maskingTagData
		, data::ApplicationSetting& applicationSetting
	)
		:_imagesData(imagesData)
		,_maskingTagData(maskingTagData)
		,_applicationSetting(applicationSetting)
	{
	};



	void DrawMaskTagRect::drawTagRect(double currentImageWidth, double currentImageHeight, int imageSelection, ImVec2 initialCursorPosition, ImVec2 transformCursorPosition)
	{
		ImDrawList* drawList = ImGui::GetWindowDrawList();

		float deltaCursorX = transformCursorPosition.x - initialCursorPosition.x;
		float deltaCursorY = transformCursorPosition.y - initialCursorPosition.y;


		for (int tagFoSelectedImage = 0; tagFoSelectedImage < _maskingTagData.getMaskingTagData().size(); ++tagFoSelectedImage)
		{

			for (int tagPoints = 0; tagPoints < _maskingTagData.getMaskingTagData().at(tagFoSelectedImage).size(); tagPoints += 2)
			{

				ImGui::PushID((int)_maskingTagData.getMaskingTagData().at(tagFoSelectedImage).at(tagPoints).x + tagPoints * 10000); // maybe will need the id to style it globally // will see later

				_topLeft = ImVec2
				(
					deltaCursorX + ImGui::GetWindowPos().x + ImGui::GetWindowContentRegionMin().x
					+ _maskingTagData.getMaskingTagData().at(tagFoSelectedImage).at(tagPoints).x * (float) currentImageWidth / _imagesData.getImageContainer().at(imageSelection)._width
					,
					deltaCursorY + ImGui::GetWindowPos().y + ImGui::GetWindowContentRegionMin().y
					+ _maskingTagData.getMaskingTagData().at(tagFoSelectedImage).at(tagPoints).y * (float)currentImageHeight / _imagesData.getImageContainer().at(imageSelection)._height
				);

				_bottomRight = ImVec2
				(
					deltaCursorX + ImGui::GetWindowPos().x + ImGui::GetWindowContentRegionMin().x
					+ _maskingTagData.getMaskingTagData().at(tagFoSelectedImage).at(tagPoints + 1).x * (float)currentImageWidth / _imagesData.getImageContainer().at(imageSelection)._width
					,
					deltaCursorY + ImGui::GetWindowPos().y + ImGui::GetWindowContentRegionMin().y
					+ _maskingTagData.getMaskingTagData().at(tagFoSelectedImage).at(tagPoints + 1).y * (float)currentImageHeight / _imagesData.getImageContainer().at(imageSelection)._height

				);


				drawList->AddRectFilled(
					_topLeft,
					_bottomRight
					,imageSelection == tagFoSelectedImage ?
					IM_COL32(219, 217, 112, 1000) : IM_COL32(0, 0, 255, 1000)
					, 1.0f
				);

				// draw border 
				drawRectangleBorder(&drawList, _topLeft, _bottomRight); 
				
				ImGuiIO& io = ImGui::GetIO();
				if (imageSelection == tagFoSelectedImage
					&& _applicationSetting._tagMaskingActivation
					)
				{


					if (ImGui::IsMouseReleased(ImGuiMouseButton_Right)
						&& io.MousePos.x > _topLeft.x && io.MousePos.x  < _bottomRight.x
						&& io.MousePos.y > _topLeft.y && io.MousePos.y < _bottomRight.y)
					{
						
						_maskingTagData.removeMaskingTag(imageSelection, tagPoints);

					}
				



					//if (ImGui::BeginPopup("remove_tag"))
					//{
					//	if (ImGui::Button("Remove This")) 
					//	{
				//		}

					//	ImGui::EndPopup();
//					}
				}
				ImGui::PopID();
			}
		}



	}


	void DrawMaskTagRect::drawRectangleBorder(ImDrawList** drawList, ImVec2 topLeft, ImVec2 bottomRight)
	{

		// top border
		(*drawList)->AddLine(
			ImVec2(topLeft.x, topLeft.y)
			, ImVec2(topLeft.x + (bottomRight.x - topLeft.x), topLeft.y),
			IM_COL32(0,0,0, 1000),
			1.0f
		);

		// right corner
		(*drawList)->AddLine(
			ImVec2(topLeft.x + (bottomRight.x - topLeft.x), topLeft.y)
			, ImVec2(topLeft.x + (bottomRight.x - topLeft.x), bottomRight.y),
			IM_COL32(0,0,0, 1000),
			1.0f
		);

		// bottom corner 
		(*drawList)->AddLine(
			ImVec2(topLeft.x , bottomRight.y)
			, ImVec2(topLeft.x + (bottomRight.x - topLeft.x), bottomRight.y),
			IM_COL32(0,0,0, 1000),
			1.0f
		);

		// left corner
		(*drawList)->AddLine(
			ImVec2(topLeft.x, topLeft.y)
			, ImVec2(topLeft.x, bottomRight.y),
			IM_COL32(0, 0, 0, 1000),
			1.0f
		);
	
	}


}