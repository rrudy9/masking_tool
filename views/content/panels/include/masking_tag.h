#pragma once 

#include <vector>
#include <data_manager.h>
#include <masking_properties_data.h>

namespace ImGuiBaseContent
{
	class MaskingTag
	{
	public:

		MaskingTag(
			data::TagSettingProperties& maskingTagProperties,
			data::MaskingTagData& maskingTagData
		);

		void getOriginalPictureSize(int width, int height);

		void startDrawMaskTag(int currentSelectedIndex, double imageWidth, double imageHeight, ImVec2 initialCursorPosition, ImVec2 transformCursorPosition);

	private:

		ImVec2 _tagPositionInWindow;

		ImVec2 _imageOriginalSize;

		ImVec2 _currentImageRectSize; 

		ImVec2 _topLeft; 
		ImVec2 _bottomRight; 

		data::TagSettingProperties& _tagSettingProperties;
		data::MaskingTagData& _maskingTagData; 
	
		void drawRectangleBorder(ImVec2 cursor, ImVec2 ratio);

	};
}