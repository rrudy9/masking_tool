#pragma once

#include <base_content.h>
#include <masking_inputs_data.h>
#include <data_manager.h>

namespace ImGuiBaseContent
{

	class KeyboardInputs : public BaseContentItem
	{
	public :
		KeyboardInputs(
			data::InputsKeyboardData& keyboardData
			, struct data::ApplicationSetting& applicationSetting
		);

		void show(bool& open) override; 

	private:

		ImU32 _blackListColor = IM_COL32(120, 46, 49, 1000);
		ImU32 _whiteListColor = IM_COL32(50, 84, 60, 2000);

		std::string _vKey = "";
		std::string _vKeyCharName = "";

		UINT _scanCode = 0;

		void keyBoardWindowContent();

		ImGuiWindowFlags childWindowsFlags =
			ImGuiWindowFlags_HorizontalScrollbar |
			ImGuiWindowFlags_MenuBar;

		data::InputsKeyboardData& _keyboardData;

		struct data::ApplicationSetting& _applicationSetting;

		int checkIfSelectionChanged = 0;


	};
}