#pragma once

#include <base_content.h>

#include <masking_properties_data.h>

namespace ImGuiBaseContent
{

	class OverlayFilterProperties : public BaseContentItem
	{

	public:
		OverlayFilterProperties(
			struct data::OverlayFilterProperties& overlayFilterProperties
		);
	
		void show(bool& open) override; 

	private:

		void createOverlayFilterContent();


		// overlay filter max
		float _maxOverlay = 1.0;

		struct data::OverlayFilterProperties& _overlayFilterProperties; 


	};
}