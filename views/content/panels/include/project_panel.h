#pragma once


#include <base_content.h>
#include <file_browser.h>
#include <data_manager.h>

#include <dialog.h>

#include <project_saver.h>
#include <project_loader.h>
#include <xml_saver.h>

namespace ImGuiBaseContent
{
	class ProjectPanel : public BaseContentItem
	{
	public:

		ProjectPanel(
			data::DataManager& imageData
		);
		~ProjectPanel();

		void show(bool& open) override;
	
	private:

		void createItem();
		void checkState();

		bool _showBrowser = false;

		FileBrowser _fileBrowser;
		Dialog _dialog; 

		bool _showDialog = false; 
		bool _getDialogResponse = false; 

		data::DataManager& _dataManager;

		data::ProjectLoader _projectLoader; 
		data::ProjectSaver _projectSaver; 
		data::XmlSaver _xmlSaver; 


		std::string _filePathName = ""; 
		std::string _filePath = "";

		void getBrowserResponse();
	
	};

}