#pragma once

#include <images_data.h>
#include <masking_tag_data.h>
#include <data_manager.h>

namespace ImGuiBaseContent
{
	
	class DrawMaskTagRect
	{
	public:

		DrawMaskTagRect(
			data::ImagesData& imagesData
			,data::MaskingTagData& maskingTagData
			,data::ApplicationSetting& applicationSetting
		); 

		void drawTagRect(double currentImageWidth, double currentImageHeight, int imageSelection, ImVec2 initialCursorPosition, ImVec2 transformCursorPosition);

	private:

		ImVec2 _topLeft, _bottomRight; 

		data::ImagesData& _imagesData; 
		data::MaskingTagData& _maskingTagData; 
		data::ApplicationSetting& _applicationSetting; 
	
		void drawRectangleBorder(ImDrawList** drawList, ImVec2 topLeft, ImVec2 bottomRight);

	};

}