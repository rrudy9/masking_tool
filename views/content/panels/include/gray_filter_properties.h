#pragma once

#include <base_content.h>
#include <masking_properties_data.h>

namespace ImGuiBaseContent
{
	class GrayFilterProperties : public BaseContentItem
	{

	public:
		GrayFilterProperties(
			struct data::GrayFilterProperties& grayFilterProperties
		);

		void show(bool& open) override; 

	private:

		void createGrayFilterContent();

		// gray filter max
		float _maxGrayFilter = 1.0;

		struct data::GrayFilterProperties& _grayFilterProperties; 
	};

}