#pragma once

#include <base_content.h>
#include <inputs_mouse_tag_data.h>

namespace ImGuiBaseContent
{
	class MouseTagInput 
	{
	public:

		MouseTagInput(
			data::InputsMouseTagData& inputMouseTagData
		);


		void getOriginalPictureSize(int width, int height);
		void startDrawMouseTag(double imageWidth, double imageHeight, ImVec2 initialCursorPosition, ImVec2 transformCursorPosition);

		

	private:

		data::InputsMouseTagData& _inputMouseTagData; 
		ImVec2 _currentImageRectSize;

		ImVec2 _imageOriginalSize; 

		ImVec2 _mousePositionInWindow; 
		
		ImVec2 _mousePositionInImage; 
		
		bool _startDrawing = false; 


	};

}