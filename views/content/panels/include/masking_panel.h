#pragma once 

#include <base_content.h>
#include <masking_properties.h>
#include <masking_inputs.h>
#include <masking_events.h>

#include <data_manager.h>

namespace ImGuiBaseContent
{

	class MaskingPanel : public BaseContentItem
	{
	public:
		MaskingPanel(data::DataManager& dataManager);
		~MaskingPanel();

		void show(bool& open) override;

	private:
		void createItem();
		void checkState();             

		bool _activePropertiesMasking = true;
		bool _activeInputMasking = true;
		bool _activeEventMasking = true;


		MaskingProperties _properties;
		MaskingInputs _inputs;
		MaskingEvents _events;

		data::DataManager& _dataManager;


	};

}