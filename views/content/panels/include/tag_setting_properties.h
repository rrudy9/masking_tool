#pragma once

#include <base_content.h>
#include <masking_properties_data.h>

namespace ImGuiBaseContent
{
	class TagSettingProperties : public BaseContentItem
	{

	public:
		TagSettingProperties(
			struct data::TagSettingProperties& tagSettingProperties
		);

		void show(bool& open) override;

	private:

		uint8_t _maxWidth = 16;//32;
		uint8_t _maxHeight = 16;//32;

		float _maxTagsPercentage = 1.0;

		struct data::TagSettingProperties& _tagSettingProperties;
	};

}