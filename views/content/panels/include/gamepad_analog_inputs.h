#pragma once

#include <base_content.h>

#include <masking_inputs_data.h>

namespace ImGuiBaseContent
{

	class GamepadAnalogInputs : public BaseContentItem
	{
	public:
		GamepadAnalogInputs(
			std::vector <data::GamepadAnalogInput>& gamepadAnalogList
		);

		void show(bool& open) override; 

	private:
		std::vector<std::string> _padsLabel;

		float _minimumPadValue = -1.0;
		float _maximumPadValue = 1.0;

		ImGuiWindowFlags childWindowsFlags =
			ImGuiWindowFlags_HorizontalScrollbar |
			ImGuiWindowFlags_MenuBar;

		void padInputContentWindow();

		ImU32 _whiteListColor = IM_COL32(50, 84, 60, 2000);


		std::vector<data::GamepadAnalogInput>& _gamepadAnalogList;
	};
}