#pragma once

#include <base_content.h>
#include <masking_inputs_data.h>
#include <data_manager.h>
namespace ImGuiBaseContent
{

	class MouseInputs : public BaseContentItem
	{
	public: 

		MouseInputs(
			data::InputsMouseTagData& mouseData
			, data::ApplicationSetting& applicationSetting
		);

		void show(bool& open) override; 

	private:

		ImGuiWindowFlags childWindowsFlags =
			ImGuiWindowFlags_HorizontalScrollbar |
			ImGuiWindowFlags_MenuBar;

		ImU32 _blackListColor = IM_COL32(120, 46, 49, 1000);
		ImU32 _whiteListColor = IM_COL32(50, 84, 60, 2000);

		void mouseWindowContent();
		
		data::InputsMouseTagData& _mouseData;

		data::ApplicationSetting& _applicationSetting;

	};
}