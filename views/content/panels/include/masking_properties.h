#pragma once

#include <base_content.h>

#include <masking_properties_data.h>
#include <data_manager.h>
#include <gray_filter_properties.h>
#include <tag_setting_properties.h>
#include <overlay_filter_properties.h>
#include <exit_filter_properties.h>

namespace ImGuiBaseContent
{

	class MaskingProperties : public BaseContentItem
	{
	public:
		MaskingProperties(
			data::MaskingPropertiesData& maskingPropertiesData
		);
		~MaskingProperties();

		void show(bool& open) override;


	private:
		void createItem();

		bool _showPropertiesContent = true; 

		TagSettingProperties _tagSettingProperties;
		GrayFilterProperties _grayFilterProperties; 
		OverlayFilterProperties _overlayFilterProperties; 
		ExitFilterProperties _exitFilterProperties; 

		data::MaskingPropertiesData& _maskingPropertiesData;

	};

}