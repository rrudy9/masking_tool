#pragma once 

#include <images_data.h>
#include <mouse_tag_input.h>

#include <data_manager.h>

#include <windows_manager.h>

namespace ImGuiBaseContent
{

	class DrawMouseRect
	{
	public:

		DrawMouseRect(
			data::ImagesData& imagesData
			,data::InputsMouseTagData& inputMouseTagData
			,data::ApplicationSetting& applicationSetting

		);

		void drawMouseRect(double currentImageWidth, double currentImageHeight, int imageSelection, ImVec2 initialCursorPosition, ImVec2 transformCursorPosition);


	private:

		data::ImagesData& _imagesData;
		data::InputsMouseTagData& _mouseTagData;

		ImU32 _blackListColor = IM_COL32(170, 46, 49, 180);
		ImU32 _whiteListColor = IM_COL32(50, 164, 60, 140);

		ImVec2 _topLeft; 
		ImVec2 _bottomRight; 
		data::ApplicationSetting& _applicationSetting;

		void drawRectangleBorder(ImDrawList** drawList, ImVec2 topLeft, ImVec2 bottomRight);

	};
}