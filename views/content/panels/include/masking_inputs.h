#pragma once

#include <base_content.h>

#include <data_manager.h>
#include <masking_inputs_data.h>
#include <keyboard_inputs.h>
#include <gamepad_button_inputs.h>
#include <mouse_inputs.h>
#include <gamepad_analog_inputs.h>
#include <inputs_result.h>

namespace ImGuiBaseContent
{
	class MaskingInputs : public BaseContentItem
	{
	public:

		enum class inputSettingLabel { black_list = 0, white_list };

		MaskingInputs(
			data::MaskingInputsData& maskingInpuiData
			, struct data::ApplicationSetting& applicationSetting
		);
		~MaskingInputs();

		void show(bool& open) override;

	private:
		void createItem();


		bool _showKeyboardViewContent = true;
		bool _showMouseViewContent = true;
		bool _showGamepadButtonViewContent = true;
		bool _showPads = true;
		bool _showInputResults = true;


		KeyboardInputs _keyboardInputs; 
		MouseInputs _mouseInputs; 
		GamepadButtonInputs _gamepadButtonInputs; 
		GamepadAnalogInputs _gamepadAnalogInputs;
		InputsResult _maskingInputResult; 

		data::MaskingInputsData& _maskingInputData;

	};
}

