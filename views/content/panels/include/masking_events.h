#pragma once
#include <base_content.h>

#include <data_manager.h>

namespace ImGuiBaseContent
{
	class MaskingEvents : public BaseContentItem
	{
	public:
		MaskingEvents(
			data::MaskingEventsData& maskingEventData
			, struct data::ApplicationSetting& applicationSetting
		);
		~MaskingEvents();

		void show(bool& open) override;

	private:
		void createItem();
		
		void createTableEventItem();


		uint8_t _sizeOfEvenItemsArray = 4;

		uint8_t _currentEventSelectedComboBox = 0; 

		std::string _eventValue = "" ; 

		data::MaskingEventsData& _maskingEventsData;

		static int MyResizeCallback(ImGuiInputTextCallbackData* data);

		struct data::ApplicationSetting& _applicationSetting; 

		int checkIfSelectionChanged = 0; 

	};

} 