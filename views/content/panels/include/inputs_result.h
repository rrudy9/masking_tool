#pragma once


#include <base_content.h>
#include <masking_inputs_data.h>

namespace ImGuiBaseContent
{

	class InputsResult : public BaseContentItem
	{
	public:
		
		InputsResult(
			data::MaskingInputsData& maskingInputData
		);

		void show(bool& open) override; 

	private: 

		data::MaskingInputsData& _maskingInputData;

		ImGuiWindowFlags childWindowsFlags =
			ImGuiWindowFlags_HorizontalScrollbar |
			ImGuiWindowFlags_MenuBar;
		
		
		void inputResults();

		ImU32 _blackListColor = IM_COL32(120, 46, 49, 1000);
		ImU32 _whiteListColor = IM_COL32(50, 84, 60, 2000);

		std::vector<std::string> _inputLabels; 
		
		void fillInputContent(int row);

	};
}