#pragma once

#include <base_content.h>

#include <masking_inputs_data.h>
#include <data_manager.h>

namespace ImGuiBaseContent
{
	class GamepadButtonInputs : public BaseContentItem
	{
	public:
		GamepadButtonInputs(
			data::GamepadButtonData& gamepadButtonData
			, struct data::ApplicationSetting &applicationSetting
		); 
		void show(bool& open) override; 


	private:

		ImGuiWindowFlags childWindowsFlags =
			ImGuiWindowFlags_HorizontalScrollbar |
			ImGuiWindowFlags_MenuBar;

		void gamepadButtonContent();

		ImU32 _blackListColor = IM_COL32(120, 46, 49, 1000);
		ImU32 _whiteListColor = IM_COL32(50, 84, 60, 2000);

		uint8_t _gamepadButton = 0;

		data::GamepadButtonData& _gamepadButtonData;

		struct data::ApplicationSetting& _applicationSetting; 

		int checkIfSelectionChanged = 0;

	};

}