#ifndef style_editor_h
#define style_editor_h

#include <graphical_lib.h>
#include <base_content.h>

namespace ImGuiBaseContent
{

    class StyleEditor :public BaseContentItem
    {
    public:
        StyleEditor();
        ~StyleEditor();

        void show(bool& open);


    private:

        bool showStyleSelector(const char* label);
        //void showFontSelector(const char* label);


        void windowBorderStyle();

        void saveStyle();

        void styleYourApp();

        void createItem();
    };

}
#endif /* style_editor_h */
