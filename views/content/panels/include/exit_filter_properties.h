#pragma once

#include <base_content.h>
#include <masking_properties_data.h>


namespace ImGuiBaseContent
{

	class ExitFilterProperties : public BaseContentItem
	{

	public:
		ExitFilterProperties(
			struct data::ExitFilterProperties& exitFilterProperties
		);

		void show(bool& open) override; 

	private:

		void createExitFilterContent();

		//exit filter settings
		uint8_t  _exitCodeSteps = 1;
		float _maxExitCode = 1.0;

		struct data::ExitFilterProperties& _exitFilterProperties;

	};
}