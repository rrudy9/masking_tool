#pragma once


#define NOMINMAX

#include <windows.h>

#include <base_content.h>
#include <images_data.h>

#include <mouse_tag_input.h>

#include <data_manager.h>
#include <draw_mouse_rect.h>

#include <masking_tag.h>
#include <draw_mask_tag_rect.h>
#include <data_manager.h>

#include <algorithm>

#include <virtual_window.h>
#include <windows_manager.h>


namespace ImGuiBaseContent
{
	class PictureView : public BaseContentItem
	{
	public:
	
		PictureView(
			data::DataManager& dataManager
		);
		~PictureView();

		void show(bool& open) override;

		void setSelectedImage();


	private:

		void createItem();

		double _imageWidth = 0; 
		double _imageHeight = 0;

		void zoomPicture();
		
		void dragImage(); 

		data::ImagesData& _imageData; 

		MouseTagInput _mouseTagInput; 
		DrawMouseRect _drawMouseRect; 
		DrawMaskTagRect _drawMaskTag; 

		MaskingTag _maskingTag; 

		float clip(float n, float lower, float upper);

		ImVec2 _initialCursorPos;
		ImVec2 _imageCursorPos;

		ImVec2 _initScreenCursor; 
		struct data::ApplicationSetting& _applicationSetting; 

	};

}