#pragma once

#include <base_content.h>
#include <images_data.h>

#include <data_manager.h>

#include <windows_manager.h>

#include <Windows.h>

namespace ImGuiBaseContent
{

	class PictureJunker : public BaseContentItem
	{

	public:
		PictureJunker(
			data::DataManager& dataManager
		);

		~PictureJunker();

		void show(bool& open) override;

	private:

		SHORT _deleteKeyState;

		void createItem();

		ImVec2 _imageButtonSize = ImVec2(220, 120);

		data::DataManager& _dataManager; 

		bool _deleteSelection = false; 
		bool _sameSelection = false; 

		int _countSelectionOnSameIndex = 0; 

	};
}