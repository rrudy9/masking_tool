
#pragma once

#include <component.h>
#include <dialog.h>
#include <file_browser.h>
#include <mutex>

namespace ImGuiBaseContent
{
	class ComponentCreator
	{

	public:

		enum class components {
			Dialog = 0,
			FileBrowser
		};

		ComponentCreator();

		void createComponent(components componentRendering, Component** component);

		static ComponentCreator* getInstance();
		static void resetInstance();

		ComponentCreator(ComponentCreator& other) = delete;
		void operator=(const ComponentCreator&) = delete;

	private:

		static ComponentCreator* _creatorInstance;
		static std::mutex _mutex;
	};
}