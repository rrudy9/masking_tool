#pragma once

//#include <base_content.h>
//#include <signal.h>
#include "IconsFontAwesome5.h"
#include <iostream>

namespace ImGuiBaseContent
{

	class Component
	{
	public:
		Component();

		virtual void show(bool& open) = 0;

		virtual void setComponentState(int state) = 0;


		virtual ~Component() = 0;

	protected:

		//BaseContent* _parent = nullptr;
		int _stateComponent = 0;

	};

}


