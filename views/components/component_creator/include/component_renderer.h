#pragma once
#include <component.h>


namespace ImGuiBaseContent
{
	class ComponentRenderer
	{
	public:
		ComponentRenderer(Component* component, const char* title, const char* subTitle);

		void renderObject();

		~ComponentRenderer();

	private:
		Component* _component;

	};

}