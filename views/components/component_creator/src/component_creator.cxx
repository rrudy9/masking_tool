#include <component_creator.h>


namespace ImGuiBaseContent
{

    ComponentCreator* ComponentCreator::_creatorInstance{ nullptr };
    std::mutex ComponentCreator::_mutex ;


    ComponentCreator::ComponentCreator()
    {
    }

    ComponentCreator* ComponentCreator::getInstance()
    {
        std::lock_guard<std::mutex> lock(_mutex);
        if (_creatorInstance == nullptr)
        {
            _creatorInstance = new ComponentCreator();
        }
        return _creatorInstance;
    }


    void ComponentCreator::createComponent(components componentRendering, Component** component)
    {
        if (componentRendering == components::Dialog)
        {
         //   *component = new class Dialog();
        }
        else if (componentRendering == components::FileBrowser)
        {
            //*component = new class FileBrowser();
        }
    }

    void ComponentCreator::resetInstance()
    {
        delete ComponentCreator::_creatorInstance;
        ComponentCreator::_creatorInstance = NULL;
    }

}