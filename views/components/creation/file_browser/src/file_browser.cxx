#include <file_browser.h>

namespace ImGuiBaseContent
{

    FileBrowser::FileBrowser(std::string& filePathName, std::string& filePath)
        :Component()
        , _filePathName(filePathName)
        ,_filePath(filePath)
    {
    };

    void FileBrowser::show(bool& open)
    {
        if (open)
        {

            // Add an icon for .png files 
            ImGuiFileDialog::Instance()->SetExtentionInfos(".png", ImVec4(0, 1, 1, 0.9), ICON_FA_FILE_IMAGE);
            ImGuiFileDialog::Instance()->SetExtentionInfos(".jpg", ImVec4(0, 1, 1, 0.9), ICON_FA_FILE_IMAGE);
            ImGuiFileDialog::Instance()->SetExtentionInfos(".PNG", ImVec4(0, 1, 1, 0.9), ICON_FA_FILE_IMAGE);
            ImGuiFileDialog::Instance()->SetExtentionInfos(".jpeg", ImVec4(0, 1, 1, 0.9), ICON_FA_FILE_IMAGE);
            ImGuiFileDialog::Instance()->SetExtentionInfos(".bmp", ImVec4(0, 1, 1, 0.9), ICON_FA_FILE_IMAGE);
            
            ImGuiFileDialog::Instance()->SetExtentionInfos(".pic_tool", ImVec4(.8, .4, .4, 1), ICON_FA_QRCODE);

            if (ImGuiFileDialog::Instance()->Display("browserDir"))
            {
                // action if OK
                if (ImGuiFileDialog::Instance()->IsOk())
                {

                    // check for saving project 
                    
                    std::string filePathName = ImGuiFileDialog::Instance()->GetFilePathName();
                    std::string filePath = ImGuiFileDialog::Instance()->GetCurrentPath();

                    _filePathName = ImGuiFileDialog::Instance()->GetFilePathName();
                    _filePath = ImGuiFileDialog::Instance()->GetCurrentPath();
                }

                ImGuiFileDialog::Instance()->Close();

                // close
            }
        }

    }

    int FileBrowser::getComponentState()
    {
        return _stateComponent; 
    }

    void FileBrowser::setComponentState(int state)
    {

        _stateComponent = state;
        ImGuiFileDialog::Instance()->Close();
        const char* filters = "";
        // uint8_t confirmationSelection = 0;
        ImGuiFileDialogFlags confirmationSelection = ImGuiFileDialogFlags_None;

        if (state == browserState::load_project)
        {
            filters = "Project (*.pic_tool *.ptp){.pic_tool,.ptp}";

            _title = ICON_FA_FOLDER_OPEN " Open Project";
        }
        else if (state == browserState::save_project)
        {
            filters = ".pic_tool";

            _title = ICON_FA_SAVE " Save Project";

            confirmationSelection = ImGuiFileDialogFlags_ConfirmOverwrite;

        }
        else if (state == browserState::export_as_xml)
        {
            _title = ICON_FA_FILE_EXPORT " Export As XML";

            filters = ".xml";

            confirmationSelection = ImGuiFileDialogFlags_ConfirmOverwrite;

        }
        else if (state == browserState::load_pictures)
        {
            filters = "Image files (*.png *.PNG *.jpg *.jpeg *.bmp){.png,.PNG,.jpg,.jpeg,.bmp}";
            _title = ICON_FA_FILE_IMAGE " Load Pictures";

        }

        ImGuiFileDialog::Instance()->OpenDialog("browserDir",
            _title, filters,
            ".", "", 1, nullptr, confirmationSelection);


    }



    FileBrowser::~FileBrowser()
    {

    }

}