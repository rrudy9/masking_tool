#pragma once


#include <graphical_lib.h>
#include <component.h>
#include <signal.h>


namespace ImGuiBaseContent
{

	class FileBrowser : public Component
	{
	public:

		enum browserState 
		{
			load_project = 0,
			save_project,
			export_as_xml,
			load_pictures,
		};

		FileBrowser(std::string& filePathName, std::string& filePath);
		~FileBrowser();

		void show(bool& open) override;
		void setComponentState(int state) override;

		int getComponentState(); 

	private:

		const char* _title;

		std::string& _filePathName;
		std::string& _filePath; 


	};

}