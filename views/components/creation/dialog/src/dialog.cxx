
#include <Dialog.h>

namespace ImGuiBaseContent
{
	Dialog::Dialog(bool&  dialogResponse )
		:Component()
		,_dialogResponse(dialogResponse)
	{
	}


	void Dialog::show(bool& open)
	{

		if (open)
		{
			ImGui::OpenPopup(_title.c_str());

			ImVec2 center = ImGui::GetMainViewport()->GetCenter();
			ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));

			if (ImGui::BeginPopupModal(_title.c_str(), NULL, ImGuiWindowFlags_AlwaysAutoResize))
			{
				ImGui::Text(_subTitle.c_str());
				ImGui::Separator();


				if (ImGui::Button(_acceptButtonTitle.c_str(), ImVec2(60, 0))) 
				{
					if(_stateComponent != dialogState::warning)
						_dialogResponse = true;
					open = false; 
					ImGui::CloseCurrentPopup();
				}

				if (_stateComponent != dialogState::warning)
				{
					ImGui::SetItemDefaultFocus();
					ImGui::SameLine();
					if (ImGui::Button(_cancelButtonTitle.c_str(), ImVec2(80, 0)))
					{
						_dialogResponse = false;
						open = false; 
						ImGui::CloseCurrentPopup();
					
					}
				}
				ImGui::EndPopup();

			}

		}


	}

	void Dialog::setComponentSetting(int state, const std::string& title, const std::string& subTitle, const std::string& acceptButtonTitle, const std::string& cancelButtonTitle)
	{
		_stateComponent = state; 

		_title = title; 
		_subTitle = subTitle; 
		_acceptButtonTitle = acceptButtonTitle; 
		_cancelButtonTitle = cancelButtonTitle; 
	
	}


	void Dialog::setComponentState(int state)
	{
		_stateComponent = state;
		if (state == dialogState::quit_state)
		{
		//	_title = "Quit";
		//	_subTitle = "Do you really want to quit?";
		}
		else if (state == dialogState::warning)
		{

		}
	}



	Dialog::~Dialog()
	{

	}
}