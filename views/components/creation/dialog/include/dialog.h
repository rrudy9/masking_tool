#pragma once

#include <graphical_lib.h>
#include <iostream>
#include <component.h>
namespace ImGuiBaseContent
{

	class Dialog : public Component
	{
	public:

		enum dialogState 
		{ 
			quit_state = 0,
			warning
		};

		Dialog(bool& dialogResponse);
		~Dialog();

		void show(bool& open) override;

		void setComponentState(int state) override;

		void setDialogResponse(bool& acceptedResponse); 

		void setComponentSetting(int state, const std::string& title, const std::string& subTitle, const std::string& acceptButtonTitle, const std::string& cancelButtonTitle);

	private:

		std::string _title = ""; 
		std::string _subTitle = "";

		std::string _acceptButtonTitle = ""; 
		std::string _cancelButtonTitle = ""; 

		bool &_dialogResponse; 


	};

}