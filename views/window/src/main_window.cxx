
#include <main_window.h>

MainWindow* MainWindow::_mWindowInstance{ nullptr };
std::mutex MainWindow::_mutex;

#ifdef DIRECTX_FOUND
LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
#endif

MainWindow::MainWindow()
    : VirtualWindow()
    #ifdef DIRECTX_FOUND
    ,_hwnd(nullptr)
    ,_directXContext(DirectXContext())
    #else
        , _window(nullptr)
        , _openGLContext(OpenGLContext())
    #endif
    ,_imGuiContext(ImGUIContext())
    ,_mainToolbar(ImGuiBaseContent::MainToolbar())
{
}

void MainWindow::init(int width, int height, const char* windowTitle)
{

    _widthScreen = width;
    _heightScreen = height;
    _windowTitle = windowTitle;

#ifdef DIRECTX_FOUND

      HICON hIcon = HICON (LoadImage(nullptr, _T("Ressources/assets/icon.ico"), IMAGE_ICON, 16, 16, LR_LOADFROMFILE));

    // initialize window 
    _windowClass = { sizeof(WNDCLASSEX),
         CS_CLASSDC,
         WndProc,
         0L,
         0L,
         GetModuleHandle(NULL),
         hIcon,
         NULL,
         NULL,
         NULL,
         _T("PicTools"),
         NULL };

    ::RegisterClassEx(&_windowClass);
    _hwnd = ::CreateWindow(
        _windowClass.lpszClassName,
        _T(_windowTitle),
        WS_OVERLAPPEDWINDOW,
        100,
        100,
        _widthScreen,
        _heightScreen,
        NULL,
        NULL,
        _windowClass.hInstance,
        NULL);

    _directXContext.init(this);
#else
    _openGLContext.init(this);
#endif
    
    _imGuiContext.init(this);
    _mainToolbar.init(this);

#ifndef DIRECTX_FOUND
    WindowsManager::getInstance()->_mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    WindowsManager::getInstance()->_monitor = glfwGetWindowMonitor(_window);
#endif // !DIRECTX_FOUND


}

MainWindow* MainWindow::getInstance()
{
    std::lock_guard<std::mutex> lock(_mutex);
    if (_mWindowInstance == nullptr)
    {
        _mWindowInstance = new MainWindow();
    }
    return _mWindowInstance;
}




unsigned int MainWindow::getWindowWidth()
{
    return _widthScreen;
}

unsigned int MainWindow::getWindowHeight()
{
    return _heightScreen;
}




void MainWindow::render()
{
#ifdef DIRECTX_FOUND

    while (!_done)
    {
        MSG msg;
        while (::PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
        {
            ::TranslateMessage(&msg);
            ::DispatchMessage(&msg);
            if (msg.message == WM_QUIT)
                _done = true;
        }
        if (_done)
            break;

        _imGuiContext.preRendering();
        
        _mainToolbar.rendering();

        _imGuiContext.postRendering();

        _directXContext.postRendering();

    }

#else

    while (!glfwWindowShouldClose(_window))
    {

        _openGLContext.preRendering();

        _imGuiContext.preRendering();

        _mainToolbar.rendering();

        _imGuiContext.postRendering();

        _openGLContext.postRendering();
    }

#endif 


}

#ifdef DIRECTX_FOUND

HWND MainWindow::getHWND()
{
    return _hwnd; 
}


WNDCLASSEX MainWindow::getWNDClass()
{
    return _windowClass; 
}

void MainWindow::resizeWindow(WPARAM wParam, LPARAM lParam)
{
    if (WindowsManager::getInstance()->g_pd3dDevice != NULL && wParam != SIZE_MINIMIZED)
    {
        _directXContext.CleanupRenderTarget();
        WindowsManager::getInstance()->g_pSwapChain->ResizeBuffers(0, (UINT)LOWORD(lParam), (UINT)HIWORD(lParam), DXGI_FORMAT_UNKNOWN, 0);
        _directXContext.CreateRenderTarget();
    }
}

#endif // DIRECTX_FOUND


#ifndef DIRECTX_FOUND

GLFWwindow* MainWindow::getWindow()
{
    return _window;
}

void MainWindow::setWindow(GLFWwindow** window)
{
    _window = *window;
}

void MainWindow::resizeWindow(int width, int height)
{
    glViewport(0, 0, width, height);
}




void MainWindow::onKey(int key, int scancode, int action, int mods)
{

    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {

    }
}

#endif // !DIRECTX_FOUND

void MainWindow::resetInstance()
{
    delete MainWindow::_mWindowInstance;
    MainWindow::_mWindowInstance = NULL;
}

void MainWindow::goBorderless()
{

#ifdef DIRECTX_FOUND

    if (_goFrameless && !_countFramelessWindowCall)
    {
        DWORD dwStyle = GetWindowLong(_hwnd, GWL_STYLE);
        
        SetWindowLong(_hwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
        SetWindowPos(_hwnd, NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);

        _checkBeforeFramelessWindow = true; 
        
        ++_countFramelessWindowCall;
    
    }

    if (!_goFrameless && _checkBeforeFramelessWindow && _countFramelessWindowCall == 1)
    {
        _countFramelessWindowCall = 0; 

        DWORD dwStyle = GetWindowLong(_hwnd, GWL_STYLE);

        SetWindowLong(_hwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
        SetWindowPos(_hwnd, NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER | SWP_FRAMECHANGED);

        _checkBeforeFramelessWindow = false;

    }

#else
    if (_goFrameless && !_countFramelessWindowCall)
    {
        glfwSetWindowAttrib(_window, GLFW_DECORATED, _goFrameless);
        _checkBeforeFramelessWindow = true;

        ++_countFramelessWindowCall;

        if (!_goFrameless && _checkBeforeFramelessWindow && _countFramelessWindowCall == 1)
        {

            glfwSetWindowAttrib(_window, GLFW_DECORATED, !_goFrameless);
            _countFramelessWindowCall = 0;

        }

    }

 //   glfwSetWindowAttrib(_window, GLFW_DECORATED, !_goFrameless);
#endif

}


void MainWindow::minimizeWindow()
{

#ifdef DIRECTX_FOUND
    PostMessage(_hwnd, WM_SYSCOMMAND, SC_MINIMIZE, 0);

#else
    glfwIconifyWindow(_window);

#endif

}

void MainWindow::onClose()
{
#ifdef DIRECTX_FOUND
    DestroyWindow(_hwnd);
#else
    glfwSetWindowShouldClose(_window, true); 
#endif
}

void MainWindow::maximizeWindow()
{
#ifdef DIRECTX_FOUND

    if (!_fullScreen)
    {
        _oldScreenHeight = _heightScreen;
        _oldScreenWidth = _widthScreen;

        GetWindowRect(_hwnd, &_lpRect);
        
        HDC hDC = ::GetWindowDC(NULL);
        ::SetWindowPos(_hwnd, NULL, 0, 0, ::GetDeviceCaps(hDC, HORZRES), ::GetDeviceCaps(hDC, VERTRES), SWP_FRAMECHANGED);
    
        _fullScreen = true;
    
    }
    else
    {
        ::SetWindowPos(_hwnd, NULL, _lpRect.left, _lpRect.top, _oldScreenWidth, _oldScreenHeight, SWP_FRAMECHANGED);
        _fullScreen = false;

    }

#else

    if (!_fullScreen)
    {
        _oldScreenHeight = _heightScreen;
        _oldScreenWidth =  _widthScreen;

        _oldTopLeftX = topLeftX;
        _oldTopLeftY = topLeftY;

        // switch to full screen

        glfwSetWindowMonitor(_window, WindowsManager::getInstance()->_monitor, 0, 0, WindowsManager::getInstance()->_mode->width, WindowsManager::getInstance()->_mode->height, 0);

        _fullScreen = true;

    }
    else
    {
        glfwSetWindowMonitor(_window, nullptr, _oldTopLeftX, _oldTopLeftY, _oldScreenWidth, _oldScreenHeight, 0);

        _fullScreen = false;

    }

#endif

}


MainWindow::~MainWindow()
{
    _imGuiContext.endRendering();
    

#ifdef DIRECTX_FOUND
    _directXContext.endRendering(); 
#else
    _openGLContext.endRendering();
#endif 

    WindowsManager::resetInstance(); 

    //    ComponentCreator::resetInstance();

}

#ifdef DIRECTX_FOUND


#ifndef WM_DPICHANGED
#define WM_DPICHANGED 0x02E0 // From Windows SDK 8.1+ headers
#endif

// Forward declare message handler from imgui_impl_win32.cpp
extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);


// Forward declare message handler from imgui_impl_win32.cpp
extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

// Win32 message handler
LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (ImGui_ImplWin32_WndProcHandler(hWnd, msg, wParam, lParam))
        return true;

    switch (msg)
    {

    case WM_RBUTTONDOWN:
    {
        if (WindowsManager::getInstance()->getWindowMovingValue())
        {
            SetCursor(LoadCursor(NULL, IDC_SIZEALL));
            MainWindow::getInstance()->_isMainWindowMoved = 1;
            SetCapture(hWnd);

            GetCursorPos(&MainWindow::getInstance()->_lastLocation);
            RECT rect;
            GetWindowRect(hWnd, &rect);
            MainWindow::getInstance()->_lastLocation.x = MainWindow::getInstance()->_lastLocation.x - rect.left;
            MainWindow::getInstance()->_lastLocation.y = MainWindow::getInstance()->_lastLocation.y - rect.top;

            return 0;
        }
    }
    case WM_RBUTTONUP:
    {
        if (WindowsManager::getInstance()->getWindowMovingValue())
        {
            ReleaseCapture();
            MainWindow::getInstance()->_isMainWindowMoved = 0;

            return 0;
        }
    }
    case WM_MOUSEMOVE:
        if (MainWindow::getInstance()->_isMainWindowMoved
            && !MainWindow::getInstance()->_fullScreen
            )
        {
         
            RECT mainWindowRect = { 0 };

            int windowWidth, windowHeight;
            GetWindowRect(hWnd, &mainWindowRect);
            windowHeight = mainWindowRect.bottom - mainWindowRect.top;
            windowWidth = mainWindowRect.right - mainWindowRect.left;

            POINT currentpos;
            GetCursorPos(&currentpos);
            int x = currentpos.x -  MainWindow::getInstance()->_lastLocation.x;
            int y = currentpos.y -  MainWindow::getInstance()->_lastLocation.y;
            MoveWindow(hWnd, x, y, windowWidth, windowHeight, false);
          
        }
        return 0;
    case WM_INPUT:
    {
        auto input = GetWindowLongPtr(hWnd, GWLP_USERDATA);

        UINT dwSize;
        GetRawInputData((HRAWINPUT)lParam, RID_INPUT, NULL, &dwSize, sizeof(RAWINPUTHEADER));
        LPBYTE lpb = new BYTE[dwSize];

        if (GetRawInputData((HRAWINPUT)lParam, RID_INPUT, lpb, &dwSize, sizeof(RAWINPUTHEADER)) != dwSize) {
            OutputDebugString(TEXT("GetRawInputData does not return correct size !\n"));
        }

        RAWINPUT* raw = (RAWINPUT*)lpb;

        if (RIM_TYPEKEYBOARD == raw->header.dwType)
        {


            // need to add hot keys
            RAWKEYBOARD* data = &raw->data.keyboard;

            UINT scanCode = MapVirtualKey(data->VKey, MAPVK_VK_TO_VSC);

            switch (data->VKey)
            {
            case VK_LEFT: case VK_UP: case VK_RIGHT: case VK_DOWN:
            case VK_RCONTROL: case VK_RMENU:
            case VK_LWIN: case VK_RWIN: case VK_APPS:
            case VK_PRIOR: case VK_NEXT:
            case VK_END: case VK_HOME:
            case VK_INSERT: case VK_DELETE:
            case VK_DIVIDE:
            case VK_NUMLOCK:

                scanCode |= KF_EXTENDED;

            }
            TCHAR keyStrName[128];
            int result = 0;

            try {

                result = GetKeyNameText(scanCode << 16, keyStrName, 128);
                if (result == 0)
                {
                    throw std::runtime_error("GetKeyNameText result error\n");
                }
            }
            catch (const std::runtime_error& exception) {
                std::cerr << "WinAPI Error occured " << exception.what() << '\n';
                break;
            }

            WindowsManager::getInstance()->_vKeyCharName = keyStrName;
            WindowsManager::getInstance()->_vKeyValue = std::to_string(data->VKey);
            WindowsManager::getInstance()->_scanCode = scanCode;

        }

    }
    return 0;

    case WM_SIZE:
        MainWindow::getInstance()->resizeWindow(wParam, lParam); 
        return 0;
    
    //case WM_SYSCOMMAND:
    //    if ((wParam & 0xfff0) == SC_KEYMENU) // Disable ALT application menu
    //        return 0;
    //    break;

    case WM_DESTROY:
        ::PostQuitMessage(0);
        return 0;
    case WM_DPICHANGED:
        if (ImGui::GetIO().ConfigFlags & ImGuiConfigFlags_DpiEnableScaleViewports)
        {
            //const int dpi = HIWORD(wParam);
            //printf("WM_DPICHANGED to %d (%.0f%%)\n", dpi, (float)dpi / 96.0f * 100.0f);
            const RECT* suggested_rect = (RECT*)lParam;
            ::SetWindowPos(hWnd, NULL, suggested_rect->left, suggested_rect->top, suggested_rect->right - suggested_rect->left, suggested_rect->bottom - suggested_rect->top, SWP_NOZORDER | SWP_NOACTIVATE);
        }
        break;
    }


    return ::DefWindowProc(hWnd, msg, wParam, lParam);
}

#endif 



