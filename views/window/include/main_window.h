#pragma once 

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <mutex>

#include <virtual_window.h>

#ifdef DIRECTX_FOUND
#include <DirectX_context.h>
#else
#include <openGL_context.h>
#endif

#include <ImGui_context.h> 
#include <main_toolbar.h>

#include <windows_manager.h>

//#include <Component.h>

//#include <ComponentCreator.h>
//#include <ComponentRenderer.h>


class MainWindow : public VirtualWindow
{

public:

	MainWindow();
	~MainWindow();

	void init(int width, int height, const char* windowTitle);

#ifdef DIRECTX_FOUND

	HWND getHWND() override;
	WNDCLASSEX getWNDClass() override; 
	void resizeWindow(WPARAM wParam, LPARAM lParam);

#else
	GLFWwindow* getWindow() override;
	void setWindow(GLFWwindow** window) override;
	void resizeWindow(int width, int height) override;

	void onKey(int key, int scancode, int action, int mods) override;
#endif

	unsigned int getWindowWidth();
	unsigned int getWindowHeight();

	void onClose() override;
	void goBorderless() override; 
	void minimizeWindow() override;
	void maximizeWindow() override;


	void render();

	MainWindow(MainWindow& other) = delete;
	void operator=(const MainWindow&) = delete;

	static MainWindow* getInstance();
	static void resetInstance();


private:
#ifdef DIRECTX_FOUND

	WNDCLASSEX  _windowClass;
	HWND _hwnd;
	DirectXContext _directXContext; 
	RECT  _lpRect;

#else
	GLFWwindow* _window;
	OpenGLContext _openGLContext;
#endif

	// Render contexts
	ImGUIContext _imGuiContext;


	ImGuiBaseContent::MainToolbar _mainToolbar;

	static MainWindow* _mWindowInstance;
	static std::mutex _mutex;

	bool _done = false;

	int _oldScreenWidth = 0;
	int _oldScreenHeight = 0;

	int _oldTopLeftX = 0;
	int _oldTopLeftY = 0;

	bool _checkBeforeFramelessWindow = false; 
	int _countFramelessWindowCall = 0; 



};
