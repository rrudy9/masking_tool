#pragma once
#include <iostream>


#include <graphical_lib.h>


class VirtualWindow
{
public:

	VirtualWindow();

#ifdef DIRECTX_FOUND

	virtual HWND getHWND() = 0; 
	virtual WNDCLASSEX getWNDClass() = 0;


#else

	virtual GLFWwindow* getWindow() = 0;
	virtual void setWindow(GLFWwindow** window) = 0;
	virtual void resizeWindow(int width, int height) = 0;

	virtual void onKey(int key, int scancode, int action, int mods) = 0;

#endif 

	virtual void onClose() = 0;
	virtual void goBorderless() = 0; 
	virtual void minimizeWindow() = 0; 
	virtual void maximizeWindow() = 0; 
	
	int _widthScreen = 0;
	int _heightScreen = 0;
	const char* _windowTitle = NULL;


	int topLeftX = 0;
	int topLeftY = 0; 

#ifdef DIRECTX_FOUND
	POINT _lastLocation;
#else
	int originCursorPosX = 0;
	int originCursorPosY = 0;
	int offsetCursorX = 0 ;
	int offsetCursorY = 0;
	int currentCursorPosX = 0;
	int currentCursorPosY = 0;
#endif



	bool _isMainWindowMoved = false;

	bool _fullScreen = false; 

	bool _goFrameless = false;

	
};

