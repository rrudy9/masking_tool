file(GLOB tiny_xml_lib 
	src/*.cpp
	include/*.h)

add_library(tiny_xml ${tiny_xml_lib} ) 

set_property(TARGET tiny_xml PROPERTY FOLDER "third_libs/tiny_xml")

target_include_directories(tiny_xml PRIVATE "${PROJECT_SOURCE_DIR}/third_libs/tiny_xml/include")
