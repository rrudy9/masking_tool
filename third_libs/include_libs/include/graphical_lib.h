#pragma once


#include <glad/glad.h>
#include <GLFW/glfw3.h>


#include "imgui.h"
#include "ImGuiFileDialog.h"

#ifdef DIRECTX_FOUND
	#include "imgui_impl_win32.h"
	#include "imgui_impl_dx11.h"
	#include <d3d11.h>
#else
	#include "imgui_impl_glfw.h"
	#include "imgui_impl_opengl3.h"

#endif

