#pragma once 
#include <iostream>
#include <functional>
#include <utility>
#include <vector>

template<typename ... ARGS>
class Signal {
public:
    
    Signal(const Signal<ARGS...>&) = delete;
    Signal(Signal<ARGS...>&&) = delete;
    Signal<ARGS...>& operator = (const Signal<ARGS...>&) = delete;

    void emit(ARGS... args)  {
        Signal<ARGS...>::_function(args...);
    }
    
//
    void connect(std::function<void(ARGS...)> slot) {

//        if(Signal<ARGS...>::_connection == nullptr)
//        {
//            Signal<ARGS...>::_connection = (std::function<void(ARGS...)>*) malloc (sizeof(std::function<void(ARGS...)>) * 4);
//        }
//
//        Signal<ARGS...>::_connection[0] = slot;
        Signal<ARGS...>::_function = slot;
    }
    
private:
//    static std::vector<std::function<void(ARGS...)>> _connection;
    static std::function<void(ARGS...)> * _connection;
    static std::function<void(ARGS...)> _function;
    
};


template<typename ... ARGS>
std::function<void(ARGS...)>* Signal<ARGS...>::_connection = nullptr;
//std::vector<std::function<void(ARGS...)>> Signal<ARGS...>::_connection = { 0 };

template<typename ... ARGS>
std::function<void(ARGS...)> Signal<ARGS...>::_function = nullptr;
