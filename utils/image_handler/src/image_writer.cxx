#include <image_writer.h>

#include <iostream>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>


bool writeTextureToFile(const std::string& fileName, int width, int height, int compression, unsigned char* data)
{
#ifdef DIRECTX_FOUND

    if (!stbi_write_bmp(fileName.c_str(), width, height,4, data))
	{
		return false; 
	}


#else
	if (!stbi_write_bmp(fileName.c_str(), width, height, compression, data))
	{
		return false; 
	}

#endif
	return true; 
}
