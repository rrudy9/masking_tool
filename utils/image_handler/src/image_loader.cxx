#include <image_loader.h>

#define STB_IMAGE_IMPLEMENTATION
#define STBI_FAILURE_USERMSG
#include <stb_image.h>

#ifdef DIRECTX_FOUND



// Simple helper function to load an image into a DX11 texture with common settings
bool loadTextureFromFile(const std::string& fileName, ID3D11ShaderResourceView** texture, int& width, int& height, int& compression, unsigned char** imageData, int reqComp)
{
    // Load from disk into a raw RGBA buffer
    *imageData = stbi_load(fileName.c_str(), &width, &height, NULL, reqComp);

    if (*imageData == NULL)
    {
        fprintf(stderr, "error while loading image :%s\n", stbi_failure_reason());
        return false;
    }

    D3D11_TEXTURE2D_DESC desc;
    ZeroMemory(&desc, sizeof(desc));
    desc.Width = width;
    desc.Height = height;
    desc.MipLevels = 1;
    desc.ArraySize = 1;
    
    desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    
    desc.SampleDesc.Count = 1;
    desc.Usage = D3D11_USAGE_DEFAULT;
    desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
    desc.CPUAccessFlags = 0;

    ID3D11Texture2D* pTexture = NULL;
    D3D11_SUBRESOURCE_DATA subResource;
    subResource.pSysMem = * imageData;
    subResource.SysMemPitch = desc.Width * reqComp;
    subResource.SysMemSlicePitch = 0;
    WindowsManager::getInstance()->g_pd3dDevice->CreateTexture2D(&desc, &subResource, &pTexture);

    // Create texture view
    D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc; 
    ZeroMemory(&srvDesc, sizeof(srvDesc));
    srvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
    srvDesc.Texture2D.MipLevels = desc.MipLevels;
    srvDesc.Texture2D.MostDetailedMip = 0;
    WindowsManager::getInstance()->g_pd3dDevice->CreateShaderResourceView(pTexture, &srvDesc, texture);
    pTexture->Release();


    return true; 
}


#else 

bool loadTextureFromFile(const std::string& fileName, unsigned int& texture, int& width, int& height, int& compression, unsigned char** imageData, int reqComp)
{
    // Load from file

    *imageData = stbi_load(fileName.c_str(), &width, &height, &compression, reqComp);
    if (*imageData == NULL)
    {
        fprintf(stderr, "error while loading image :%s\n", stbi_failure_reason());
        return false;
    }


    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    // Setup filtering parameters for display
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    // Upload pixels into texture
#if defined(GL_UNPACK_ROW_LENGTH) && !defined(__EMSCRIPTEN__)
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
#endif

    GLenum format = 0;
    if (compression == 1)
        format = GL_RED;
    else if (reqComp == 3)
        format = GL_RGB;
    else if (reqComp == 4)
        format = GL_RGBA;

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, format, GL_UNSIGNED_BYTE, *imageData);
    glGenerateMipmap(GL_TEXTURE_2D);

    return true;
}

#endif // DIRECTX_FOUND


void loadImage(const std::string& fileName, int& width, int& height, unsigned char** imageData)
{
    try
    {
        *imageData = stbi_load(fileName.c_str(), &width, &height, 0, 4);
        if (*imageData == NULL)
        {
            throw std::exception(stbi_failure_reason()); 
        }
    }

    catch (std::exception& e)
    {
        fprintf(stderr, e.what()); 
    }
   
}

void freeImage(unsigned char** imageData)
{
    stbi_image_free(*imageData);
}
