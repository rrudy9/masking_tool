#ifndef image_writer_h
#define image_writer_h

#include <graphical_lib.h>
#include <string>

bool writeTextureToFile(const std::string& fileName, int width, int height, int compression, unsigned char* data);

#endif