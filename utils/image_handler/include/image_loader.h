#ifndef image_loader_h
#define image_loader_h

#include <graphical_lib.h>

#include <windows_manager.h>


#ifdef DIRECTX_FOUND
bool loadTextureFromFile(const std::string& fileName, ID3D11ShaderResourceView** texture, int& width, int& height,int& compression, unsigned char** imageData, int reqComp);

#else
bool loadTextureFromFile(const std::string& fileName, unsigned int& texture, int& width, int& height,int& compression, unsigned char** imageData, int reqComp);
#endif

void freeImage(unsigned char** imageData);

void loadImage(const std::string& fileName, int& width, int& height, unsigned char** imageData);

#endif // !
